﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Yugamiru
{
    class saveRecords
    {
        JointEditDoc joint_edit; /*= new JointEditDoc();*/


        FrontBodyResultWnd m_wndStandingPostures;
        FrontBodyResultWnd m_wndKneedownPostures;
        SQLiteConnection yugamiru_con;


        public saveRecords(ResultView rv)
        {
            joint_edit = rv.m_JointEditDoc;
       

        }
        public saveRecords(JointEditDoc joint)
        {
            joint_edit = joint;
        }

        public saveRecords(ResultView rv, FrontBodyResultWnd standing_frcs, FrontBodyResultWnd kneedown_frcs)
        {
            joint_edit = rv.m_JointEditDoc;
            m_wndStandingPostures = standing_frcs;
            m_wndKneedownPostures = kneedown_frcs;
          

          
           
        }
        /////////////////////////////////////////////////////**************Compressed Image************////////////////////////////


        private void VaryQualityLevel_standing()                /// /////////////////////Standing image////
        {
            try
            {

                string pathFirst = GlobalVar.OriginalImages[GlobalVar.positionNameOne].FileName;
                //byte[] standing_image = System.Text.Encoding.Default.GetBytes(pathFirst);
                Bitmap bmp1 = new Bitmap(pathFirst);
                ImageCodecInfo jgpEncoder = GetEncoder(ImageFormat.Jpeg);


                System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;

                EncoderParameters myEncoderParameters = new EncoderParameters(1);

                EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 50L);

                myEncoderParameters.Param[0] = myEncoderParameter;
                bmp1.SetResolution(1280, 1024);
                string path = System.Windows.Forms.Application.StartupPath + @"\standing.JPG";
                bmp1.Save(path, jgpEncoder, myEncoderParameters);
                Image standing_bms = bmp1;
                ImageConverter converter = new ImageConverter();

                byte[] standing_test = (byte[])converter.ConvertTo(standing_bms, typeof(byte[]));

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        private void VaryQualityLevel_sidepic()   ////////**********Sidepic image********////////
        {

            string pathSecond = GlobalVar.OriginalImages[GlobalVar.positionNameTwo].FileName;
            Bitmap bmp3 = new Bitmap(pathSecond);
            try
            {

                ImageCodecInfo jpgseEncoder = GetEncods(ImageFormat.Jpeg);


                System.Drawing.Imaging.Encoder myEncoder =
            System.Drawing.Imaging.Encoder.Quality;

                EncoderParameters myEncoderParameters = new EncoderParameters(1);

                EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 50L);

                myEncoderParameters.Param[0] = myEncoderParameter;
                bmp3.SetResolution(1280, 1024);


                bmp3.Save(System.Windows.Forms.Application.StartupPath + @"\sidepic.JPG", jpgseEncoder,
                    myEncoderParameters);


                Image sidepic_bms = bmp3;


                ImageConverter converter = new ImageConverter();
                byte[] sidepic_test = (byte[])converter.ConvertTo(sidepic_bms, typeof(byte[]));



                // byte[] show_comp_img = File.ReadAllBytes(@"E:\image.JPG");
                //  MessageBox.Show("done");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

        }

        private ImageCodecInfo GetEncods(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        private void VaryQualityLevel_crouched()   ///////************Crouched image*******////////
        {
            try
            {

                string pathFirst = GlobalVar.OriginalImages[GlobalVar.positionNameOne].FileName;
                //byte[] standing_image = System.Text.Encoding.Default.GetBytes(pathFirst);
                Bitmap bmp1 = new Bitmap(pathFirst);
                ImageCodecInfo jgpEncoder = GetEncodec(ImageFormat.Jpeg);


                System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;

                EncoderParameters myEncoderParameters = new EncoderParameters(1);

                EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 50L);

                myEncoderParameters.Param[0] = myEncoderParameter;
                bmp1.SetResolution(1280, 1024);
                string path = System.Windows.Forms.Application.StartupPath + @"\crouched.JPG";
                bmp1.Save(path, jgpEncoder, myEncoderParameters);
                Image standing_bms = bmp1;
                ImageConverter converter = new ImageConverter();

                byte[] standing_test = (byte[])converter.ConvertTo(standing_bms, typeof(byte[]));

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }  //

        private ImageCodecInfo GetEncodec(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }


        public void editID()
        {
            if (File.Exists("yugamirus.sqlite"))
            {
                try
                {


                    string Id = joint_edit.GetDataID();
                    string name = joint_edit.GetDataName();
                    double height = joint_edit.GetDataHeight();
                    string str = "-";
                    if (joint_edit.GetDataGender() == 1) str = "M";
                    if (joint_edit.GetDataGender() == 2) str = "F";
                    string gender = str;

                    string sYear = string.Empty, sMonth = string.Empty, sDay = string.Empty;
                    joint_edit.GetDataDoB(ref sYear, ref sMonth, ref sDay);
                    string date_Birth = sYear + "-" + sMonth + "-" + sDay;

                    DateTime date_birth = DateTime.Parse(date_Birth);

                    var dateBirth = date_birth.ToString("yyyy-MM-dd");


                    string strYear = string.Empty, strMonth = string.Empty, strDay = string.Empty, strTime = string.Empty;
                    string measureTime = joint_edit.GetDataMeasurementTime();
                    if (measureTime != null)
                    {
                        joint_edit.GetDataAcqDate(ref strYear, ref strMonth, ref strDay, ref strTime);
                    }
                    else
                    {
                        //		strYear		= time.Format("%y");
                        strYear = DateTime.Now.Year.ToString();
                        //		strMonth	= time.Format("%m");
                        strMonth = DateTime.Now.Month.ToString();
                        strDay = DateTime.Now.Day.ToString();
                        strTime = DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString();
                    }
                    string db_str_TmpTime;
                    db_str_TmpTime = strYear + "-" + strMonth + " - " + strDay;

                    DateTime db_strTmp_Time = DateTime.Parse(db_str_TmpTime);

                    var db_strTmpTimes = db_strTmp_Time.ToString("yyyy-MM-dd");




                    yugamiru_con = new SQLiteConnection("DataSource=yugamirus.sqlite;version=3");
                    yugamiru_con.Open();
                    string sql = @"UPDATE tblmaster_yugamiru SET Patientname = '" + name + "', PatientDOB = '"
                        + dateBirth + "', PatientGender = '" + gender + "', Height = '" + height + "', Dateofcreation = '"
                        + db_strTmpTimes + "' WHERE PatientID = '" + Id + "'";
                    SQLiteCommand comd = new SQLiteCommand(sql, yugamiru_con);
                    comd.ExecuteNonQuery();
                    yugamiru_con.Close();
                    System.Windows.Forms.MessageBox.Show("Data Updated Successfully");
                }
                catch (SQLiteException ex)
                {
                    Logger.LogError(ex);
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Error!");
            }
        }


        public void insertResult(ResultView res)
        {
          if ((GlobalVar.OriginalImages.ContainsKey("FirstImage")  && (GlobalVar.OriginalImages.ContainsKey("SecondImage")) && (GlobalVar.OriginalImages.ContainsKey("ThirdImage"))))
            {
                try
                {
                    JointEditDoc joint_edit = res.m_JointEditDoc;


                    string Id = joint_edit.GetDataID();
                    string name = joint_edit.GetDataName();
                    double height = joint_edit.GetDataHeight();
                    string str = "-";
                    if (joint_edit.GetDataGender() == 1) str = "M";
                    if (joint_edit.GetDataGender() == 2) str = "F";
                    string gender = str;






                    string standing_glabella_pos = m_wndStandingPostures.GetStandingEarPos();
                    string kneeDown_glabella_pos = m_wndKneedownPostures.GetKneeDownGlabellaPos();
                    string standing_ear_pos = m_wndStandingPostures.GetStandingEarPos();
                    string kneeDown_ear_pos = m_wndKneedownPostures.GetKneeDownEarPos();
                    string standing_chin_pos = m_wndStandingPostures.GetStandingChinPos();
                    string kneedown_chin_pos = m_wndKneedownPostures.GetKneeDownChinPos();
                    string standing_shoulder_pos = m_wndStandingPostures.GetStandingShoulderPos();
                    string kneedown_shoulder_pos = m_wndKneedownPostures.GetKneeDownShoulderPos();
                    string standing_hip_pos = m_wndStandingPostures.GetStandingHipPos();
                    string kneedown_hip_pos = m_wndKneedownPostures.GetKneeDownHipPos();
                    string standing_ear_angle = m_wndStandingPostures.GetStandingEarAngle();
                    string kneedown_ear_angle = m_wndKneedownPostures.GetKneeDownEarAngle();
                    string standing_shoulder_angle = m_wndStandingPostures.GetStandingShoulderAngle();
                    string kneedown_shoulder_angle = m_wndKneedownPostures.GetKneeDownShoulderAngle();
                    string standing_hip_angle = m_wndStandingPostures.GetStandingHipAngle();
                    string kneedown_hip_angle = m_wndKneedownPostures.GetKneeDownHipAngle();
                    string standing_right_Knee_Ankle = m_wndStandingPostures.GetStandingRightKneeAngle();
                    string kneedown_right_Knee_Ankle = m_wndKneedownPostures.GetKneeDownRightKneeAnkle();
                    string standing_left_Knee_Ankle = m_wndStandingPostures.GetStandingLeftKneeAngle();
                    string kneedown_left_Knee_Ankle = m_wndKneedownPostures.GetKneeDownLeftKneeAnkle();

                    //**********************************************inserting values for standing position in standing table**************************//

                    double standing_Glabella_X = joint_edit.m_FrontBodyPositionStanding.GetGlabellaX();
                    //MessageBox.Show("Glabella_X>> " + standing_Glabella_X);
                    double standing_Glabella_Y = joint_edit.m_FrontBodyPositionStanding.GetGlabellaY();
                    double standing_chin_X = joint_edit.m_FrontBodyPositionStanding.GetChinX();
                    double standing_chin_Y = joint_edit.m_FrontBodyPositionStanding.GetChinX();
                    double standing_leftKnee_X = joint_edit.m_FrontBodyPositionStanding.GetLeftKneeX();
                    double standing_leftKnee_Y = joint_edit.m_FrontBodyPositionStanding.GetLeftKneeY();
                    double standing_rightKnee_X = joint_edit.m_FrontBodyPositionStanding.GetRightKneeX();
                    double standing_rightKnee_Y = joint_edit.m_FrontBodyPositionStanding.GetRightKneeY();
                    double standing_leftShoulder_X = joint_edit.m_FrontBodyPositionStanding.GetLeftShoulderX();
                    double standing_leftShoulder_Y = joint_edit.m_FrontBodyPositionStanding.GetLeftShoulderY();
                    double standing_rightShoulder_X = joint_edit.m_FrontBodyPositionStanding.GetRightShoulderX();
                    double standing_rightShoulder_Y = joint_edit.m_FrontBodyPositionStanding.GetRightShoulderY();
                    double standing_leftEar_X = joint_edit.m_FrontBodyPositionStanding.GetLeftEarX();
                    double standing_leftEar_Y = joint_edit.m_FrontBodyPositionStanding.GetLeftEarY();
                    double standing_rightEar_X = joint_edit.m_FrontBodyPositionStanding.GetRightEarX();
                    double standing_rightEar_Y = joint_edit.m_FrontBodyPositionStanding.GetRightEarY();
                    double standing_leftAnkle_X = joint_edit.m_FrontBodyPositionStanding.GetRightAnkleX();
                    double standing_leftAnkleY = joint_edit.m_FrontBodyPositionStanding.GetLeftAnkleX();
                    double standing_rightAnkle_X = joint_edit.m_FrontBodyPositionStanding.GetRightAnkleX();
                    double standing_rightAnkle_Y = joint_edit.m_FrontBodyPositionStanding.GetRightAnkleY();
                    double standing_leftHip_X = joint_edit.m_FrontBodyPositionStanding.GetLeftHipX();
                    double standing_leftHip_Y = joint_edit.m_FrontBodyPositionStanding.GetLeftHipY();
                    double standing_rightHip_X = joint_edit.m_FrontBodyPositionStanding.GetRightHipX();
                    double standing_rightHip_Y = joint_edit.m_FrontBodyPositionStanding.GetRightHipY();
                    double standing_leftBelt_X = joint_edit.m_FrontBodyPositionStanding.GetLeftBeltX();
                    double standing_leftBelt_Y = joint_edit.m_FrontBodyPositionStanding.GetLeftBeltY();
                    double standing_rightBelt_X = joint_edit.m_FrontBodyPositionStanding.GetRightBeltX();
                    double standing_rightBelt_Y = joint_edit.m_FrontBodyPositionStanding.GetRightBeltY();
                    bool standing_underbodyPositiondetected = joint_edit.m_FrontBodyPositionStanding.IsUnderBodyPositionDetected();
                    bool standing_upperbodyPositiondetected = joint_edit.m_FrontBodyPositionStanding.IsUpperBodyPositionDetected();
                    bool standing_kneePositiondetected = joint_edit.m_FrontBodyPositionStanding.IsKneePositionDetected();
                    int standing_benchmark_dist = joint_edit.GetBenchmarkDistance();
                    string strYear = string.Empty, strMonth = string.Empty, strDay = string.Empty, strTime = string.Empty;
                    string measureTime = joint_edit.GetDataMeasurementTime();
                    if (measureTime != null)
                    {
                        joint_edit.GetDataAcqDate(ref strYear, ref strMonth, ref strDay, ref strTime);
                    }
                    else
                    {
                        //		strYear		= time.Format("%y");
                        strYear = DateTime.Now.Year.ToString();
                        //		strMonth	= time.Format("%m");
                        strMonth = DateTime.Now.Month.ToString();
                        strDay = DateTime.Now.Day.ToString();
                        strTime = DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString();
                    }
                    string db_str_TmpTime;
                    db_str_TmpTime = strYear + "-" + strMonth + " - " + strDay;

                    DateTime db_strTmp_Time = DateTime.Parse(db_str_TmpTime);

                    var db_strTmpTimes = db_strTmp_Time.ToString("yyyy-MM-dd");



                    string sYear = string.Empty, sMonth = string.Empty, sDay = string.Empty;
                    joint_edit.GetDataDoB(ref sYear, ref sMonth, ref sDay);
                    string date_Birth = sYear + "-" + sMonth + "-" + sDay;

                    DateTime date_birth = DateTime.Parse(date_Birth);

                    var dateBirth = date_birth.ToString("yyyy-MM-dd");



                    //****************************************************************inserting values for kneedwon postion***********************//


                    double kneedown_glabella_X = joint_edit.m_FrontBodyPositionKneedown.GetGlabellaX();
                    //MessageBox.Show("kneedown_glabella_X>> " + kneedown_glabella_X);
                    double kneedown_glabella_Y = joint_edit.m_FrontBodyPositionKneedown.GetGlabellaY();
                    double kneedown_chin_X = joint_edit.m_FrontBodyPositionKneedown.GetChinX();
                    double kneedown_chin_Y = joint_edit.m_FrontBodyPositionKneedown.GetChinY();
                    double kneedown_left_Ear_X = joint_edit.m_FrontBodyPositionKneedown.GetLeftEarX();
                    double knnedwon_left_Ear_Y = joint_edit.m_FrontBodyPositionKneedown.GetLeftEarY();
                    double kneedown_right_Ear_X = joint_edit.m_FrontBodyPositionKneedown.GetRightEarX();
                    double kneedown_right_Ear_Y = joint_edit.m_FrontBodyPositionKneedown.GetRightEarY();
                    double kneedown_left_shoulder_X = joint_edit.m_FrontBodyPositionKneedown.GetLeftShoulderX();
                    double kneedown_left_shoulder_Y = joint_edit.m_FrontBodyPositionKneedown.GetLeftShoulderY();
                    double kneedown_right_shoulder_X = joint_edit.m_FrontBodyPositionKneedown.GetRightShoulderX();
                    double kneedown_right_shoulder_Y = joint_edit.m_FrontBodyPositionKneedown.GetRightShoulderY();
                    double kneedown_left_belt_X = joint_edit.m_FrontBodyPositionKneedown.GetLeftBeltX();
                    double kneedown_left_belt_Y = joint_edit.m_FrontBodyPositionKneedown.GetLeftBeltY();
                    double kneedown_right_belt_X = joint_edit.m_FrontBodyPositionKneedown.GetRightBeltX();
                    double kneedown_right_belt_Y = joint_edit.m_FrontBodyPositionKneedown.GetRightBeltY();
                    double kneedown_left_ankle_X = joint_edit.m_FrontBodyPositionKneedown.GetLeftAnkleX();
                    double kneedown_left_ankle_Y = joint_edit.m_FrontBodyPositionKneedown.GetLeftAnkleY();
                    double kneedown_right_ankle_X = joint_edit.m_FrontBodyPositionKneedown.GetRightAnkleX();
                    double kneedown_right_ankle_Y = joint_edit.m_FrontBodyPositionKneedown.GetRightAnkleY();
                    double kneedown_left_hip_X = joint_edit.m_FrontBodyPositionKneedown.GetLeftHipX();
                    double kneedown_left_hip_Y = joint_edit.m_FrontBodyPositionKneedown.GetLeftHipY();
                    double kneedown_right_hip_X = joint_edit.m_FrontBodyPositionKneedown.GetRightHipX();
                    double kneedown_right_hip_Y = joint_edit.m_FrontBodyPositionKneedown.GetRightHipY();
                    double kneedown_left_knee_X = joint_edit.m_FrontBodyPositionKneedown.GetLeftKneeX();
                    double kneedown_left_knee_Y = joint_edit.m_FrontBodyPositionKneedown.GetLeftKneeY();
                    double kneedown_right_knee_X = joint_edit.m_FrontBodyPositionKneedown.GetRightAnkleX();
                    double kneedown_right_knee_Y = joint_edit.m_FrontBodyPositionKneedown.GetRightKneeY();
                    bool kneedown_underbodyPositiondetected = joint_edit.m_FrontBodyPositionKneedown.IsUnderBodyPositionDetected();
                    bool kneedown_upperbodyPositiondetected = joint_edit.m_FrontBodyPositionKneedown.IsUpperBodyPositionDetected();
                    bool kneedown_kneePositiondetected = joint_edit.m_FrontBodyPositionKneedown.IsKneePositionDetected();
                    int kneedown_benchmark_dist = joint_edit.GetBenchmarkDistance();

                    ////***********************************Compress Image for database******************************//


                    VaryQualityLevel_standing();
                    VaryQualityLevel_sidepic();
                    VaryQualityLevel_crouched();

                    //******************************Database connection***************************//
                    byte[] standing_data = null;
                    byte[] crouched_datas = null;
                    byte[] side_data = null;

                    try
                    {

                        if (File.Exists("yugamirus.sqlite"))
                        {
                            //Console.WriteLine("File Exists");
                        }
                        else
                        {
                            SQLiteConnection.CreateFile("yugamirus.sqlite");
                            yugamiru_con = new SQLiteConnection("DataSource=yugamirus.sqlite;version=3");
                            yugamiru_con.Open();
                            string sql = @"CREATE TABLE tblmaster_yugamiru (Sideimage BLOB(80), 
                                     Uprightimage BLOB(80),Crouchedimage BLOB(80),URglabellapos VARCHAR(50),URearangle VARCHAR(50),
                                     URearpos VARCHAR(50),URshoulderangle VARCHAR(50),URchinpos VARCHAR(50),URhipangle VARCHAR(50),
                                    URshoulderpos VARCHAR(50),URrightkneeangle VARCHAR(50),
                                   URhippos VARCHAR(50),URleftkneeangle VARCHAR(50),Cglabellapos VARCHAR(50),Cearangle VARCHAR(50),
                                   Cearpos VARCHAR(50),Cshoulderangle VARCHAR(50),Cchinpos VARCHAR(50),Chipangle VARCHAR(50),
                                    Cshoulderpos VARCHAR(50),Crightkneeangle VARCHAR(50),Chippos VARCHAR(50),Cleftkneeangle VARCHAR(50),
                                       Patientname VARCHAR(50),PatientID VARCHAR(50), PatientGender VARCHAR(30),
                                   PatientDOB TEXT(20),Score INTEGER(20),Computerid VARCHAR(50),Computername VARCHAR(50), 
                                  Rank VARCHAR(20),Height INTEGER(50),Dateofcreation TEXT(40)) ";

                            string standing_query = @"CREATE TABLE standing_table (
                            PatientID VARCHAR(50),Uprightimage BLOB(80),measurement_time TEXT(40),benchmark_distance
                            INT (40) ,URhip_X DOUBLE(50),URhip_Y DOUBLE(50),ULhip_X DOUBLE(50),ULhip_Y DOUBLE(50),
                        URightknee_X DOUBLE(50),URightknee_Y DOUBLE(50),ULeftknee_X DOUBLE(50),ULeftknee_Y DOUBLE(50),
                        URankle_X DOUBLE(50),URankle_Y DOUBLE(50),ULankle_X DOUBLE(50),ULankle_Y DOUBLE(50),URightbelt_X DOUBLE(50),
                        URightbelt_Y DOUBLE(50),ULeftbelt_X DOUBLE(50),ULeftbelt_Y DOUBLE(50),URightshoulder_X DOUBLE(50),
                        URightshoulder_Y DOUBLE(50),ULeftshoulder_X DOUBLE(50),ULeftshoulder_Y DOUBLE(50),URightear_X DOUBLE(50),
                        URightear_Y DOUBLE(50),ULeftear_X DOUBLE(50),ULeftear_Y DOUBLE(50),Uchin_X DOUBLE(50),Uchin_Y DOUBLE(50),
                        Uglabella_X DOUBLE(50),Uglabella_Y DOUBLE(50),Uunderbodyposition_detected VARCHAR(50) DEFAULT (null) ,
                        Ukneeposition_detected VARCHAR(50) DEFAULT (null) ,Uuperbodyposition_detected VARCHAR(50) DEFAULT (null))";

                            string crouched_query = @"CREATE TABLE kneedown_table (
                            PatientID VARCHAR(50),Crouchedimage BLOB(80),measurement_time TEXT(40),
                        benchmark_distance INT(40),CRhip_X DOUBLE(50),CRhip_Y DOUBLE(50),CLhip_X DOUBLE(50),
                        CLhip_Y DOUBLE(50),CRightknee_X DOUBLE(50),CRightknee_Y DOUBLE(50),CLeftknee_X DOUBLE(50),CLeftknee_Y DOUBLE(50),
                        CRankle_X DOUBLE(50),CRankle_Y DOUBLE(50),CLankle_X DOUBLE(50),CLankle_Y DOUBLE(50),CRightbelt_X DOUBLE(50),
                        CRightbelt_Y DOUBLE(50),CLeftbelt_X DOUBLE(50),CLeftbelt_Y DOUBLE(50),CRightshoulder_X DOUBLE(50),
                        CRightshoulder_Y DOUBLE(50),CLeftshoulder_X DOUBLE(50),CLeftshoulder_Y DOUBLE(50),CRightear_X DOUBLE(50),
                        CRightear_Y DOUBLE(50),CLeftear_X DOUBLE(50),CLeftear_Y DOUBLE(50),Cchin_X DOUBLE(50),Cchin_Y DOUBLE(50),
                        Cglabella_X DOUBLE(50),Cglabella_Y DOUBLE(50),Cunderbodyposition_detected VARCHAR DEFAULT (50) ,
                        Ckneeposition_detected VARCHAR DEFAULT (50) ,Cuperbodyposition_detected VARCHAR DEFAULT (50))";

                            SQLiteCommand command = new SQLiteCommand(sql, yugamiru_con);
                            SQLiteCommand cmd = new SQLiteCommand(standing_query, yugamiru_con);
                            SQLiteCommand conn = new SQLiteCommand(crouched_query, yugamiru_con);
                            command.ExecuteNonQuery();
                            cmd.ExecuteNonQuery();
                            conn.ExecuteNonQuery();
                            yugamiru_con.Close();
                            System.Windows.Forms.MessageBox.Show("Data Inserted Successfully");
                        }
                    }
                    catch (SQLiteException ex)
                    {
                        Logger.LogError(ex);
                    }


                    try
                    {
                        side_data = File.ReadAllBytes(System.Windows.Forms.Application.StartupPath + @"\sidepic.JPG");
                        standing_data = File.ReadAllBytes(System.Windows.Forms.Application.StartupPath + @"\standing.JPG");
                        crouched_datas = File.ReadAllBytes(System.Windows.Forms.Application.StartupPath + @"\crouched.JPG");

                        yugamiru_con = new SQLiteConnection("DataSource=yugamirus.sqlite;version=3");
                        yugamiru_con.Open();
                        string query = @"insert into tblmaster_yugamiru (Uprightimage, Crouchedimage, Sideimage, PatientID, Patientname, Height, PatientGender, PatientDOB, URglabellapos, 
                             Cglabellapos, URearpos, Cearpos, URchinpos, Cchinpos, URshoulderpos, Cshoulderpos, URhippos, Chippos, URearangle, Cearangle, URshoulderangle, Cshoulderangle, URhipangle, Chipangle, URrightkneeangle, Crightkneeangle, URleftkneeangle, Cleftkneeangle, Dateofcreation) 
                             values(@standing_image, @kneedown_image, @side_image, '" + Id + "','" + name + "','" + height + "','" + gender + "','" + dateBirth + "','" + standing_glabella_pos + "','" + kneeDown_glabella_pos + "','" + standing_ear_pos + "','" + kneeDown_ear_pos + "','" + standing_chin_pos + "','" + kneedown_chin_pos + "','"
                                   + standing_shoulder_pos + "','" + kneedown_shoulder_pos + "','" + standing_hip_pos + "','" + kneedown_hip_pos + "','" + standing_ear_angle + "','" + kneedown_ear_angle + "','" + standing_shoulder_angle + "','" + kneedown_shoulder_angle + "','"
                                   + standing_hip_angle + "','" + kneedown_hip_angle + "','" + standing_right_Knee_Ankle + "','" + kneedown_right_Knee_Ankle + "','" + standing_left_Knee_Ankle + "','" + kneedown_left_Knee_Ankle + "', '" + db_strTmpTimes + "')";

                        string standing_sql = @"insert into standing_table(PatientID, Uprightimage, measurement_time, benchmark_distance, 
                       URhip_X, URhip_Y, ULhip_X, ULhip_Y, URightknee_X, URightknee_Y, ULeftknee_X, ULeftknee_Y, URankle_X, URankle_Y,
                       ULankle_X, ULankle_Y, URightbelt_X, URightbelt_Y, ULeftbelt_X, ULeftbelt_Y, URightshoulder_X, URightshoulder_Y, ULeftshoulder_X, 
                       ULeftshoulder_Y, URightear_X, URightear_Y, ULeftear_X, ULeftear_Y, Uchin_X, Uchin_Y, Uglabella_X, Uglabella_Y,
                       Uunderbodyposition_detected, Ukneeposition_detected, Uuperbodyposition_detected )
                       values('" + Id + "', @standing_image,'" + db_strTmpTimes + "','" + standing_benchmark_dist + "','" + standing_rightHip_X + "','" + standing_rightHip_Y + "','" + standing_leftHip_X + "','" + standing_leftHip_Y + "','"
                             + standing_rightKnee_X + "','" + standing_rightKnee_Y + "','" + standing_leftKnee_X + "','" + standing_leftKnee_Y + "','" + standing_rightAnkle_X + "','"
                             + standing_rightAnkle_Y + "','" + standing_leftAnkle_X + "','" + standing_leftAnkleY + "','" + standing_rightBelt_X + "','" + standing_rightBelt_Y + "','"
                             + standing_leftBelt_X + "','" + standing_leftBelt_Y + "','" + standing_rightShoulder_X + "','" + standing_rightShoulder_Y + "','" + standing_leftShoulder_X + "','" + standing_leftShoulder_Y + "','"
                             + standing_rightEar_X + "','" + standing_rightEar_Y + "','" + standing_leftEar_X + "','" + standing_leftEar_Y + "','" + standing_chin_X + "','" + standing_chin_Y + "','" + standing_Glabella_X + "','"
                             + standing_Glabella_Y + "','" + standing_underbodyPositiondetected + "','" + kneedown_upperbodyPositiondetected + "','" + standing_kneePositiondetected + "')";

                        string crouched_sql = @"insert into kneedown_table(PatientID, Crouchedimage, measurement_time, benchmark_distance, 
                       CRhip_X, CRhip_Y, CLhip_X, CLhip_Y, CRightknee_X, CRightknee_Y, CLeftknee_X, CLeftknee_Y, CRankle_X, CRankle_Y,
                       CLankle_X, CLankle_Y, CRightbelt_X, CRightbelt_Y, CLeftbelt_X, CLeftbelt_Y, CRightshoulder_X, CRightshoulder_Y, CLeftshoulder_X, 
                       CLeftshoulder_Y, CRightear_X, CRightear_Y, CLeftear_X, CLeftear_Y, Cchin_X, Cchin_Y, Cglabella_X, Cglabella_Y,
                       Cunderbodyposition_detected, Ckneeposition_detected, Cuperbodyposition_detected )
                       values('" + Id + "', @kneedown_image, '" + db_strTmpTimes + "','" + kneedown_benchmark_dist + "','" + kneedown_right_hip_X + "','" + kneedown_right_hip_Y + "','" + kneedown_left_hip_X + "','" + kneedown_left_hip_Y + "','"
                                      + kneedown_left_knee_X + "','" + kneedown_left_knee_Y + "','" + kneedown_right_knee_X + "','" + kneedown_right_knee_X + "','" + kneedown_right_ankle_X + "','"
                                      + kneedown_right_ankle_Y + "','" + kneedown_left_ankle_X + "','" + kneedown_left_ankle_Y + "','" + kneedown_right_belt_X + "','" + kneedown_right_belt_Y + "','"
                                      + kneedown_left_belt_X + "','" + kneedown_left_belt_Y + "','" + kneedown_right_shoulder_X + "','" + kneedown_right_shoulder_Y + "','" + kneedown_left_shoulder_X + "','" + kneedown_left_shoulder_Y + "','"
                                      + kneedown_right_Ear_X + "','" + kneedown_right_Ear_Y + "','" + kneedown_left_Ear_X + "','" + knnedwon_left_Ear_Y + "','" + kneedown_chin_X + "','" + kneedown_chin_Y + "','" + kneedown_glabella_X + "','"
                                      + kneedown_glabella_Y + "','" + kneedown_underbodyPositiondetected + "','" + standing_upperbodyPositiondetected + "','" + kneedown_kneePositiondetected + "')";

                        SQLiteCommand cmd = new SQLiteCommand(query, yugamiru_con);
                        cmd.Parameters.Add("@standing_image", DbType.Binary).Value = standing_data;
                        cmd.Parameters.Add("@kneedown_image", DbType.Binary).Value = crouched_datas;
                        cmd.Parameters.Add("@side_image", DbType.Binary).Value = side_data;
                        cmd.ExecuteNonQuery();

                        SQLiteCommand conn = new SQLiteCommand(standing_sql, yugamiru_con);
                        conn.Parameters.Add("@standing_image", DbType.Binary).Value = standing_data;
                        conn.ExecuteNonQuery();

                        SQLiteCommand com = new SQLiteCommand(crouched_sql, yugamiru_con);
                        com.Parameters.Add("@kneedown_image", DbType.Binary).Value = crouched_datas;
                        com.ExecuteNonQuery();

                        yugamiru_con.Close();
                        System.Windows.Forms.MessageBox.Show("Data Inserted Successfully");

                    }
                    catch (SQLiteException ex)
                    {
                        Logger.LogError(ex);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                }

            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Please insert images");
            }
        }
    }

}
