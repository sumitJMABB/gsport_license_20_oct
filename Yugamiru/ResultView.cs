﻿using System;
using System.Collections.Generic;
using System.IO;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Yugamiru.stretchDIBbits;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.Drawing.Drawing2D;
using System.Diagnostics;


namespace Yugamiru
{
    public partial class ResultView : Form
    {
        
        PictureBox m_MainPic = new PictureBox();
        public JointEditDoc m_JointEditDoc;
        FrontBodyResultWnd m_wndStandingPosture;
        FrontBodyResultWnd m_wndKneedownPosture;
        MyData m_BalanceLabData = new MyData();
        internal PrintPreviewDialog PrintPreviewDialog1;
        private PrintDocument document = new PrintDocument();
      
      


         FrontBodyAngle FrontBodyAngleStanding = new FrontBodyAngle();
        FrontBodyAngle FrontBodyAngleKneedown = new FrontBodyAngle();
        SideBodyAngle m_SideBodyAngle = new SideBodyAngle();
        bool m_SecondPageFlag = false;

        SymbolFunc m_SymbolFunc = new SymbolFunc();

        int NextPageNum = 1;
        string m_CL_Path = Constants.path + Yugamiru.Properties.Resources.CURRENT_LANGUAGE;

        public Font printFont =
               new Font("MS Gothic", 11,
               FontStyle.Regular);
        
~ResultView()
        {
            
            
        }
        public ResultView(JointEditDoc GetDocument)
        {
            // Declare a PrintDocument object named document.
            InitializeComponent();
            m_JointEditDoc = GetDocument;


            m_wndStandingPosture = new FrontBodyResultWnd();
            m_wndKneedownPosture = new FrontBodyResultWnd();
     




            m_MainPic.Size = new Size(Yugamiru.Properties.Resources.Mainpic6.Size.Width,
                Yugamiru.Properties.Resources.Mainpic6.Size.Height);
            m_MainPic.BackColor = Color.Transparent;
            this.Controls.Add(m_MainPic);
            m_MainPic.Image = Yugamiru.Properties.Resources.Mainpic6;

            IDC_Mag1Btn.Image = Yugamiru.Properties.Resources.mag2;
            IDC_Mag2Btn.Image = Yugamiru.Properties.Resources.mag1;
            IDC_ResetImgBtn.Image = Yugamiru.Properties.Resources.mag3_down;

            IDC_BTN_NAMECHANGE.Image = Yugamiru.Properties.Resources.namechange_down;
            IDC_BTN_RETURNTOPMENU.Image = Yugamiru.Properties.Resources.returnstart_down;
            IDC_RemeasurementBtn.Image = Yugamiru.Properties.Resources.imagechange_down;
            IDC_EditBtn.Image = Yugamiru.Properties.Resources.jointedit_down;


            IDC_PrintBtn.Image = Yugamiru.Properties.Resources.reportprint_down;
            IDC_ScoresheetBtn.Image = Yugamiru.Properties.Resources.reportdisplay_down;
            IDC_BTN_DATASAVE.Image = Yugamiru.Properties.Resources.datasave_down;
            IDC_MeasurementEndBtn.Image = Yugamiru.Properties.Resources.startred_down;


            m_wndStandingPosture.SetBackgroundBitmap(1024, 1280, GetDocument.m_FrontStandingImageBytes);
            m_wndStandingPosture.SetDataVersion(GetDocument.GetDataVersion());
            m_wndStandingPosture.SetMarkerSize(GetDocument.GetMarkerSize());

            m_wndStandingPosture.SetMidLine(GetDocument.IsValidMidLine());
            m_wndStandingPosture.SetMidLineWidth(GetDocument.GetMidLineWidth());
            m_wndStandingPosture.SetMidLineColor(GetDocument.GetMidLineColor());
            m_wndStandingPosture.SetMidLineStyle(GetDocument.GetMidLineStyle());

            m_wndStandingPosture.SetStyleLine(GetDocument.IsValidStyleLine());
            m_wndStandingPosture.SetStyleLineStyle(GetDocument.GetStyleLineStyle());
            m_wndStandingPosture.SetStyleLineColor(GetDocument.GetStyleLineColor());
            m_wndStandingPosture.SetStyleLineWidth(GetDocument.GetStyleLineWidth());
           
            Font lf;
            //GetDocument.GetLogFont(lf);
            //m_wndStandingPosture.SetLogFont(lf);
            m_wndStandingPosture.SetFontColor(GetDocument.GetFontColor());
            m_wndStandingPosture.SetOutline(GetDocument.IsValidOutline());
            m_wndStandingPosture.SetOutlineColor(GetDocument.GetOutlineColor());

            int iBenchmarkDistance = GetDocument.GetBenchmarkDistance();
            SideBodyPosition SideBodyPosition = new SideBodyPosition();
            GetDocument.GetSideBodyPosition(ref SideBodyPosition);
            Point ptBenchmark1 = new Point(0, 0);
            Point ptBenchmark2 = new Point(0, 0);
            SideBodyPosition.GetBenchmark1Position(ref ptBenchmark1);
            SideBodyPosition.GetBenchmark2Position(ref ptBenchmark2);

            FrontBodyPosition FrontBodyPositionStanding = new FrontBodyPosition();
            //m_BalanceLabData.InitBodyBalance(1024, 1280, m_CalibInf, m_CalcPostureProp);
            //FrontBodyPositionStanding = m_BalanceLabData.m_FrontBodyPositionStanding;

            FrontBodyPositionStanding.SetAllPositionDetected();
            m_wndStandingPosture.SetFrontBodyPosition(GetDocument.m_FrontBodyPositionStanding);
            //GetDocument.SetStandingFrontBodyPosition(GetDocument.m_FrontBodyPositionStanding);

            GetDocument.CalcBodyAngleStanding(ref FrontBodyAngleStanding);
            m_wndStandingPosture.SetFrontBodyAngle(FrontBodyAngleStanding);
            FrontBodyResultData FrontBodyResultDataStanding = new FrontBodyResultData();
            GetDocument.CalcStandingFrontBodyResultData(ref FrontBodyResultDataStanding);

            m_wndStandingPosture.SetFrontBodyResultData(FrontBodyResultDataStanding);
            m_wndStandingPosture.SetBenchmark1Point(ptBenchmark1);
            m_wndStandingPosture.SetBenchmark2Point(ptBenchmark2);
            m_wndStandingPosture.SetBenchmarkDistance(iBenchmarkDistance);

            m_wndKneedownPosture.SetBackgroundBitmap(1024, 1280, GetDocument.m_FrontKneedownImageBytes);
            m_wndKneedownPosture.SetDataVersion(GetDocument.GetDataVersion());
            m_wndKneedownPosture.SetMarkerSize(GetDocument.GetMarkerSize());

            m_wndKneedownPosture.SetMidLine(GetDocument.IsValidMidLine());
            m_wndKneedownPosture.SetMidLineWidth(GetDocument.GetMidLineWidth());
            m_wndKneedownPosture.SetMidLineColor(GetDocument.GetMidLineColor());
            m_wndKneedownPosture.SetMidLineStyle(GetDocument.GetMidLineStyle());

            m_wndKneedownPosture.SetStyleLine(GetDocument.IsValidStyleLine());
            m_wndKneedownPosture.SetStyleLineStyle(GetDocument.GetStyleLineStyle());
            m_wndKneedownPosture.SetStyleLineColor(GetDocument.GetStyleLineColor());
            m_wndKneedownPosture.SetStyleLineWidth(GetDocument.GetStyleLineWidth());

            //GetDocument()->GetLogFont(&lf);
            //m_wndKneedownPosture.SetLogFont(lf);
            m_wndKneedownPosture.SetFontColor(GetDocument.GetFontColor());
            m_wndKneedownPosture.SetOutline(GetDocument.IsValidOutline());
            m_wndKneedownPosture.SetOutlineColor(GetDocument.GetOutlineColor());

            FrontBodyPosition FrontBodyPositionKneedown = new FrontBodyPosition();
            //FrontBodyPositionKneedown = m_BalanceLabData.m_FrontBodyPositionKneedown;

            FrontBodyPositionKneedown.SetAllPositionDetected();
            m_wndKneedownPosture.SetFrontBodyPosition(GetDocument.m_FrontBodyPositionKneedown);
            //GetDocument.SetKneedownFrontBodyPosition(GetDocument.m_FrontBodyPositionKneedown);

            GetDocument.CalcBodyAngleKneedown(ref FrontBodyAngleKneedown);
            m_wndKneedownPosture.SetFrontBodyAngle(FrontBodyAngleKneedown);

            GetDocument.CalcBodyAngleSide(ref m_SideBodyAngle);

            FrontBodyResultData FrontBodyResultDataKneedown = new FrontBodyResultData();
            GetDocument.CalcKneedownFrontBodyResultData(ref FrontBodyResultDataKneedown);
            m_wndKneedownPosture.SetFrontBodyResultData(FrontBodyResultDataKneedown);
            m_wndKneedownPosture.SetBenchmark1Point(ptBenchmark1);
            m_wndKneedownPosture.SetBenchmark2Point(ptBenchmark2);
            m_wndKneedownPosture.SetBenchmarkDistance(iBenchmarkDistance);

            string strID = string.Empty;
            IDC_ID.Text = GetDocument.GetDataID().ToString();             // ID
            IDC_Name.Text = GetDocument.GetDataName();         // –¼‘O
            string str = "-";
            if (GetDocument.GetDataGender() == 1) str = "M";
            if (GetDocument.GetDataGender() == 2) str = "F";
            IDC_Gender.Text = str;   // «•Ê

            string sYear = string.Empty, sMonth = string.Empty, sDay = string.Empty;
            GetDocument.GetDataDoB(ref sYear, ref sMonth, ref sDay);
            str = sMonth + "." + sDay + " " + sYear;
            IDC_DoB.Text = str;                               // ¶”NŒŽ“ú

            if (GetDocument.GetDataHeight() == 0)
                str = "-";  // g’·
            else
                str = GetDocument.GetDataHeight().ToString();
            IDC_Height.Text = str;                            // g’·					

            string strInstitutionName = string.Empty;
            GetDocument.GetInstitutionName(strInstitutionName);
            //IDC_InstitutionName, strInstitutionName);    // Ž{Ý–¼

            //((CEdit*)GetDlgItem(IDC_CommentField))->SetLimitText(300);

            string strComment = GetDocument.GetDataComment();
            if (strComment.Length == 0)
            {
                string strFixedComment = string.Empty;
                GetDocument.GetFixedComment(strFixedComment);
                IDC_CommentField.Text = strFixedComment;  // ’èŒ`•¶
            }
            else
            {
                IDC_CommentField.Text = strComment;   // ƒRƒƒ“ƒg—“
            }

            // ƒGƒfƒBƒbƒgƒ{ƒbƒNƒX‚ÍƒtƒHƒ“ƒg‚ðÝ’è‚µ‚È‚¨‚·.
            {
                /*     CWnd* pWnd = GetDlgItem(IDC_CommentField);
                     if (pWnd != NULL)
                     {
                         pWnd->SetFont(&m_FontEditBox);
                     }*/
            }

            // ”»’è•¶
            string strJudge = string.Empty;
            if (m_BalanceLabData.IsDetected(FrontBodyPositionStanding, FrontBodyPositionKneedown))
            {
                FrontBodyResultData temp_FrontBodyResultDataStanding = new FrontBodyResultData();
                FrontBodyResultData temp_FrontBodyResultDataKneedown = new FrontBodyResultData();
                SideBodyResultData temp_SideBodyResultData = new SideBodyResultData();
                GetDocument.CalcStandingFrontBodyResultData(ref FrontBodyResultDataStanding);
                GetDocument.CalcKneedownFrontBodyResultData(ref FrontBodyResultDataKneedown);
                GetDocument.CalcSideBodyResultData(ref temp_SideBodyResultData);
                JudgementResult JudgementResultStanding = new JudgementResult(FrontBodyResultDataStanding, temp_SideBodyResultData);
                JudgementResult JudgementResultKneedown = new JudgementResult(FrontBodyResultDataKneedown, temp_SideBodyResultData);
                JudgementMessageInfo JudgementMessageInfoStanding = new JudgementMessageInfo(JudgementResultStanding);
                JudgementMessageInfo JudgementMessageInfoKneedown = new JudgementMessageInfo(JudgementResultKneedown);
                string str1 = string.Empty;
                string str2 = string.Empty;
                //# ifdef LANG_EN
                //JudgementMessageInfoStanding.MakeString(str1, "", GetDocument.IsScoresheetDetail());
                /*                strJudge = str1;
                #else
                #if defined(PEDX)
                        JudgementMessageInfoStanding.MakeString( str1, "StandingF", GetDocument()->IsScoresheetDetail() );
                        JudgementMessageInfoKneedown.MakeString( str2, "Walking F", GetDocument()->IsScoresheetDetail() );
                #else*/
                JudgementMessageInfoStanding.MakeString(ref str1, /*"Standing:"*/Yugamiru.Properties.Resources.KEY_STAND, GetDocument.IsScoresheetDetail());
                JudgementMessageInfoKneedown.MakeString(ref str2, /*"Bending:"*/Yugamiru.Properties.Resources.KEY_BEND, GetDocument.IsScoresheetDetail());
                strJudge = str1 + "\r\n\r\n" + str2;
                /*
#endif
                strJudge = str1 + "\r\n\r\n" + str2;
#endif*/
            }
            else
            {
                strJudge = Yugamiru.Properties.Resources.MUSCLEREPORT17; //"Without specification of the joints' positions, posture evaluation is not available.";
            }
            IDC_EDIT1.Text = strJudge;

            int iBackgroundWidth = m_wndStandingPosture.GetBackgroundWidth();
            int iBackgroundHeight = m_wndStandingPosture.GetBackgroundHeight();
            int iSrcWidth = (int)(iBackgroundWidth / GetDocument.GetImgMag());
            int iSrcHeight = (int)(iBackgroundHeight / GetDocument.GetImgMag());
            int iSrcX = iBackgroundWidth / 2 - iSrcWidth / 2;
            int iSrcY = iBackgroundHeight / 2 - iSrcHeight / 2;
            m_wndStandingPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndStandingPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            m_wndKneedownPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndKneedownPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            m_wndStandingPosture.SetArrowLength(GetDocument.GetArrowLength());
            m_wndStandingPosture.SetArrowWidth(GetDocument.GetArrowWidth());
            m_wndStandingPosture.SetArrowInvisible(GetDocument.IsArrowInvisible());
            m_wndStandingPosture.SetLabelInvisible(GetDocument.IsLabelInvisible());
            //m_wndStandingPosture.UpdateOffscreen(m_pDCOffscreen);
            m_wndKneedownPosture.SetArrowLength(GetDocument.GetArrowLength());
            m_wndKneedownPosture.SetArrowWidth(GetDocument.GetArrowWidth());
            m_wndKneedownPosture.SetArrowInvisible(GetDocument.IsArrowInvisible());
            m_wndKneedownPosture.SetLabelInvisible(GetDocument.IsLabelInvisible());
            //m_wndKneedownPosture.UpdateOffscreen();

            // ƒXƒ‰ƒCƒ_[
            /*  CSliderCtrl* pSB1 = (CSliderCtrl*)GetDlgItem(IDC_SLIDER1);
              pSB1->SetRange(int(IMG_SCALE_MIN * 100), int(IMG_SCALE_MAX * 100));
              //	pSB1->SetRange(int(IMG_SCALE_MAX*100),int(IMG_SCALE_MIN*100));
              pSB1->SetPos(int(IMG_SCALE_MAX * 100 + IMG_SCALE_MIN * 100 - GetDocument()->GetImgMag() * 100));
              pSB1->SetTic(50);
              */

            IDC_SLIDER1.SetRange((int)(Constants.IMG_SCALE_MIN * 100), (int)(Constants.IMG_SCALE_MAX * 100));
            IDC_SLIDER1.TickFrequency = 50;
            IDC_SLIDER1.Value = (int)(Constants.IMG_SCALE_MIN * 100);/*(int)(Constants.IMG_SCALE_MAX * 100 +
                Constants.IMG_SCALE_MIN * 100 - GetDocument.GetImgMag() * 100);*/

        }

        public void ResultView_SizeChanged(object sender, EventArgs e)
        {
            //--to centre the picture box while resizing the form
            m_MainPic.Left = (this.ClientSize.Width - m_MainPic.Width) / 2;
            m_MainPic.Top = 0;//(this.ClientSize.Height - m_MainPic.Height) / 2; ;

            pictureBox_Standing.Size = new Size(304, 380);
            pictureBox_Standing.Location = new Point(120 + 170, 50 + 35);
            pictureBox_Standing.Left = m_MainPic.Left + 90;

            pictureBox_KneeDown.Size = new Size(304, 380);
            pictureBox_KneeDown.Location = new Point(520 + 170, 50 + 35);
            pictureBox_KneeDown.Left = pictureBox_Standing.Left + pictureBox_Standing.Width + 90;

            IDC_BTN_NAMECHANGE.Size = new Size(112, 42);
            IDC_BTN_NAMECHANGE.Location = new Point(100 + 70, 600 + 30);
            IDC_BTN_NAMECHANGE.Left = m_MainPic.Left +30;

            IDC_RemeasurementBtn.Size = new Size(112, 42); //uÄ‘ª’èvƒ{ƒ^ƒ“.
            IDC_RemeasurementBtn.Location = new Point(220 + 70, 600 + 30);
            IDC_RemeasurementBtn.Left = IDC_BTN_NAMECHANGE.Left + IDC_BTN_NAMECHANGE.Width+ 2;

            IDC_EditBtn.Size = new Size(112, 42);//uŠÖßˆÊ’uvƒ{ƒ^ƒ“.
            IDC_EditBtn.Location = new Point(340 + 70, 600 + 30);
            IDC_EditBtn.Left = IDC_RemeasurementBtn.Left + IDC_RemeasurementBtn.Width +2;

            IDC_ScoresheetBtn.Size = new Size(112, 42); //uƒŒƒ|[ƒgvƒ{ƒ^ƒ“.
            IDC_ScoresheetBtn.Location = new Point(460 + 70, 600 + 30);
            IDC_ScoresheetBtn.Left = IDC_EditBtn.Left + IDC_EditBtn.Width + 2;

            IDC_PrintBtn.Size = new Size(112, 42); //uˆóüvƒ{ƒ^ƒ“. 
            IDC_PrintBtn.Location = new Point(580 + 70, 600 + 30); //uˆóüvƒ{ƒ^ƒ“. 
            IDC_PrintBtn.Left = IDC_ScoresheetBtn.Left + IDC_ScoresheetBtn.Width + 2;

            IDC_BTN_DATASAVE.Size = new Size(112, 42);
            IDC_BTN_DATASAVE.Location = new Point(700 + 70, 600 + 30);
            IDC_BTN_DATASAVE.Left = IDC_PrintBtn.Left + IDC_PrintBtn.Width + 2;

            IDC_MeasurementEndBtn.Size = new Size(112, 42); //u–ß‚évƒ{ƒ^ƒ“i‘ª’èI—¹j.
            IDC_MeasurementEndBtn.Location = new Point(820 + 70, 600 + 30);
            IDC_MeasurementEndBtn.Left = IDC_BTN_DATASAVE.Left + IDC_BTN_DATASAVE.Width + 2;

            IDC_BTN_RETURNTOPMENU.Size = new Size(112, 42);
            IDC_BTN_RETURNTOPMENU.Location = new Point(940 + 70, 600 + 30);
            IDC_BTN_RETURNTOPMENU.Left = IDC_MeasurementEndBtn.Left + IDC_MeasurementEndBtn.Width + 2;
            IDC_CommentField.Size = new Size(346, 116 + 6);
            IDC_CommentField.Location = new Point(200 + 64, 500 + 2);
            IDC_CommentField.Left = pictureBox_Standing.Left - 26;

            IDC_EDIT1.Size = new Size(468 + 6, 116 + 6);
            IDC_EDIT1.Location = new Point(600 + 28, 500 + 2);
            IDC_EDIT1.Left = IDC_CommentField.Left + IDC_CommentField.Width + 20;

            IDC_Mag1Btn.Size = new Size(48, 48);
            IDC_Mag1Btn.Location = new Point(1110, 200 + 100 - 20);
            IDC_Mag1Btn.Left = IDC_EDIT1.Left + IDC_EDIT1.Width + 10;

            panel1.Size = new Size(42, 192);
            panel1.Location = new Point(1110, 250 + 100 - 20);
            panel1.Left = IDC_EDIT1.Left + IDC_EDIT1.Width + 10;

            IDC_SLIDER1.Size = new Size(38, 192);
            //IDC_SLIDER1.Location = new Point(1115, 250 - 50);

            IDC_Mag2Btn.Size = new Size(48, 48);
            IDC_Mag2Btn.Location = new Point(1110, 450 + 100 - 20);
            IDC_Mag2Btn.Left = IDC_EDIT1.Left + IDC_EDIT1.Width + 10;

            IDC_ResetImgBtn.Size = new Size(48, 48);
            IDC_ResetImgBtn.Location = new Point(1110, 500 + 100 - 20);
            IDC_ResetImgBtn.Left = IDC_EDIT1.Left + IDC_EDIT1.Width + 10;

          
            IDC_ID.Size = new Size(99 - 6, 24 + 2);
            IDC_ID.Location = new Point(200 + 70, 20);

            IDC_Name.Size = new Size(163 - 6, 24 + 2);
            IDC_Name.Location = new Point(310 + 70, 20);

            IDC_Gender.Size = new Size(29, 24 + 2);
            IDC_Gender.Location = new Point(480 + 74, 20);

            IDC_DoB.Size = new Size(119, 24 + 2);
            IDC_DoB.Location = new Point(600 + 20, 20);

            IDC_Height.Size = new Size(38, 24 + 2);
            IDC_Height.Location = new Point(720, 20);

            IDC_ID.Left = (this.Width - IDC_ID.Width) / 2 - 400;
            IDC_Name.Left = IDC_ID.Left + 100;
            IDC_Gender.Left = IDC_Name.Left + 170 + 4;
            IDC_DoB.Left = IDC_Gender.Left + 56 + 4;
            IDC_Height.Left = IDC_DoB.Left + 104;


        }

        private void pictureBox_Standing_Paint(object sender, PaintEventArgs e)
        {
            m_wndStandingPosture.m_pbyteBits = m_JointEditDoc.m_FrontStandingImageBytes;
            m_wndStandingPosture.m_iOffscreenWidth = 304;
            m_wndStandingPosture.m_iOffscreenHeight = 380;
            m_wndStandingPosture.UpdateOffscreen(e.Graphics);
        }

        private void pictureBox_KneeDown_Paint(object sender, PaintEventArgs e)
        {
            m_wndKneedownPosture.m_pbyteBits = m_JointEditDoc.m_FrontKneedownImageBytes;
            m_wndKneedownPosture.m_iOffscreenWidth = 304;
            m_wndKneedownPosture.m_iOffscreenHeight = 380;
            m_wndKneedownPosture.UpdateOffscreen(e.Graphics);
        }
        public void UpdatePictureScale(int iSliderPos)
        {
            int iSrcX = m_wndStandingPosture.GetSrcX();
            int iSrcY = m_wndStandingPosture.GetSrcY();
            int iSrcWidth = m_wndStandingPosture.GetSrcWidth();
            int iSrcHeight = m_wndStandingPosture.GetSrcHeight();

            int iSrcCenterX = iSrcX + iSrcWidth / 2;
            int iSrcCenterY = iSrcY + iSrcHeight / 2;

            /*CRect rcClient;
            m_wndStandingPosture.GetClientRect(&rcClient);*/

            int Val = 400 - (iSliderPos - 100);
            double dImgMag = (Constants.IMG_SCALE_MAX + Constants.IMG_SCALE_MIN) - Val / 100.0;//iSliderPos / 100.0;
            if (dImgMag < Constants.IMG_SCALE_MIN)
            {
                dImgMag = Constants.IMG_SCALE_MIN;
            }
            else if (dImgMag > Constants.IMG_SCALE_MAX)
            {
                dImgMag = Constants.IMG_SCALE_MAX;
            }
            int iNewSrcWidth = (int)(m_wndStandingPosture.GetBackgroundWidth() / dImgMag);
            int iNewSrcHeight = (int)(m_wndStandingPosture.GetBackgroundHeight() / dImgMag);

            int iNewSrcX = iSrcCenterX - iNewSrcWidth / 2;
            int iNewSrcY = iSrcCenterY - iNewSrcHeight / 2;
            if (iNewSrcX < 0)
            {
                iNewSrcX = 0;
            }
            if (iNewSrcY < 0)
            {
                iNewSrcY = 0;
            }
            if (iNewSrcX + iNewSrcWidth >= m_wndStandingPosture.GetBackgroundWidth())
            {
                iNewSrcX = m_wndStandingPosture.GetBackgroundWidth() - 1 - iNewSrcWidth;
            }
            if (iNewSrcY + iNewSrcHeight >= m_wndStandingPosture.GetBackgroundHeight())
            {
                iNewSrcY = m_wndStandingPosture.GetBackgroundHeight() - 1 - iNewSrcHeight;
            }
            m_wndStandingPosture.SetSrcPos(iNewSrcX, iNewSrcY);
            m_wndStandingPosture.SetSrcSize(iNewSrcWidth, iNewSrcHeight);
            m_wndKneedownPosture.SetSrcPos(iNewSrcX, iNewSrcY);
            m_wndKneedownPosture.SetSrcSize(iNewSrcWidth, iNewSrcHeight);

            /* m_wndStandingPosture.UpdateOffscreen();
             m_wndStandingPosture.GetClientRect(&rcClient);
             m_wndStandingPosture.InvalidateRect(&rcClient);
             m_wndStandingPosture.UpdateWindow();

             m_wndKneedownPosture.UpdateOffscreen();
             m_wndKneedownPosture.GetClientRect(&rcClient);
             m_wndKneedownPosture.InvalidateRect(&rcClient);
             m_wndKneedownPosture.UpdateWindow();*/
            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();
        }


        private void IDC_SLIDER1_Scroll(object sender, EventArgs e)
        {
            int scale = IDC_SLIDER1.Value;
            UpdatePictureScale(scale);

        }

        private void IDC_Mag1Btn_Click(object sender, EventArgs e)
        {
            int scale = IDC_SLIDER1.Value;
            if (scale >= 400)
                return;
            if ((scale + 50) > 400)
                IDC_SLIDER1.Value = 400;
            else
                IDC_SLIDER1.Value = scale + 50;
            UpdatePictureScale(scale + 50);

        }

        private void IDC_Mag2Btn_Click(object sender, EventArgs e)
        {

            int scale = IDC_SLIDER1.Value;
            //TRACE2("scale now %d updated %d",scale,scale-50);
            if (scale <= 100)
                return;
            if ((scale - 50) < 100)
                IDC_SLIDER1.Value = 100;
            else
                IDC_SLIDER1.Value = (scale - 50);
            UpdatePictureScale(scale - 50);

        }

        private void IDC_ResetImgBtn_Click(object sender, EventArgs e)
        {
            int iBackgroundWidth = m_wndStandingPosture.GetBackgroundWidth();
            int iBackgroundHeight = m_wndStandingPosture.GetBackgroundHeight();
            int iSrcWidth = (int)(iBackgroundWidth / m_JointEditDoc.GetImgMag());
            int iSrcHeight = (int)(iBackgroundHeight / m_JointEditDoc.GetImgMag());
            int iSrcX = iBackgroundWidth / 2 - iSrcWidth / 2;
            int iSrcY = iBackgroundHeight / 2 - iSrcHeight / 2;
            m_wndStandingPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndStandingPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            m_wndKneedownPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndKneedownPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            // m_wndStandingPosture.UpdateOffscreen();
            // m_wndKneedownPosture.UpdateOffscreen();

            double dImgMag = (double)iBackgroundWidth / iSrcWidth;

            UpdatePictureScale((int)(Constants.IMG_SCALE_MAX * 100 + Constants.IMG_SCALE_MIN * 100 - dImgMag * 100));

            IDC_SLIDER1.SetRange((int)(Constants.IMG_SCALE_MIN * 100), (int)(Constants.IMG_SCALE_MAX * 100));
            IDC_SLIDER1.Value = (int)(Constants.IMG_SCALE_MAX * 100 + Constants.IMG_SCALE_MIN * 100 - dImgMag * 100);


        }

        private void pictureBox_Standing_MouseMove(object sender, MouseEventArgs e)
        {

            m_wndStandingPosture.OnMouseMove(e);
            m_wndKneedownPosture.OnMouseMove(e);
            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();



        }

        private void pictureBox_Standing_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                m_wndStandingPosture.OnRButtonUp(e);
                m_wndKneedownPosture.OnRButtonUp(e);
            }
            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();
        }

        private void pictureBox_Standing_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                m_wndStandingPosture.OnRButtonDown(e);
                m_wndKneedownPosture.OnRButtonDown(e);
            }
            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();

        }

        private void pictureBox_KneeDown_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                m_wndStandingPosture.OnRButtonUp(e);
                m_wndKneedownPosture.OnRButtonUp(e);
            }
            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();

        }

        private void pictureBox_KneeDown_MouseMove(object sender, MouseEventArgs e)
        {

            m_wndStandingPosture.OnMouseMove(e);
            m_wndKneedownPosture.OnMouseMove(e);
            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();


        }

        private void pictureBox_KneeDown_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                m_wndStandingPosture.OnRButtonDown(e);
                m_wndKneedownPosture.OnRButtonDown(e);
            }
            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();

        }

        private void IDC_BTN_RETURNTOPMENU_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
                Yugamiru.Properties.Resources.WARNING1/*"if return to title view, current data will be lost. return to title view OK?"*/,
                "JointEdit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.No)
            {
                return;
            }
            m_JointEditDoc.SetInputMode(Constants.INPUTMODE_NEW);
            m_JointEditDoc.SetFinalScreenMode(Constants.FINAL_SCREEN_MODE_NONE);
            //m_JointEditDoc.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);
            FunctiontoInitialScreen(EventArgs.Empty);
            GlobalVar.OriginalImages = new Dictionary<string, OpenFileDialog>();
            this.Close();
            DisposeControls();

        }
        public event EventHandler EventToInitialScreen;
        public void FunctiontoInitialScreen(EventArgs e)
        {
            EventHandler eventHandler = EventToInitialScreen;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }

        }


        private void IDC_EditBtn_Click(object sender, EventArgs e) // check position button
        {
            m_JointEditDoc.SetSaveFilePath("");
            m_JointEditDoc.SetInputMode(Constants.INPUTMODE_MODIFY);
            m_JointEditDoc.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP);
            //m_JointEditDoc.ChangeToJointEditView();
            FunctiontoCheckPosition(EventArgs.Empty);
            this.Close();
            DisposeControls();
        }
        public event EventHandler EventToCheckPosition; // creating event handler - step1// SUMIT NEED TO NOTICE
        public void FunctiontoCheckPosition(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToCheckPosition;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
        public event EventHandler EventToEditID; // creating event handler - step1
        public void FunctiontoEditID(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToEditID;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDC_BTN_NAMECHANGE_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetSaveFilePath("");
            m_JointEditDoc.SetInputMode(Constants.INPUTMODE_MODIFY);
            m_JointEditDoc.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_RETAIN);
            FunctiontoEditID(EventArgs.Empty);
           

            this.Close();
            DisposeControls();
         

        }

        private void IDC_RemeasurementBtn_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetSaveFilePath("");
            m_JointEditDoc.SetInputMode(Constants.INPUTMODE_MODIFY);
            m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING);
            FunctiontoChange(EventArgs.Empty);

            //GetDocument.ChangeToMeasurementStartView();
            this.Close();
            DisposeControls();
        }
        public event EventHandler EventToChange; // creating event handler - step1 // SUMIT NEED TO NOTICE
        public void FunctiontoChange(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToChange;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDC_MeasurementEndBtn_Click(object sender, EventArgs e)// Restart Button
        {
            DialogResult result = MessageBox.Show(
                Yugamiru.Properties.Resources.WARNING3/*"if start new measurement, current data will be lost. start new measurement OK?"*/,
                "JointEdit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.No)
            {
                return;
            }

            m_JointEditDoc.SetInputMode(Constants.INPUTMODE_NEW);
            m_JointEditDoc.SetFinalScreenMode(Constants.FINAL_SCREEN_MODE_NONE);
            m_JointEditDoc.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);
            FunctiontoRestart(EventArgs.Empty);
        }
        public event EventHandler EventToRestart; // creating event handler - step1 // SUMIT NEED TO NOTICE
        public void FunctiontoRestart(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToRestart;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDC_ScoresheetBtn_Click(object sender, EventArgs e)
        {
           // MessageBox.Show("hello");
            NextPageNum = 1;
            PrintDocument printDocument_temp = new PrintDocument();
            using (var printPreviewDialog = new PrintPreviewDialog())
            {
                //this.PrintPreviewDialog1 = new PrintPreviewDialog();

                //Set the size, location, and name.
                printPreviewDialog.ClientSize = new Size(1024, 1280);
                //printPreviewDialog.Size = new System.Drawing.Size(100, 100);
                printPreviewDialog.Location = new Point(0, 0);
                printPreviewDialog.Name = "Yugamiru";
                //printPreviewDialog.PrintPreviewControl.Zoom = 1;

                string strComment;
                strComment = IDC_CommentField.Text;
                //’èŒ`•¶‚Ì‚Ü‚Ü‚È‚ç‚Î•Û‘¶‚µ‚È‚¢
                /* string strFixedComment;
                 m_JointEditDoc.GetFixedComment(strFixedComment);
                 if (strComment.Compare(strFixedComment) == 0)
                 {
                     strComment = "";
                 }*/
                m_JointEditDoc.SetDataComment(strComment);

                // Associate the event-handling method with the 
                // document's PrintPage event.
                printPreviewDialog.UseAntiAlias = true;
                printDocument_temp.DocumentName = "Yugamiru";
               
                /* Margins margins = new Margins(0, 0, 1100, 778);
                 printDocument_temp.DefaultPageSettings.Margins = margins;*/
                /* PaperSize ps = new PaperSize();
                 ps.RawKind = (int)PaperKind.A4;
                 printDocument_temp.DefaultPageSettings.PaperSize = ps;*///new PaperSize("A4", 827, 1169);
                printDocument_temp.DefaultPageSettings.PaperSize = new PaperSize("", 827, 1170);
                printDocument_temp.DefaultPageSettings.Landscape = true;
               
                printPreviewDialog.Document = printDocument_temp;
              
                //printDocument_temp.DefaultPageSettings.PaperSize = new System.Drawing.Printing.PaperSize("", 10, 10);
                //printDocument_temp.DefaultPageSettings.Landscape = true;

                printDocument_temp.PrintPage += document_PrintPage; //new PrintPageEventHandler(document_PrintPage);


                //document.PrintPage += PrintImage;
                // Set the minimum size the dialog can be resized to.
                //this.PrintPreviewDialog1.MinimumSize = new Size(375, 250);

                // Set the UseAntiAlias property to true, which will allow the 
                // operating system to smooth fonts.
                
                printPreviewDialog.ShowDialog(this);

                

            }
           
            
            // Create a new PrintPreviewDialog using constructor.


        }

        private void document_PrintPage(object sender, PrintPageEventArgs e)
        {
            /*   e.PageSettings.PaperSize.Width = 1200;
               e.PageSettings.PaperSize.Height = 850;
               e.PageSettings.Landscape = true;*/

            // Insert code to render the page here.
            // This code will be called when the PrintPreviewDialog.Show 
            // method is called.

            // The following code will render a simple
            // message on the document in the dialog.
            //  string text = "In document_PrintPage method.";

            // e.Graphics.DrawString(text, printFont,
            //    System.Drawing.Brushes.Black, 0, 0);
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            e.Graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            switch (NextPageNum)
            {

                case 1:
                    Rectangle rect = new Rectangle(e.PageBounds.X, e.PageBounds.Y, e.PageBounds.Width, e.PageBounds.Height);//10, 20, 1000, 800);
                   

                    if (m_JointEditDoc.GetDisplayScore() == 1)
                    {
                        Bitmap testimage1 = null;
                        testimage1 =    new Bitmap(m_CL_Path +@"\postureReport_bg.bmp");
                        if(testimage1 != null)
                        {
                           
                                e.Graphics.DrawImage(testimage1, rect);//e.MarginBounds);
              
                            testimage1.Dispose();
                            testimage1 = null;

                        }
                    }
                    else
                    {
                        Bitmap testimage2 = null;
                        testimage2 =  new Bitmap(Constants.path +@"\postureReport_bg2.bmp");
                        if (testimage2 != null)
                        {

                                e.Graphics.DrawImage(testimage2, rect);//e.MarginBounds);
                          
                            testimage2.Dispose();
                            testimage2 = null;
                        }
                    }
                    // Drawing the comment
                    DrawComment(e);

                    //Drawing Measurement Time
                    DrawMeasurementTime(e);

                    //Draw the User ID
                    DrawUserID(e);


                    //Draw the User Name
                    DrawUserName(e);


                    //Draw the GenderName
                    DrawGenderName(e);


                    //Draw the UserHeight
                    DrawUserHeight(e);

                    DrawPosturePatternPicture(e, 520 + 28, 590 - 10 );

                    if (m_JointEditDoc.GetDisplayScore() == 1)
                    {
                        // ‚ä‚ª‚Ý[‚éŽw”‚Ì•\Ž¦.
                        DrawYugamiPointPicture(70 + 14, 90 + 14 , 110 + 16, 250 - 10 , e);
                    }
                    if (m_JointEditDoc.GetDisplayScore() == 1)
                    {
                        // ‚ä‚ª‚Ý[‚éƒ‰ƒ“ƒN‚Ì•\Ž¦.
                        DrawYugamiruPointRankPicture(250 + 30 +10, 250 - 10, e);
                    }
                    else
                    {
                        // ƒXƒRƒA‚Ì•ª‚¾‚¯¶‚É‚¸‚ç‚·.
                        DrawYugamiruPointRankPicture(250 + 30 +10, 250 - 10, e);
                    }

                    DrawYugamiruPointRankCommentPicture(e, 50 + 30, 280 - 22+10 +6);

                    DrawRaderChart(e, 20 + 4 , 450 - 20);

                    DrawStandingImage(e, 400 - 6 , 110 -4, 230 + 10, 380 +10);
                   
                    DrawKneedownImage(e, 600 +44, 110 - 4, 230 + 10, 380 + 10);

                    //DrawSideImage(e, 5000, 700 - 50, 1300 + 30 + 40 + 20, 1800 + 500 + 20);
                    DrawSideImage(e, 840 + 54, 110 - 4, 230 + 10, 380 + 10);

                    break;

                case 2:
                    Rectangle rect1 = new Rectangle(e.PageBounds.X, e.PageBounds.Y,
                        e.PageBounds.Width, e.PageBounds.Height);//10, 20, 1000, 800);

                    Bitmap testimage = null;
                    testimage =  new Bitmap(m_CL_Path + @"\muscleReport_bg.bmp");

                    if (testimage != null)
                    {
                        e.Graphics.DrawImage(testimage, rect1);                    
                        testimage.Dispose();
                        testimage = null;
                    }

                    // Drawing the comment
                    DrawComment(e);
                 
                    //Drawing Measurement Time
                    DrawMeasurementTime(e);
                    string filePath = Application.StartupPath;

                    //Draw the User ID
                    DrawUserID(e);
                   

                    //Draw the User Name
                    DrawUserName(e);

                    //Draw the GenderName
                    DrawGenderName(e);


                    //Draw the UserHeight
                    DrawUserHeight(e);

                    DrawMuscleCommentPicture(e, 100 , 100 + 136 , 260 + 6, 450);

                    if (m_JointEditDoc.GetJudgeMode() == 1)
                    {
                        int[] aiNewTrainingID = new int[4];
                        m_JointEditDoc.GetNewTrainingIDs(aiNewTrainingID);
                        DrawNewTrainingPicture(e, 300, 400 - 20, aiNewTrainingID[0]);
                        DrawNewTrainingPicture(e, 600, 400 - 20, aiNewTrainingID[1]);
                        DrawNewTrainingPicture(e, 300, 500 - 20, aiNewTrainingID[2]);
                        DrawNewTrainingPicture(e, 600, 500 - 20, aiNewTrainingID[3]);
                    }
                    else
                    {
                        DrawTrainingPicture(e, 310 + 80, 400 + 170 - 10, 800 - 100, 400 - 190);
                    }

                    // —§ˆÊ‹Ø“÷‰æ‘œ‚Ì•\Ž¦.

                    DrawStandingMusclePicture(e, 400 + 24, 10);
                    // ‹üˆÊ‹Ø“÷‰æ‘œ‚Ì•\Ž¦.
                    DrawKneedownMusclePicture(e, 800 - 8, 10);
                    break;
                default:
                    break;
            }

            // Next time print the next page.
            NextPageNum += 1;

            // We have more pages if wee have not yet printed page 3.
            if (NextPageNum <= 2 && m_JointEditDoc.GetDisplayMuscleReport() == 1)
            {
                e.HasMorePages = true;//(NextPageNum <= 2);
                return;
            }
            else
                e.HasMorePages = false;


            // If we have no more pages, reset for the next time we print.
            /*    if (NextPageNum > 2)
                { NextPageNum = 1; return; }*/
        }
        void DrawKneedownMusclePicture(PrintPageEventArgs e, int x, int y)
        {
            Image ImageFilePath = Yugamiru.Properties.Resources.k_Body;

            //e.Graphics.DrawImage(ImageFilePath, new Point(x, y));

            BackgroundBodyBitmapFileImage BackgroundBodyBitmapFileImage = new BackgroundBodyBitmapFileImage();
            BackgroundBodyBitmapFileImage.Load(ImageFilePath);

            /*  using (Graphics g = Graphics.FromImage(ImageFilePath))
              {
                  g.DrawImageUnscaled(Yugamiru.Properties.Resources.r_02l, new Point(0, 0));
              }*/


            MuscleBitmapFileImage MuscleBitmapFileImageBlue = new MuscleBitmapFileImage();
            if (!MuscleBitmapFileImageBlue.Create(BackgroundBodyBitmapFileImage.GetWidth(), BackgroundBodyBitmapFileImage.GetHeight()))
            {
                return;
            }
            MuscleBitmapFileImage MuscleBitmapFileImageRed = new MuscleBitmapFileImage();
            if (!MuscleBitmapFileImageRed.Create(BackgroundBodyBitmapFileImage.GetWidth(), BackgroundBodyBitmapFileImage.GetHeight()))
            {
                return;
            }
            MuscleBitmapFileImage MuscleBitmapFileImageYellow = new MuscleBitmapFileImage();
            if (!MuscleBitmapFileImageYellow.Create(BackgroundBodyBitmapFileImage.GetWidth(), BackgroundBodyBitmapFileImage.GetHeight()))
            {
                return;
            }
            MuscleColorInfo MuscleColorInfo = new MuscleColorInfo();
            m_JointEditDoc.CalcKneedownMuscleColor(MuscleColorInfo);

            int i = 0;
            for (i = 0; i < Constants.MUSCLEID_MAX; i++)
            {
                if (MuscleColorInfo.GetMuscleColor(i) != Constants.MUSCLECOLORID_NONE)
                {
                    Image MuscleImageFilePath = GetKneedownMuscleBMPFileNameByID(i);
                    MuscleBitmapFileImage MuscleBitmapFileImageNew = new MuscleBitmapFileImage();
                    MuscleBitmapFileImageNew.Load(MuscleImageFilePath);

                    switch (MuscleColorInfo.GetMuscleColor(i))
                    {
                        case Constants.MUSCLECOLORID_YELLOW:
                            MuscleBitmapFileImageYellow.Merge(MuscleBitmapFileImageNew);
                            break;
                        case Constants.MUSCLECOLORID_RED:
                            MuscleBitmapFileImageRed.Merge(MuscleBitmapFileImageNew);
                            break;
                        case Constants.MUSCLECOLORID_BLUE:
                            MuscleBitmapFileImageBlue.Merge(MuscleBitmapFileImageNew);
                            break;
                        default:
                            break;
                    }

                }
            }

            BackgroundBodyBitmapFileImage.ApplyMuscleBitmapFileImage(MuscleBitmapFileImageYellow, 255, 255, 0, 255, 255, 0);
            BackgroundBodyBitmapFileImage.ApplyMuscleBitmapFileImage(MuscleBitmapFileImageRed, 192, 0, 13, 235, 191, 215);
            BackgroundBodyBitmapFileImage.ApplyMuscleBitmapFileImage(MuscleBitmapFileImageBlue, 0, 0, 255, 178, 178, 255);
            BackgroundBodyBitmapFileImage.Draw(e.Graphics, x, y);
        }
        void DrawStandingMusclePicture(PrintPageEventArgs e, int x, int y)
        {

            Image ImageFilePath = Yugamiru.Properties.Resources.r_Body;

            //e.Graphics.DrawImage(ImageFilePath, new Point(x, y));

            BackgroundBodyBitmapFileImage BackgroundBodyBitmapFileImage = new BackgroundBodyBitmapFileImage();
            BackgroundBodyBitmapFileImage.Load(ImageFilePath);

            /*  using (Graphics g = Graphics.FromImage(ImageFilePath))
              {
                  g.DrawImageUnscaled(Yugamiru.Properties.Resources.r_02l, new Point(0, 0));
              }*/


            MuscleBitmapFileImage MuscleBitmapFileImageBlue = new MuscleBitmapFileImage();
            if (!MuscleBitmapFileImageBlue.Create(BackgroundBodyBitmapFileImage.GetWidth(), BackgroundBodyBitmapFileImage.GetHeight()))
            {
                return;
            }
            MuscleBitmapFileImage MuscleBitmapFileImageRed = new MuscleBitmapFileImage();
            if (!MuscleBitmapFileImageRed.Create(BackgroundBodyBitmapFileImage.GetWidth(), BackgroundBodyBitmapFileImage.GetHeight()))
            {
                return;
            }
            MuscleBitmapFileImage MuscleBitmapFileImageYellow = new MuscleBitmapFileImage();
            if (!MuscleBitmapFileImageYellow.Create(BackgroundBodyBitmapFileImage.GetWidth(), BackgroundBodyBitmapFileImage.GetHeight()))
            {
                return;
            }
            MuscleColorInfo MuscleColorInfo = new MuscleColorInfo();
            m_JointEditDoc.CalcStandingMuscleColor(MuscleColorInfo);

            int i = 0;
            //MessageBox.Show("step 1");
            for (i = 0; i < Constants.MUSCLEID_MAX; i++)
            {
                if (MuscleColorInfo.GetMuscleColor(i) != Constants.MUSCLECOLORID_NONE)
                {
                    Image MuscleImageFilePath = GetStandingMuscleBMPFileNameByID(i);
                    MuscleBitmapFileImage MuscleBitmapFileImageNew = new MuscleBitmapFileImage();
                    MuscleBitmapFileImageNew.Load(MuscleImageFilePath);
                    /*using (Graphics g = Graphics.FromImage(ImageFilePath))
                    {
                        g.DrawImageUnscaled(MuscleImageFilePath, new Point(0, 0));
                    }*/
                    
                    switch (MuscleColorInfo.GetMuscleColor(i))
                    {
                        case Constants.MUSCLECOLORID_YELLOW:
                            MuscleBitmapFileImageYellow.Merge(MuscleBitmapFileImageNew);
                            break;
                        case Constants.MUSCLECOLORID_RED:
                            MuscleBitmapFileImageRed.Merge(MuscleBitmapFileImageNew);
                            break;
                        case Constants.MUSCLECOLORID_BLUE:
                            MuscleBitmapFileImageBlue.Merge(MuscleBitmapFileImageNew);
                            break;
                        default:
                            break;
                    }

                }
            }
            //MessageBox.Show("step 2");
            BackgroundBodyBitmapFileImage.ApplyMuscleBitmapFileImage(MuscleBitmapFileImageYellow, 255, 255, 0, 255, 255, 0);
            BackgroundBodyBitmapFileImage.ApplyMuscleBitmapFileImage(MuscleBitmapFileImageRed, 192, 0, 13, 235, 191, 215);
            BackgroundBodyBitmapFileImage.ApplyMuscleBitmapFileImage(MuscleBitmapFileImageBlue, 0, 0, 255, 178, 178, 255);
            BackgroundBodyBitmapFileImage.Draw(e.Graphics, x, y);
        }

        void DrawTrainingPicture(PrintPageEventArgs e, int x, int y, int width, int height)
        {
            int iPosturePatternID = m_JointEditDoc.CalcPosturePatternID();
            Bitmap ImageFileName = null;
            switch (iPosturePatternID)
            {
                case Constants.POSTUREPATTERNID_STOOP_AND_BEND_BACKWARD:  /* ”L”w{”½‚è˜ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\training_nekoSorigosi.bmp");
                    break;
                case Constants.POSTUREPATTERNID_STOOP:                    /* ”L”w */
                    ImageFileName = new Bitmap(m_CL_Path + @"\training_nekoze.bmp");
                    break;
                case Constants.POSTUREPATTERNID_BEND_BACKWARD:            /* ”½‚è˜ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\training_sorigosi_flatback.bmp");
                    break;
                case Constants.POSTUREPATTERNID_FLATBACK:                 /* ƒtƒ‰ƒbƒgƒoƒbƒN */
                    ImageFileName = new Bitmap(m_CL_Path + @"\training_sorigosi_flatback.bmp");
                    break;
                case Constants.POSTUREPATTERNID_NORMAL:                   /* —‘z‚ÌŽp¨ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\training_standard.bmp");
                    break;
                case Constants.POSTUREPATTERNID_FRONTSIDE_UNBALANCED: /* ³–Êƒoƒ‰ƒ“ƒX”ñ‘ÎÌ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\training_standard.bmp"); // ‰¼
                    break;
                default:
                    break;
            }
            if (ImageFileName != null)
            {
                ImageFileName.MakeTransparent(Color.White);
                e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                e.Graphics.SmoothingMode = SmoothingMode.HighQuality;               
                e.Graphics.DrawImage(ImageFileName, new Rectangle(x + 18, y - 4, width + 38, height + 40), 0, 0, ImageFileName.Width, ImageFileName.Height, GraphicsUnit.Pixel);              
                ImageFileName.Dispose();
                ImageFileName = null;
            }
           
        }
        void DrawNewTrainingPicture(PrintPageEventArgs e, int x, int y, int iTrainingID)
        {
            Bitmap ImageFileName = null;
            switch (iTrainingID)
            {
                case 1:
                    ImageFileName = new Bitmap(m_CL_Path + @"\u_1.bmp");
                    break;
                case 2:
                    ImageFileName = new Bitmap(m_CL_Path + @"\u_2.bmp");
                    break;
                case 3:
                    ImageFileName = new Bitmap(m_CL_Path + @"\t_3.bmp");
                    break;
                case 4:
                    ImageFileName = new Bitmap(m_CL_Path + @"\t_4.bmp");
                    break;
                case 5:
                    ImageFileName = new Bitmap(m_CL_Path + @"\t_5.bmp");
                    break;
                case 6:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_6.bmp");
                    break;
                case 7:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_7.bmp");
                    break;
                case 8:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_8.bmp");
                    break;
                case 9:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_9.bmp");
                    break;
                case 10:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_10.bmp");
                    break;
                case 11:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_11.bmp");
                    break;
                case 12:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_12.bmp");
                    break;
                case 13:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_13.bmp");
                    break;
                case 14:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_14.bmp");
                    break;
                case 15:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_15.bmp");
                    break;
                case 16:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_16.bmp");
                    break;
                case 17:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_17.bmp");
                    break;
                case 18:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_18.bmp");
                    break;
                case 19:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_19.bmp");
                    break;
                case 20:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_20.bmp");
                    break;
                case 21:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_21.bmp");
                    break;
                case 22:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l_22.bmp");
                    break;
                default:
                    break;
            }
          
                if (ImageFileName != null)
                {
                    ImageFileName.MakeTransparent(Color.White);
                    e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
                    using (ImageFileName)
                    {
                        e.Graphics.DrawImage(ImageFileName,  x, y);
                    }
                    ImageFileName.Dispose();
                ImageFileName = null;
                }

            

        }
        void DrawMuscleCommentPicture(PrintPageEventArgs e, int x, int y, int width, int height)
        {
            int iPosturePatternID = m_JointEditDoc.CalcPosturePatternID();
            Bitmap ImageFileName = null;
            switch (iPosturePatternID)
            {
                case Constants.POSTUREPATTERNID_STOOP_AND_BEND_BACKWARD:  /* ”L”w{”½‚è˜ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\muscleInfo_nekoSorigosi.bmp");
                    break;
                case Constants.POSTUREPATTERNID_STOOP:                    /* ”L”w */
                    ImageFileName = new Bitmap(m_CL_Path + @"\muscleInfo_nekoze.bmp");
                    break;
                case Constants.POSTUREPATTERNID_BEND_BACKWARD:            /* ”½‚è˜ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\muscleInfo_sorigosi.bmp");
                    break;
                case Constants.POSTUREPATTERNID_FLATBACK:                 /* ƒtƒ‰ƒbƒgƒoƒbƒN */
                    ImageFileName = new Bitmap(m_CL_Path + @"\muscleInfo_flatback.bmp");
                    break;
                case Constants.POSTUREPATTERNID_NORMAL:                   /* —‘z‚ÌŽp¨ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\muscleInfo_standard.bmp");
                    break;
                case Constants.POSTUREPATTERNID_FRONTSIDE_UNBALANCED: /* ³–Êƒoƒ‰ƒ“ƒX”ñ‘ÎÌ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\muscleInfo_standard.bmp"); // ‰¼
                    break;
                default:
                    break;
            }
            ImageFileName.MakeTransparent(Color.White);
            if (ImageFileName != null)
            {
                using (ImageFileName)
                {
                    e.Graphics.DrawImage(ImageFileName, new Rectangle(x, y, width, height), 0, 0, ImageFileName.Width, ImageFileName.Height - 20, GraphicsUnit.Pixel);
                }
                ImageFileName.Dispose();
                ImageFileName = null;
            }

        }
        void DrawUserHeight(PrintPageEventArgs e)
        {
            Rectangle RectforUserHeight = new Rectangle(250 + 60, 148, 200, 20);
            e.Graphics.DrawString(IDC_Height.Text, printFont, Brushes.Black, RectforUserHeight);
        }
        void DrawGenderName(PrintPageEventArgs e)
        {

            Rectangle RectforUserGender = new Rectangle(150 + 86, 148, 200, 20);
            e.Graphics.DrawString(IDC_Gender.Text, printFont, Brushes.Black, RectforUserGender);

        }
        void DrawMeasurementTime(PrintPageEventArgs e)
        {
            string strYear = string.Empty, strMonth = string.Empty, strDay = string.Empty, strTime = string.Empty;
            string MeasurementTime = m_JointEditDoc.GetDataMeasurementTime();
            if (MeasurementTime != null)
            {
                m_JointEditDoc.GetDataAcqDate(ref strYear, ref strMonth, ref strDay, ref strTime);
            }
            else
            {
                //	strYear		= time.Format(" % y");
                strYear = DateTime.Now.ToString("yy");
                //		strMonth	= time.Format("%m");
                strMonth = DateTime.Now.Month.ToString();
                strDay = DateTime.Now.Day.ToString();
                strTime = DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString();
            }
            string strTmp;
            if(m_JointEditDoc.GetLanguage() == "English" || m_JointEditDoc.GetLanguage() == "Thai")
                strTmp = strMonth + ".  " + strDay + " " + strYear;
            else
            {
                strTmp = strYear + Yugamiru.Properties.Resources.YY + strMonth + Yugamiru.Properties.Resources.MM + strYear + Yugamiru.Properties.Resources.DD;
            }

            Rectangle RectforMeasurementTime = new Rectangle(10 + 80, 100+20 , 200, 20);
            e.Graphics.DrawString(strTmp, printFont, Brushes.Black, RectforMeasurementTime);

        }
        void DrawUserName(PrintPageEventArgs e)
        {
            Rectangle RectforUserName = new Rectangle(20 + 70, 138 + 10, 200, 20);
            e.Graphics.DrawString(m_JointEditDoc.GetDataName(), printFont, Brushes.Black, RectforUserName);

        }
        void DrawUserID(PrintPageEventArgs e)
        {
            Rectangle RectforUserID = new Rectangle(200 + 40, 100 + 20, 200, 20);
            e.Graphics.DrawString(m_JointEditDoc.GetDataID(), printFont, Brushes.Black, RectforUserID);

        }
        void DrawComment(PrintPageEventArgs e)
        {

            Rectangle Rect4Comment = new Rectangle(10, 800, 200 - 20, 20);
            e.Graphics.DrawString(m_JointEditDoc.GetDataComment(), printFont, Brushes.Black, Rect4Comment);

        }


        void DrawSideImage(PrintPageEventArgs e, int x, int y, int w, int h)
        {
            Image<Bgr, byte> test = new Image<Bgr, byte>(1024, 1280);
            test.Bytes = m_JointEditDoc.m_SideImageBytes;

            Bitmap test1 = test.ToBitmap();

            SideBodyPosition SideBodyPosition = new SideBodyPosition();
            m_JointEditDoc.GetSideBodyPosition(ref SideBodyPosition);

            using (Graphics g = Graphics.FromImage(test1))
            {
                GlyphOverlayerToSideImage GlyphOverlayer = new GlyphOverlayerToSideImage();

                Point ptAnkle = new Point();
                SideBodyPosition.GetAnklePosition(ref ptAnkle);

                GlyphOverlayer.SetCenterLineData(
                   m_JointEditDoc.IsValidMidLine(),
                   m_JointEditDoc.GetMidLineStyle(),
                    Color.FromArgb(255, 192, 203),//m_JointEditDoc.GetMidLineColor(),
                    m_JointEditDoc.GetMidLineWidth() ,
                    ptAnkle.X,
                    0,
                    /*m_JointEditDoc.GetImageHeight()*/1280);

                GlyphOverlayer.SetJointConnectionLineData(
                    m_JointEditDoc.IsValidStyleLine(),
                    m_JointEditDoc.GetStyleLineStyle(),
                    m_JointEditDoc.GetStyleLineColor(),
                    m_JointEditDoc.GetStyleLineWidth() + 8);

                GlyphOverlayer.SetMarkerSize(m_JointEditDoc.GetMarkerSize() + 8);
                GlyphOverlayer.SetSideBodyPosition(SideBodyPosition);
                GlyphOverlayer.Draw(g);
            }
            Rectangle DesRect = new Rectangle(x, y, w, h);
            //e.Graphics.DrawImage(test1, DesRect, 200, 0, 1024 - 360, 1280,GraphicsUnit.Pixel);   //830, 110, 220, 400);
            e.Graphics.DrawImage(test1, DesRect, 130, 0, 1024 - 260, 1280, GraphicsUnit.Pixel);
            SideBodyAngle tempSideBodyAngle = new SideBodyAngle();
            m_JointEditDoc.CalcBodyAngleSide(ref tempSideBodyAngle);
            SideBodyLabelString tempSideBodyLabelString = new SideBodyLabelString(ref SideBodyPosition,
                ref tempSideBodyAngle, m_JointEditDoc.GetBenchmarkDistance());
            if (m_JointEditDoc.IsLabelInvisible() <= 0)
                LabelandFont(e.Graphics, tempSideBodyLabelString, x, y);
        }
        public void LabelandFont(Graphics e, SideBodyLabelString m_SidetBodyLabelString, int x, int y)
        {
            Font f = new Font("MS Gothic", 12, FontStyle.Regular, GraphicsUnit.Pixel);
            
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.EAR_POS_REPORT, f, new Point(14 + x, 6 + y));
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetEarPositionString(), f, new Point(14 + x, 32 + y));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.SHOULDER_POS_REPORT, f, new Point(14 + x, 70 + y));
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetShoulderPositionString(), f, new Point(14 + x, 96 + y));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.HIP_POS_REPORT, f, new Point(14 + x, 141 + y));
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetHipPositionString(), f, new Point(14 + x, 167 + y));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.KNEE_POS_REPORT, f, new Point(14 + x, 212 + y));
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetKneePositionString(), f, new Point(14 + x, 238 + y));
            //SampleDrawOutlineText(e, "", f, new Point(14 + x, 283 + y));
            //SampleDrawOutlineText(e, m_SidetBodyLabelString.GetHipPositionString(), f, new Point(14 +x, 309 + y));

            //if(m_JointEditDoc.GetLanguage() == "English")
            SizeF stringSize = new SizeF();
            stringSize = e.MeasureString(Yugamiru.Properties.Resources.SHOULDER_EAR_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.SHOULDER_EAR_ANGLE_REPORT, f, new Point(240+x- (int)stringSize.Width, 6 + y));
            //else
            //  SampleDrawOutlineText(e, Yugamiru.Properties.Resources.SHOULDER_EAR_ANGLE_REPORT, f, new Point(266 + x - 100, 6 + y));

            stringSize = e.MeasureString(m_SidetBodyLabelString.GetShoulderEarAngleString(), f);
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetShoulderEarAngleString(), f, new Point(240+x - (int)stringSize.Width, 32 + y));

            //if (m_JointEditDoc.GetLanguage() == "English")
            stringSize = e.MeasureString(Yugamiru.Properties.Resources.HIP_SHOULDER_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.HIP_SHOULDER_ANGLE_REPORT, f, new Point(240 + x - (int)stringSize.Width, 70 + y));
            //else
            //SampleDrawOutlineText(e, Yugamiru.Properties.Resources.HIP_SHOULDER_ANGLE_REPORT, f, new Point(266 + x - 100, 70 + y));
            stringSize = e.MeasureString(m_SidetBodyLabelString.GetHipShoulderAngleString(), f);
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetHipShoulderAngleString(), f, new Point(240 + x - (int)stringSize.Width, 96 + y));

            stringSize = e.MeasureString(Yugamiru.Properties.Resources.HIP_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.HIP_ANGLE_REPORT, f, new Point(240 + x - (int)stringSize.Width, 141 + y));

            stringSize = e.MeasureString(m_SidetBodyLabelString.GetHipBalanceString(), f);
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetHipBalanceString(), f, new Point(240 + x - (int)stringSize.Width, 167 + y));
            //SampleDrawOutlineText(e, "Heel position", f, new Point(178 + x -50, 212 + y));
            //SampleDrawOutlineText(e, m_SidetBodyLabelString.GetAnklePositionString(), f, new Point(248 + x - 50, 238 + y));
            //SampleDrawOutlineText(e, "Heel position", f, new Point(185, 283));
            //SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strLeftKneeAngle, f, new Point(241, 309));

            m_SecondPageFlag = true;
        }
        void SampleDrawOutlineText(Graphics g, String text, Font font, Point p)
        {
            // set atialiasing
            g.SmoothingMode = SmoothingMode.HighQuality;
            // make thick pen for outlining
            Pen pen = new Pen(Color.White, 3);
            // round line joins of the pen
            pen.LineJoin = LineJoin.Round;
            // create graphics path
            GraphicsPath textPath = new GraphicsPath();
            // convert string to path
            textPath.AddString(text, font.FontFamily, (int)font.Style, font.Size, p, StringFormat.GenericTypographic);
            // clone path to make outlining path
            GraphicsPath outlinePath = (GraphicsPath)textPath.Clone();
            // outline the path
            outlinePath.Widen(pen);
            // fill outline path with some color
            g.FillPath(Brushes.White, outlinePath);
            // fill original text path with some color
            g.FillPath(Brushes.Black, textPath);
        }
        public stretchDIBbits.BITMAPINFO bmi = new stretchDIBbits.BITMAPINFO();
        void SetBitmap()
        {
            
            bmi.bmiHeader.biSize = 40;//new stretchDIBbits.BITMAPINFOHEADER().biSize;
            bmi.bmiHeader.biWidth = 1024;//test.Width;
            bmi.bmiHeader.biHeight = -1280;//-test.Height;
            bmi.bmiHeader.biPlanes = 1;
            bmi.bmiHeader.biBitCount = 24;
            //bmi.bmiHeader.biCompression = stretchDIBbits.BitmapCompressionMode.BI_RGB;
            //bmi.bmiHeader.biCompression = stretchDIBbits.BitmapCompressionMode.BI_JPEG;
            bmi.bmiHeader.biSizeImage = 0;
            bmi.bmiHeader.biXPelsPerMeter = 0;
            bmi.bmiHeader.biYPelsPerMeter = 0;
            bmi.bmiHeader.biClrUsed = 0;
            bmi.bmiHeader.biClrImportant = 0;

            bmi.bmiColors = new RGBQUAD[] { new RGBQUAD { } };
            bmi.bmiColors[0].rgbBlue = 255;
            bmi.bmiColors[0].rgbGreen = 255;
            bmi.bmiColors[0].rgbRed = 255;
            bmi.bmiColors[0].rgbReserved = 255;
        }
        void DrawUsingStretchdibits(Graphics e,int x, int y,int w,int h,Byte[] ImageBytes)
        {
            SetBitmap();
            //Image<Bgr, byte> emgucvImage = new Image<Bgr, byte>(bmp);
            stretchDIBbits.SetStretchBltMode(e.GetHdc(), StretchBltMode.STRETCH_HALFTONE);
            e.ReleaseHdc();
            var t = stretchDIBbits.StretchDIBits(
                            e.GetHdc(),
                            x,//m_ImageClipWnd.m_iDestRectUpperLeftCornerX,
                            y,//m_ImageClipWnd.m_iDestRectUpperLeftCornerY,
                           w,//emgucvimage.Width,//test.Width,//m_ImageClipWnd.m_iDestRectWidth,
                           h,//emgucvimage.Height,//test.Height,//m_ImageClipWnd.m_iDestRectHeight,
                            115,
                            0,//(m_ImageClipWnd.m_iBackgroundHeight - 1 - (0 + m_ImageClipWnd.m_iBackgroundHeight - 1)),
                             794, //1024 - 200,//test.Width,//m_ImageClipWnd.m_iBackgroundWidth,
                              1280,//test.Height,//m_ImageClipWnd.m_iBackgroundHeight,
                                   //test.Bytes,
                            ImageBytes,//pByteBits,
                            ref bmi,
                            Constants.DIB_RGB_COLORS,
                            Constants.SRCCOPY);

            e.ReleaseHdc();


        }

        void DrawStandingImage(PrintPageEventArgs e, int x, int y, int w, int h)
        {
            Bitmap bmp = new Bitmap(pictureBox_Standing.ClientSize.Width, pictureBox_Standing.ClientSize.Height);
            pictureBox_Standing.DrawToBitmap(bmp, pictureBox_Standing.ClientRectangle);

         
            Image<Bgr, byte> test = new Image<Bgr, byte>(1024, 1280);
            test.Bytes = m_JointEditDoc.m_FrontStandingImageBytes;

            Bitmap test1 = test.ToBitmap();
            FrontBodyPosition FrontBodyPositionStanding = new FrontBodyPosition();
            m_JointEditDoc.GetStandingFrontBodyPosition(ref FrontBodyPositionStanding);
            Point ptRightAnkle = new Point();
            using (Graphics g = Graphics.FromImage(test1))
            {
                GlyphOverlayerToFrontImage GlyphOverlayer = new GlyphOverlayerToFrontImage();

                
                Point ptLeftAnkle = new Point();
                FrontBodyPositionStanding.GetRightAnklePosition(ref ptRightAnkle);
                FrontBodyPositionStanding.GetLeftAnklePosition(ref ptLeftAnkle);

                GlyphOverlayer.SetArrowLength(30);
                GlyphOverlayer.SetArrowWidth(10);
                GlyphOverlayer.SetArrowInvisible(m_JointEditDoc.IsArrowInvisible());

                GlyphOverlayer.SetCenterLineData(
                    m_JointEditDoc.IsValidMidLine(),
                    (int)m_JointEditDoc.GetMidLineStyle(),
                    Color.FromArgb(255, 192, 203),//m_JointEditDoc.GetMidLineColor(),
                    (int)m_JointEditDoc.GetMidLineWidth(),
                    (ptRightAnkle.X + ptLeftAnkle.X) / 2,
                    0,
                    1280);//m_JointEditDoc.GetImageHeight());

                GlyphOverlayer.SetJointConnectionLineData(
                    m_JointEditDoc.IsValidStyleLine(),
                    (int)m_JointEditDoc.GetStyleLineStyle(),
                    m_JointEditDoc.GetStyleLineColor(),
                    10);//(int)m_JointEditDoc.GetStyleLineWidth());

                GlyphOverlayer.ptRightShoulder.X = FrontBodyPositionStanding.m_ptRightShoulder.X ;
                GlyphOverlayer.ptRightShoulder.Y = FrontBodyPositionStanding.m_ptRightShoulder.Y;
                GlyphOverlayer.ptRightAnkle.X = FrontBodyPositionStanding.m_ptRightAnkle.X ;
                GlyphOverlayer.ptRightAnkle.Y = FrontBodyPositionStanding.m_ptRightAnkle.Y ;
                GlyphOverlayer.ptRightHip.X = FrontBodyPositionStanding.m_ptRightHip.X ;
                GlyphOverlayer.ptRightHip.Y = FrontBodyPositionStanding.m_ptRightHip.Y ;
                GlyphOverlayer.ptRightKnee.X = FrontBodyPositionStanding.m_ptRightKnee.X ;
                GlyphOverlayer.ptRightKnee.Y = FrontBodyPositionStanding.m_ptRightKnee.Y ;

                GlyphOverlayer.ptLeftShoulder.X = FrontBodyPositionStanding.m_ptLeftShoulder.X ;
                GlyphOverlayer.ptLeftShoulder.Y = FrontBodyPositionStanding.m_ptLeftShoulder.Y;
                GlyphOverlayer.ptLeftAnkle.X = FrontBodyPositionStanding.m_ptLeftAnkle.X ;
                GlyphOverlayer.ptLeftAnkle.Y = FrontBodyPositionStanding.m_ptLeftAnkle.Y;
                GlyphOverlayer.ptLeftHip.X = FrontBodyPositionStanding.m_ptLeftHip.X ;
                GlyphOverlayer.ptLeftHip.Y = FrontBodyPositionStanding.m_ptLeftHip.Y ;
                GlyphOverlayer.ptLeftKnee.X = FrontBodyPositionStanding.m_ptLeftKnee.X ;
                GlyphOverlayer.ptLeftKnee.Y = FrontBodyPositionStanding.m_ptLeftKnee.Y ;


                GlyphOverlayer.ptGlabella.X = FrontBodyPositionStanding.m_ptGlabella.X ;
                GlyphOverlayer.ptGlabella.Y = FrontBodyPositionStanding.m_ptGlabella.Y ;
                GlyphOverlayer.ptRightEar.X = FrontBodyPositionStanding.m_ptRightEar.X ;
                GlyphOverlayer.ptRightEar.Y = FrontBodyPositionStanding.m_ptRightEar.Y ;
                GlyphOverlayer.ptLeftEar.X = FrontBodyPositionStanding.m_ptLeftEar.X ;
                GlyphOverlayer.ptLeftEar.Y = FrontBodyPositionStanding.m_ptLeftEar.Y ;
                GlyphOverlayer.ptChin.X = FrontBodyPositionStanding.m_ptChin.X ;
                GlyphOverlayer.ptChin.Y = FrontBodyPositionStanding.m_ptChin.Y ;


                GlyphOverlayer.SetMarkerSize(m_JointEditDoc.GetMarkerSize());

                GlyphOverlayer.SetFrontBodyPosition(FrontBodyPositionStanding);
                FrontBodyResultData FrontBodyResultDataStanding = new FrontBodyResultData();
                m_JointEditDoc.CalcStandingFrontBodyResultData(ref FrontBodyResultDataStanding);
                GlyphOverlayer.SetFrontBodyResultData(FrontBodyResultDataStanding);
                GlyphOverlayer.Draw(g);
            }

            Rectangle DesRect = new Rectangle(x, y, w, h);
            using (test1)
            {
                e.Graphics.DrawImage(test1, DesRect, 130, 0, 1024 - 260, 1280, GraphicsUnit.Pixel);
            }

            SideBodyPosition SideBodyPosition = new SideBodyPosition();
            m_JointEditDoc.GetSideBodyPosition(ref SideBodyPosition);
            Point ptBenchmark1 = new Point(0, 0);
            Point ptBenchmark2 = new Point(0, 0);
            SideBodyPosition.GetBenchmark1Position(ref ptBenchmark1);
            SideBodyPosition.GetBenchmark2Position(ref ptBenchmark2);
            FrontBodyAngle FrontBodyAngleStanding = new FrontBodyAngle();
            m_JointEditDoc.CalcBodyAngleKneedown(ref FrontBodyAngleStanding);

            FrontBodyLabelString temp_FrontBodyLabelString = new FrontBodyLabelString(FrontBodyPositionStanding, FrontBodyAngleStanding,
            ptBenchmark1, ptBenchmark2, m_JointEditDoc.GetBenchmarkDistance());

            Font f = new Font("calibri ", 12, FontStyle.Regular, GraphicsUnit.Pixel);
            if (m_JointEditDoc.IsLabelInvisible() <= 0)
                DrawOutlineText(e.Graphics, temp_FrontBodyLabelString, f, x, y);


        }
        void DrawOutlineText(Graphics e, FrontBodyLabelString temp_FrontBodyLabelString, Font f,int x,int y)
        {
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.BROW_POS_REPORT, f, new Point(14 + x, 6 + y));
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strGlabellaPosition, f, new Point(14 + x, 32 + y));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.EAR_POS_REPORT, f, new Point(14 + x, 70 + y));
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strEarCenterPosition, f, new Point(14 + x, 96 + y));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.JAW_POS_REPORT, f, new Point(14 + x, 141 + y));
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strChinPosition, f, new Point(14 + x, 167 + y));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.SHOULDER_POS_REPORT, f, new Point(14 + x, 212 + y));
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strShoulderCenterPosition, f, new Point(14 + x, 238 + y));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.HIP_POS_REPORT, f, new Point(14 + x, 283 + y));
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strHipCenterPosition, f, new Point(14 + x, 309 + y));

            SizeF stringSize = new SizeF();
            stringSize = e.MeasureString(Yugamiru.Properties.Resources.EAR_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.EAR_ANGLE_REPORT, f, new Point(240 + x - (int)stringSize.Width, 6 + y));

            stringSize = e.MeasureString(temp_FrontBodyLabelString.m_strEarBalance, f);
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strEarBalance, f, new Point(240 + x - (int)stringSize.Width, 32 + y));


            //if(m_JointEditDoc.GetLanguage() == "English")
            stringSize = e.MeasureString(Yugamiru.Properties.Resources.SHOULDER_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.SHOULDER_ANGLE_REPORT, f, new Point(240 + x - (int)stringSize.Width, 70 + y));
            //else
            //  SampleDrawOutlineText(e, Yugamiru.Properties.Resources.SHOULDER_ANGLE_REPORT, f, new Point(220 + x - 50, 70 + y));
            stringSize = e.MeasureString(temp_FrontBodyLabelString.m_strShoulderBalance, f);
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strShoulderBalance, f, new Point(240 + x - (int)stringSize.Width, 96 + y));

            stringSize = e.MeasureString(Yugamiru.Properties.Resources.HIP_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.HIP_ANGLE_REPORT, f, new Point(240 + x - (int)stringSize.Width, 141 + y));

            stringSize = e.MeasureString(temp_FrontBodyLabelString.m_strHipBalance, f);
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strHipBalance, f, new Point(240 + x - (int)stringSize.Width, 167 + y));

            //if (m_JointEditDoc.GetLanguage() == "English")
            stringSize = e.MeasureString(Yugamiru.Properties.Resources.RIGHTKNEE_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.RIGHTKNEE_ANGLE_REPORT, f, new Point(240 + x - (int)stringSize.Width, 212 + y));
            //else
            //SampleDrawOutlineText(e, Yugamiru.Properties.Resources.RIGHTKNEE_ANGLE_REPORT, f, new Point(224 + x - 50, 212 + y));
            stringSize = e.MeasureString(temp_FrontBodyLabelString.m_strRightKneeAngle, f);
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strRightKneeAngle, f, new Point(240 + x - (int)stringSize.Width, 238 + y));

            //if (m_JointEditDoc.GetLanguage() == "English")
            stringSize = e.MeasureString(Yugamiru.Properties.Resources.LEFTKNEE_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.LEFTKNEE_ANGLE_REPORT, f, new Point(240 + x - (int)stringSize.Width, 283 + y));
            //else
            //SampleDrawOutlineText(e, Yugamiru.Properties.Resources.LEFTKNEE_ANGLE_REPORT, f, new Point(224 + x - 50, 283 + y));
            stringSize = e.MeasureString(temp_FrontBodyLabelString.m_strLeftKneeAngle, f);
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strLeftKneeAngle, f, new Point(240 + x - (int)stringSize.Width, 309 + y));


        }
        void DrawKneedownImage(PrintPageEventArgs e, int x, int y, int w, int h)
        {
            /*Bitmap bmp = new Bitmap(pictureBox_KneeDown.ClientSize.Width, pictureBox_KneeDown.ClientSize.Height);
            pictureBox_KneeDown.DrawToBitmap(bmp, pictureBox_KneeDown.ClientRectangle);

            DrawUsingStretchdibits(e.Graphics, x, y, w, h, m_JointEditDoc.m_FrontKneedownImageBytes);
            */
            //e.Graphics.DrawImage(bmp, x, y, w, h);
            // Creates a graphic object so we can draw the screen in the bitmap (bmpScreenshot);

            Image<Bgr, byte> test = new Image<Bgr, byte>(1024, 1280);
            test.Bytes = m_JointEditDoc.m_FrontKneedownImageBytes;

            Bitmap test1 = test.ToBitmap();
            FrontBodyPosition FrontBodyPositionKneedown = new FrontBodyPosition();
            m_JointEditDoc.GetKneedownFrontBodyPosition(ref FrontBodyPositionKneedown);
            using (Graphics g = Graphics.FromImage(test1))
            {
                GlyphOverlayerToFrontImage GlyphOverlayer = new GlyphOverlayerToFrontImage();

                
                Point ptRightAnkle = new Point();
                Point ptLeftAnkle = new Point();
                FrontBodyPositionKneedown.GetRightAnklePosition(ref ptRightAnkle);
                FrontBodyPositionKneedown.GetLeftAnklePosition(ref ptLeftAnkle);

                GlyphOverlayer.SetArrowLength(30);
                GlyphOverlayer.SetArrowWidth(10);
                GlyphOverlayer.SetArrowInvisible(m_JointEditDoc.IsArrowInvisible());

                GlyphOverlayer.SetCenterLineData(
                    m_JointEditDoc.IsValidMidLine(),
                    (int)m_JointEditDoc.GetMidLineStyle(),
                    Color.FromArgb(255, 192, 203),//m_JointEditDoc.GetMidLineColor(),
                    (int)m_JointEditDoc.GetMidLineWidth(),
                    (ptRightAnkle.X + ptLeftAnkle.X) / 2,
                    0,
                    1280);//m_JointEditDoc.GetImageHeight());

                GlyphOverlayer.SetJointConnectionLineData(
                    m_JointEditDoc.IsValidStyleLine(),
                    (int)m_JointEditDoc.GetStyleLineStyle(),
                    m_JointEditDoc.GetStyleLineColor(),
                    10);//(int)m_JointEditDoc.GetStyleLineWidth());

                GlyphOverlayer.ptRightShoulder.X = FrontBodyPositionKneedown.m_ptRightShoulder.X;
                GlyphOverlayer.ptRightShoulder.Y = FrontBodyPositionKneedown.m_ptRightShoulder.Y;
                GlyphOverlayer.ptRightAnkle.X = FrontBodyPositionKneedown.m_ptRightAnkle.X;
                GlyphOverlayer.ptRightAnkle.Y = FrontBodyPositionKneedown.m_ptRightAnkle.Y;
                GlyphOverlayer.ptRightHip.X = FrontBodyPositionKneedown.m_ptRightHip.X;
                GlyphOverlayer.ptRightHip.Y = FrontBodyPositionKneedown.m_ptRightHip.Y;
                GlyphOverlayer.ptRightKnee.X = FrontBodyPositionKneedown.m_ptRightKnee.X;
                GlyphOverlayer.ptRightKnee.Y = FrontBodyPositionKneedown.m_ptRightKnee.Y;

                GlyphOverlayer.ptLeftShoulder.X = FrontBodyPositionKneedown.m_ptLeftShoulder.X;
                GlyphOverlayer.ptLeftShoulder.Y = FrontBodyPositionKneedown.m_ptLeftShoulder.Y;
                GlyphOverlayer.ptLeftAnkle.X = FrontBodyPositionKneedown.m_ptLeftAnkle.X;
                GlyphOverlayer.ptLeftAnkle.Y = FrontBodyPositionKneedown.m_ptLeftAnkle.Y;
                GlyphOverlayer.ptLeftHip.X = FrontBodyPositionKneedown.m_ptLeftHip.X;
                GlyphOverlayer.ptLeftHip.Y = FrontBodyPositionKneedown.m_ptLeftHip.Y;
                GlyphOverlayer.ptLeftKnee.X = FrontBodyPositionKneedown.m_ptLeftKnee.X;
                GlyphOverlayer.ptLeftKnee.Y = FrontBodyPositionKneedown.m_ptLeftKnee.Y;


                GlyphOverlayer.ptGlabella.X = FrontBodyPositionKneedown.m_ptGlabella.X;
                GlyphOverlayer.ptGlabella.Y = FrontBodyPositionKneedown.m_ptGlabella.Y;
                GlyphOverlayer.ptRightEar.X = FrontBodyPositionKneedown.m_ptRightEar.X;
                GlyphOverlayer.ptRightEar.Y = FrontBodyPositionKneedown.m_ptRightEar.Y;
                GlyphOverlayer.ptLeftEar.X = FrontBodyPositionKneedown.m_ptLeftEar.X;
                GlyphOverlayer.ptLeftEar.Y = FrontBodyPositionKneedown.m_ptLeftEar.Y;
                GlyphOverlayer.ptChin.X = FrontBodyPositionKneedown.m_ptChin.X;
                GlyphOverlayer.ptChin.Y = FrontBodyPositionKneedown.m_ptChin.Y;


                GlyphOverlayer.SetMarkerSize(m_JointEditDoc.GetMarkerSize());

                GlyphOverlayer.SetFrontBodyPosition(FrontBodyPositionKneedown);
                FrontBodyResultData FrontBodyResultDataKneedown = new FrontBodyResultData();
                m_JointEditDoc.CalcStandingFrontBodyResultData(ref FrontBodyResultDataKneedown);
                GlyphOverlayer.SetFrontBodyResultData(FrontBodyResultDataKneedown);
                GlyphOverlayer.Draw(g);

            }

            Rectangle DesRect = new Rectangle(x, y, w, h);
            using (test1)
            {
                e.Graphics.DrawImage(test1, DesRect, 130, 0, 1024 - 260, 1280, GraphicsUnit.Pixel);
            }


            SideBodyPosition SideBodyPosition = new SideBodyPosition();
            m_JointEditDoc.GetSideBodyPosition(ref SideBodyPosition);
            Point ptBenchmark1 = new Point(0, 0);
            Point ptBenchmark2 = new Point(0, 0);
            SideBodyPosition.GetBenchmark1Position(ref ptBenchmark1);
            SideBodyPosition.GetBenchmark2Position(ref ptBenchmark2);
            FrontBodyAngle FrontBodyAngleKneedown = new FrontBodyAngle();
            m_JointEditDoc.CalcBodyAngleKneedown(ref FrontBodyAngleKneedown);

            FrontBodyLabelString temp_FrontBodyLabelString = new FrontBodyLabelString(FrontBodyPositionKneedown, FrontBodyAngleKneedown,
            ptBenchmark1, ptBenchmark2, m_JointEditDoc.GetBenchmarkDistance());

            Font f = new Font("calibri ", 12, FontStyle.Regular, GraphicsUnit.Pixel);
            if (m_JointEditDoc.IsLabelInvisible() <= 0)
                DrawOutlineText(e.Graphics, temp_FrontBodyLabelString, f, x, y);
        }

        public void DrawPosturePatternPicture(PrintPageEventArgs e, int x, int y)
        {
            int iPosturePatternID = m_JointEditDoc.CalcPosturePatternID();
            Bitmap strImageFileName = null;
            switch (iPosturePatternID)
            {
                case Constants.POSTUREPATTERNID_STOOP_AND_BEND_BACKWARD:  /* ”L”w{”½‚è˜ */
                    strImageFileName = new Bitmap(m_CL_Path + @"\state_nekoSorigosi.bmp");
                    break;
                case Constants.POSTUREPATTERNID_STOOP:                    /* ”L”w */
                    strImageFileName = new Bitmap(m_CL_Path + @"\state_nekoze.bmp");
                    break;
                case Constants.POSTUREPATTERNID_BEND_BACKWARD:            /* ”½‚è˜ */
                    strImageFileName = new Bitmap(m_CL_Path + @"\state_sorigosi.bmp");
                    break;
                case Constants.POSTUREPATTERNID_FLATBACK:                 /* ƒtƒ‰ƒbƒgƒoƒbƒN */
                    strImageFileName = new Bitmap(m_CL_Path + @"\state_flatBack.bmp");
                    break;
                case Constants.POSTUREPATTERNID_NORMAL:                   /* —‘z‚ÌŽp¨ */
                    strImageFileName = new Bitmap(m_CL_Path + @"\state_standard.bmp");
                    break;
                case Constants.POSTUREPATTERNID_FRONTSIDE_UNBALANCED: /* ³–Êƒoƒ‰ƒ“ƒX”ñ‘ÎÌ */
                    //strImageFileName = Properties.Resources.propstate_standard;    // ‰¼.
                    break;
                default:
                    break;
            }
            if (strImageFileName != null)
            {
                strImageFileName.MakeTransparent(Color.White);
                e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                using (strImageFileName)
                {
                    e.Graphics.DrawImage(strImageFileName, new Rectangle(x + 4, y - 6, 550 + 36, 230), 0, 0, strImageFileName.Width, strImageFileName.Height, GraphicsUnit.Pixel);
                }
                strImageFileName.Dispose();
                strImageFileName = null;
            }
        }

        public void DrawYugamiPointPicture(int x1, int x2, int x3, int y, PrintPageEventArgs e)
        {
            bool bAlreadyDisplayed = false;
            Image ImageFileName = null;
            int iPoint = m_JointEditDoc.CalcYugamiPoint();//FrontBodyAngleStanding, FrontBodyAngleKneedown, m_SideBodyAngle);

            // 100‚ÌˆÊ‚Ì•\Ž¦.
            if (iPoint >= 100)
            {
                e.Graphics.DrawImage(Properties.Resources.num1, x1, y, 20, 20);
                iPoint = iPoint % 100;
                bAlreadyDisplayed = true;
            }
            // 10‚ÌˆÊ‚Ì•\Ž¦.
            if ((bAlreadyDisplayed) || (iPoint >= 10))
            {
                int iImageID = (iPoint / 10) % 10;

                switch (iImageID)
                {
                    case 2:
                        ImageFileName = Yugamiru.Properties.Resources.num2;
                        break;
                    case 3:
                        ImageFileName = Yugamiru.Properties.Resources.num3;
                        break;
                    case 4:
                        ImageFileName = Yugamiru.Properties.Resources.num4;
                        break;
                    case 5:
                        ImageFileName = Yugamiru.Properties.Resources.num5;
                        break;
                    case 6:
                        ImageFileName = Yugamiru.Properties.Resources.num6;
                        break;
                    case 7:
                        ImageFileName = Yugamiru.Properties.Resources.num7;
                        break;
                    case 8:
                        ImageFileName = Yugamiru.Properties.Resources.num8;
                        break;
                    case 9:
                        ImageFileName = Yugamiru.Properties.Resources.num9;
                        break;
                    default:
                        break;
                }

                using (ImageFileName)
                {
                    e.Graphics.DrawImage(ImageFileName, x2, y, 24, 24);
                }
                iPoint = iPoint % 10;
                bAlreadyDisplayed = true;
            }
            // 1‚ÌˆÊ‚Ì•\Ž¦.
            {
                int iImageID = iPoint % 10;
                switch (iImageID)
                {
                    case 2:
                        ImageFileName = Yugamiru.Properties.Resources.num2;
                        break;
                    case 3:
                        ImageFileName = Yugamiru.Properties.Resources.num3;
                        break;
                    case 4:
                        ImageFileName = Yugamiru.Properties.Resources.num4;
                        break;
                    case 5:
                        ImageFileName = Yugamiru.Properties.Resources.num5;
                        break;
                    case 6:
                        ImageFileName = Yugamiru.Properties.Resources.num6;
                        break;
                    case 7:
                        ImageFileName = Yugamiru.Properties.Resources.num7;
                        break;
                    case 8:
                        ImageFileName = Yugamiru.Properties.Resources.num8;
                        break;
                    case 9:
                        ImageFileName = Yugamiru.Properties.Resources.num9;
                        break;
                    default:
                        break;
                }
                using (ImageFileName)
                {
                    e.Graphics.DrawImage(ImageFileName, x3, y, 24, 24);
                }

                bAlreadyDisplayed = true;
            }
        }

        void DrawYugamiruPointRankPicture(int x, int y, PrintPageEventArgs e)
        {
            int YugamiruPointRank = m_JointEditDoc.CalcYugamiPointRank();//FrontBodyAngleStanding, FrontBodyAngleKneedown, m_SideBodyAngle);
            Image ImageFileName = null;
            switch (YugamiruPointRank)
            {
                case Constants.YUGAMIRU_POINT_RANK_A:
                    ImageFileName = Properties.Resources.rankA;
                    break;
                case Constants.YUGAMIRU_POINT_RANK_B:
                    ImageFileName = Properties.Resources.rankB;
                    break;
                case Constants.YUGAMIRU_POINT_RANK_C:
                    ImageFileName = Properties.Resources.rankC;
                    break;
                case Constants.YUGAMIRU_POINT_RANK_D:
                    ImageFileName = Properties.Resources.rankD;
                    break;
                default:
                    break;
            }
            if (ImageFileName != null)
            {
                if (m_JointEditDoc.GetDisplayScore() == 1)
                {
                    using (ImageFileName)
                    {
                        e.Graphics.DrawImage(ImageFileName, x, y, 24, 24);
                    }
                }
                else
                {
                    using (ImageFileName)
                    {
                        e.Graphics.DrawImage(ImageFileName, x - 70, y, 24, 24);
                    }
                }
                ImageFileName.Dispose();
                ImageFileName = null;
            }
        }

        void DrawYugamiruPointRankCommentPicture(PrintPageEventArgs e, int x, int y)
        {
            int YugamiruPointRank = m_JointEditDoc.CalcYugamiPointRank();//FrontBodyAngleStanding, FrontBodyAngleKneedown, m_SideBodyAngle);
            Bitmap ImageFileName = null;
            switch (YugamiruPointRank)
            {
                case Constants.YUGAMIRU_POINT_RANK_A:
                    ImageFileName = Yugamiru.Properties.Resources.rankA_memo;
                    break;
                case Constants.YUGAMIRU_POINT_RANK_B:
                    ImageFileName = Yugamiru.Properties.Resources.rankB_memo;
                    break;
                case Constants.YUGAMIRU_POINT_RANK_C:
                    ImageFileName = Yugamiru.Properties.Resources.rankC_memo;
                    break;
                case Constants.YUGAMIRU_POINT_RANK_D:
                    ImageFileName = Yugamiru.Properties.Resources.rankD_memo;
                    break;
                default:
                    break;
            }
            if (ImageFileName != null)
            {
                ImageFileName.MakeTransparent(Color.White);
                using (ImageFileName)
                {
                    e.Graphics.DrawImage(ImageFileName, x, y, 280, 41);
                }
                ImageFileName.Dispose();
                ImageFileName = null;
            }
        }

        void DrawRaderChart(PrintPageEventArgs e, int x, int y)
        {

            Image ImageFilePath = Yugamiru.Properties.Resources.chart;

            //CBackgroundBodyBitmapFileImage BackgroundBodyBitmapFileImage;
            if (ImageFilePath == null)
            {
                return;
            }
            // ‚Ü‚¸A”wŒi•`‰æ.
            //BackgroundBodyBitmapFileImage.Draw(&dc, 0, 0);            
            //e.Graphics.DrawImage(ImageFilePath, 0,400 + 50,350,250);            

            // ‚±‚±‚ÅƒOƒ‰ƒt‚ð•`‰æ.

            //•]‰¿‚Ì’iŠK
            int[] GRADE_VALUE = new int[] { 100, 80, 50, 30, 10, 5 };
            int GRADE_COUNT = GRADE_VALUE.Length / GRADE_VALUE.GetLength(0);
            const double FIT = 2.8;

            //•]‰¿‚·‚é€–Ú
            const int EST_COUNT = 11;
            //•]‰¿‚·‚é€–Ú•ª‚ÌXŽ²•ûŒü‚ÌˆÚ“®‹——£
            double[] WidthAlfa = new double[EST_COUNT];
            double[] HeightAlfa = new double[EST_COUNT];
            const double PI = 3.1415926535;
            const double offset_rad = 2 * PI / EST_COUNT;
            double rad = PI / 2;
            for (int i = 0; i < EST_COUNT; i++)
            {
                WidthAlfa[i] = Math.Cos(rad);
                HeightAlfa[i] = Math.Sin(rad);
                rad -= offset_rad;
            }


            //ƒŒ[ƒ_[ƒ`ƒƒ[ƒg‚Ì’†S
            Point centerP = new Point(ImageFilePath.Width / 2, ImageFilePath.Height / 2);
            using (Graphics g = Graphics.FromImage(ImageFilePath))
            {
                MyDrawPoint(g, centerP.X, centerP.Y, 26, Color.FromArgb(255, 0, 0), true);
                //g.DrawLine(Pens.Red, 0, 0, 600, 600);


                int[] rst = new int[EST_COUNT];
                Point[] pt = new Point[EST_COUNT];

                ResultData result = new ResultData();
                m_JointEditDoc.CalcBodyBalanceResultData(ref result);

                rst[0] = result.GetStandingHip() + result.GetKneedownHip();
                rst[1] = result.GetStandingHip() + result.GetKneedownHip();
                rst[2] = result.GetStandingShoulderBal() + result.GetKneedownShoulderBal();
                rst[3] = result.GetStandingShoulderBal() + result.GetKneedownShoulderBal();
                rst[4] = result.GetStandingRightKnee();
                rst[5] = result.GetKneedownRightKnee();
                rst[6] = result.GetKneedownLeftKnee();
                rst[7] = result.GetStandingLeftKnee();
                rst[8] = result.GetStandingCenterBalance() + result.GetKneedownCenterBalance();
                rst[9] = result.GetStandingEarBal() + result.GetKneedownEarBal();
                rst[10] = result.GetStandingHeadCenter() + result.GetKneedownHeadCenter();

                //Še“_‚ðŒvŽZ
                for (int i = 0; i < EST_COUNT; i++)
                {
                    //ASSERT(abs(rst[i]) < GRADE_COUNT);

                    double userVal = GRADE_VALUE[Math.Abs(rst[i])] * FIT;
                    pt[i] = new Point((int)(centerP.X + userVal * WidthAlfa[i]), (int)(centerP.Y - (userVal * HeightAlfa[i])));
                }
                //ü‚ðˆø‚¢‚Ä
                for (int i = 0; i < EST_COUNT; i++)
                {
                    // Ô‚Å•\Ž¦.
                    MyDrawLine(g, pt[i], pt[(i + 1) % EST_COUNT], 0, Color.FromArgb(255, 0, 0), 7);
                }
                //‚»‚Ìã‚Éƒ|ƒCƒ“ƒg
                for (int i = 0; i < EST_COUNT; i++)
                {
                    // Ô‚Å•\Ž¦.
                    MyDrawPoint(g, pt[i].X, pt[i].Y, 26, Color.FromArgb(255, 0, 0), true);
                }
            }
            using (ImageFilePath)
            {
                e.Graphics.DrawImage(ImageFilePath, new Rectangle(x, y, 350 - 18, 250 - 14), 0, 0, ImageFilePath.Width, ImageFilePath.Height, GraphicsUnit.Pixel);
            }
            ImageFilePath.Dispose();
            ImageFilePath = null;
            /*
            if (hBitmapOld != NULL)
            {
                dc.SelectObject(hBitmapOld);
                hBitmapOld = NULL;
            }
            CDIB dibTmp;
            DWORD dwBitsSize = (bmi.bmiHeader.biWidth * 3 + 3) / 4 * 4 * bmi.bmiHeader.biHeight;
            dibTmp.LoadFromBitmapInfoHeaderAndImageBits(&(bmi.bmiHeader), (unsigned char *)pvBits, dwBitsSize );
            dibDst.Blt(x, y, dibTmp);

            if (hBitmap != NULL)
            {

        ::DeleteObject(hBitmap);
                hBitmap = NULL;
                pvBits = NULL;
            }*/
        }

        void MyDrawPoint(Graphics pDC, int x, int y, int size, Color rgb /* =RGB(0,0,0) */, bool fill /*=false*/)
        {
            Pen penNew = new Pen(rgb, 0);
            SolidBrush brNew = new SolidBrush(rgb);

            if (fill)
            {
                pDC.FillEllipse(brNew, x - size / 2, y - size / 2, size, size);
            }
            else
            {
                pDC.DrawEllipse(penNew, x - size / 2, y - size / 2, size, size);
            }


        }


        void MyDrawLine(Graphics pDC, Point st, Point end, int style /* =PS_SOLID */, Color rgb /*=RGB(0,0,0)*/, int width /*=1*/)
        {
            //	MoveToEx(hDC, st.x, st.y, NULL);
            //	LineTo(hDC, end.x, end.y);

            uint[] type = new uint[8];
            Pen penNew = new Pen(rgb, width);
            pDC.DrawLine(penNew, st, end);


        }

        private void IDC_BTN_DATASAVE_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("hello");

            saveRecords saveData = new saveRecords(this, m_wndStandingPosture, m_wndKneedownPosture);

            saveData.insertResult(this);
        }

        private void IDC_PrintBtn_Click(object sender, EventArgs e)
        {
            NextPageNum = 1;
            PrintDialog printDlg = new PrintDialog();
            PrintDocument printDoc = new PrintDocument();
            printDoc.DocumentName = "Print Document";
            printDlg.Document = printDoc;
            printDlg.AllowSelection = true;
            printDlg.AllowSomePages = true;
           
            //Call ShowDialog
            if (printDlg.ShowDialog() == DialogResult.OK)
            {
                // Create a new PrintPreviewDialog using constructor.
                this.PrintPreviewDialog1 = new PrintPreviewDialog();

                //Set the size, location, and name.
                this.PrintPreviewDialog1.ClientSize = new Size(1024, 1280);
                this.PrintPreviewDialog1.Location = new Point(0, 0);
                this.PrintPreviewDialog1.Name = "Yugamiru";
                //this.PrintPreviewDialog1.PrintPreviewControl.Zoom = 1.25;

                string strComment;
                strComment = IDC_CommentField.Text;
                //’èŒ`•¶‚Ì‚Ü‚Ü‚È‚ç‚Î•Û‘¶‚µ‚È‚¢
                /* string strFixedComment;
                 m_JointEditDoc.GetFixedComment(strFixedComment);
                 if (strComment.Compare(strFixedComment) == 0)
                 {
                     strComment = "";
                 }*/
                m_JointEditDoc.SetDataComment(strComment);
                //printDoc.DefaultPageSettings.PaperSize = new PaperSize("", 778, 1100);
                printDoc.DefaultPageSettings.PaperSize = new PaperSize("", 827, 1170);

                // Associate the event-handling method with the 
                // document's PrintPage event.
                printDoc.PrintPage += document_PrintPage; //new PrintPageEventHandler(document_PrintPage);

                //document.PrintPage += PrintImage;
                // Set the minimum size the dialog can be resized to.
                //this.PrintPreviewDialog1.MinimumSize = new Size(375, 250);

                // Set the UseAntiAlias property to true, which will allow the 
                // operating system to smooth fonts.
                this.PrintPreviewDialog1.UseAntiAlias = true;
                printDoc.DocumentName = "Yugamiru";
                printDoc.DefaultPageSettings.Landscape = true;
                //PrintPreviewDialog1.Document = document;
                //PrintPreviewDialog1.ShowDialog();
                //document.PrinterSettings.PrintToFile = true;
                
                if(printDoc.PrinterSettings.PrinterName == "Microsoft Print to PDF")
                {
                    //MessageBox.Show("hi");
                    printDoc.PrinterSettings.PrintToFile = true;
                    printDoc.Print();
                }
               else
                    printDoc.Print();
                //document.PrinterSettings.PrintFileName = Path.Combine("e://", "hktespri" + ".pdf");
                //document.Print();
            }


        }

        public Image GetStandingMuscleBMPFileNameByID(int iMuscleID)
        {

            switch (iMuscleID)
            {
                case Constants.MUSCLEID_LATISSIMUSDORSI:
                    return Yugamiru.Properties.Resources.r_01;
                case Constants.MUSCLEID_BICEPSFEMORIS_LEFT:
                    return Yugamiru.Properties.Resources.r_02l;
                case Constants.MUSCLEID_BICEPSFEMORIS_RIGHT:
                    return Yugamiru.Properties.Resources.r_02r;
                case Constants.MUSCLEID_TENSORFASCIAELATAE_LEFT:
                    return Yugamiru.Properties.Resources.r_03l;
                case Constants.MUSCLEID_TENSORFASCIAELATAE_RIGHT:
                    return Yugamiru.Properties.Resources.r_03r;
                case Constants.MUSCLEID_VASTUSLATERALIS_LEFT:
                    return Yugamiru.Properties.Resources.r_04l;
                case Constants.MUSCLEID_VASTUSLATERALIS_RIGHT:
                    return Yugamiru.Properties.Resources.r_04r;
                case Constants.MUSCLEID_QUADRICEPSFEMORIS_LEFT:
                    return Yugamiru.Properties.Resources.r_05l;
                case Constants.MUSCLEID_QUADRICEPSFEMORIS_RIGHT:
                    return Yugamiru.Properties.Resources.r_05r;
                case Constants.MUSCLEID_HIPJOINTCAPSULE_LEFT:
                    return Yugamiru.Properties.Resources.r_06l;
                case Constants.MUSCLEID_HIPJOINTCAPSULE_RIGHT:
                    return Yugamiru.Properties.Resources.r_06r;
                case Constants.MUSCLEID_GLUTEUSMEDIUS_LEFT:
                    return Yugamiru.Properties.Resources.r_07l;
                case Constants.MUSCLEID_GLUTEUSMEDIUS_RIGHT:
                    return Yugamiru.Properties.Resources.r_07r;
                case Constants.MUSCLEID_RECTUSABDOMINIS:
                    return Yugamiru.Properties.Resources.r_08;
                case Constants.MUSCLEID_ILIOTIBIALTRACT_LEFT:
                    return Yugamiru.Properties.Resources.r_09l;
                case Constants.MUSCLEID_ILIOTIBIALTRACT_RIGHT:
                    return Yugamiru.Properties.Resources.r_09r;
                case Constants.MUSCLEID_FIBULARIS_LEFT:
                    return Yugamiru.Properties.Resources.r_10l;
                case Constants.MUSCLEID_FIBULARIS_RIGHT:
                    return Yugamiru.Properties.Resources.r_10r;
                case Constants.MUSCLEID_GASTROCNEMIUS_LEFT:
                    return Yugamiru.Properties.Resources.r_11l;
                case Constants.MUSCLEID_GASTROCNEMIUS_RIGHT:
                    return Yugamiru.Properties.Resources.r_11r;
                case Constants.MUSCLEID_CUBOID_LEFT:
                    return Yugamiru.Properties.Resources.r_12l;
                case Constants.MUSCLEID_CUBOID_RIGHT:
                    return Yugamiru.Properties.Resources.r_12r;
                case Constants.MUSCLEID_ADDUCTOR_LEFT:
                    return Yugamiru.Properties.Resources.r_13l;
                case Constants.MUSCLEID_ADDUCTOR_RIGHT:
                    return Yugamiru.Properties.Resources.r_13r;
                case Constants.MUSCLEID_TRAPEZIUS_LEFT:
                    return Yugamiru.Properties.Resources.r_14l;
                case Constants.MUSCLEID_TRAPEZIUS_RIGHT:
                    return Yugamiru.Properties.Resources.r_14r;
                case Constants.MUSCLEID_STERNOCLEIDOMASTOID:
                    return Yugamiru.Properties.Resources.r_15;
                case Constants.MUSCLEID_MAX:
                    return null;
            }
            return null;

            }

        public Image GetKneedownMuscleBMPFileNameByID(int iMuscleID)
        {
            switch (iMuscleID)
            {

                case Constants.MUSCLEID_LATISSIMUSDORSI:
                    return Yugamiru.Properties.Resources.k_01;
                case Constants.MUSCLEID_BICEPSFEMORIS_LEFT:
                    return Yugamiru.Properties.Resources.k_02l;
                case Constants.MUSCLEID_BICEPSFEMORIS_RIGHT:
                    return Yugamiru.Properties.Resources.k_02r;
                case Constants.MUSCLEID_TENSORFASCIAELATAE_LEFT:
                    return Yugamiru.Properties.Resources.k_03l;
                case Constants.MUSCLEID_TENSORFASCIAELATAE_RIGHT:
                    return Yugamiru.Properties.Resources.k_03r;
                case Constants.MUSCLEID_VASTUSLATERALIS_LEFT:
                    return Yugamiru.Properties.Resources.k_04l;
                case Constants.MUSCLEID_VASTUSLATERALIS_RIGHT:
                    return Yugamiru.Properties.Resources.k_04r;
                case Constants.MUSCLEID_QUADRICEPSFEMORIS_LEFT:
                    return Yugamiru.Properties.Resources.k_05l;
                case Constants.MUSCLEID_QUADRICEPSFEMORIS_RIGHT:
                    return Yugamiru.Properties.Resources.k_05r;
                case Constants.MUSCLEID_HIPJOINTCAPSULE_LEFT:
                    return Yugamiru.Properties.Resources.k_06l;
                case Constants.MUSCLEID_HIPJOINTCAPSULE_RIGHT:
                    return Yugamiru.Properties.Resources.k_06r;
                case Constants.MUSCLEID_GLUTEUSMEDIUS_LEFT:
                    return Yugamiru.Properties.Resources.k_07l;
                case Constants.MUSCLEID_GLUTEUSMEDIUS_RIGHT:
                    return Yugamiru.Properties.Resources.k_07r;
                case Constants.MUSCLEID_RECTUSABDOMINIS:
                    return Yugamiru.Properties.Resources.k_08;
                case Constants.MUSCLEID_ILIOTIBIALTRACT_LEFT:
                    return Yugamiru.Properties.Resources.k_09l;
                case Constants.MUSCLEID_ILIOTIBIALTRACT_RIGHT:
                    return Yugamiru.Properties.Resources.k_09r;
                case Constants.MUSCLEID_FIBULARIS_LEFT:
                    return Yugamiru.Properties.Resources.k_10l;
                case Constants.MUSCLEID_FIBULARIS_RIGHT:
                    return Yugamiru.Properties.Resources.k_10r;
                case Constants.MUSCLEID_GASTROCNEMIUS_LEFT:
                    return Yugamiru.Properties.Resources.k_11l;
                case Constants.MUSCLEID_GASTROCNEMIUS_RIGHT:
                    return Yugamiru.Properties.Resources.k_11r;
                case Constants.MUSCLEID_CUBOID_LEFT:
                    return Yugamiru.Properties.Resources.k_12l;
                case Constants.MUSCLEID_CUBOID_RIGHT:
                    return Yugamiru.Properties.Resources.k_12r;
                case Constants.MUSCLEID_ADDUCTOR_LEFT:
                    return Yugamiru.Properties.Resources.k_13l;
                case Constants.MUSCLEID_ADDUCTOR_RIGHT:
                    return Yugamiru.Properties.Resources.k_13r;
                case Constants.MUSCLEID_TRAPEZIUS_LEFT:
                    return Yugamiru.Properties.Resources.k_14l;
                case Constants.MUSCLEID_TRAPEZIUS_RIGHT:
                    return Yugamiru.Properties.Resources.k_14r;
                case Constants.MUSCLEID_STERNOCLEIDOMASTOID:
                    return Yugamiru.Properties.Resources.k_15;
                case Constants.MUSCLEID_MAX:
                    return null;
            }

          
            return null;
        }
        public void DisposeControls()
        {
            m_MainPic.Image.Dispose();

            IDC_Mag1Btn.Image.Dispose();
            IDC_Mag2Btn.Image.Dispose();
            IDC_ResetImgBtn.Image.Dispose();

            IDC_BTN_NAMECHANGE.Image.Dispose();
            IDC_BTN_RETURNTOPMENU.Image.Dispose();
            IDC_RemeasurementBtn.Image.Dispose();
            IDC_EditBtn.Image.Dispose();

            IDC_PrintBtn.Image.Dispose();
            IDC_ScoresheetBtn.Image.Dispose();
            IDC_BTN_DATASAVE.Image.Dispose();
            IDC_MeasurementEndBtn.Image.Dispose();

            Yugamiru.Properties.Resources.Mainpic6.Dispose();

            Yugamiru.Properties.Resources.mag2.Dispose();
            Yugamiru.Properties.Resources.mag1.Dispose();
            Yugamiru.Properties.Resources.mag3_down.Dispose();

            Yugamiru.Properties.Resources.namechange_down.Dispose();
            Yugamiru.Properties.Resources.returnstart_down.Dispose();
            Yugamiru.Properties.Resources.imagechange_down.Dispose();
            Yugamiru.Properties.Resources.jointedit_down.Dispose();


            Yugamiru.Properties.Resources.reportprint_down.Dispose();
            Yugamiru.Properties.Resources.reportdisplay_down.Dispose();
            Yugamiru.Properties.Resources.datasave_down.Dispose();
            Yugamiru.Properties.Resources.startred_down.Dispose();


            Yugamiru.Properties.Resources.k_01.Dispose();
                 Yugamiru.Properties.Resources.k_02l.Dispose();
                
                     Yugamiru.Properties.Resources.k_02r.Dispose();
                
                     Yugamiru.Properties.Resources.k_03l.Dispose();
                
                     Yugamiru.Properties.Resources.k_03r.Dispose();
                
                     Yugamiru.Properties.Resources.k_04l.Dispose();
               
                     Yugamiru.Properties.Resources.k_04r.Dispose();
               
                     Yugamiru.Properties.Resources.k_05l.Dispose();
               
                     Yugamiru.Properties.Resources.k_05r.Dispose();
            
                     Yugamiru.Properties.Resources.k_06l.Dispose();
                
                     Yugamiru.Properties.Resources.k_06r.Dispose();
               
                     Yugamiru.Properties.Resources.k_07l.Dispose();
               
                     Yugamiru.Properties.Resources.k_07r.Dispose();
               
                    Yugamiru.Properties.Resources.k_08.Dispose();
                
                     Yugamiru.Properties.Resources.k_09l.Dispose();
                
                     Yugamiru.Properties.Resources.k_09r.Dispose();
                
                     Yugamiru.Properties.Resources.k_10l.Dispose();
                
                     Yugamiru.Properties.Resources.k_10r.Dispose();
               
                    Yugamiru.Properties.Resources.k_11l.Dispose();
                
                     Yugamiru.Properties.Resources.k_11r.Dispose();
                
                     Yugamiru.Properties.Resources.k_12l.Dispose();
                
                     Yugamiru.Properties.Resources.k_12r.Dispose();
                
                     Yugamiru.Properties.Resources.k_13l.Dispose();
                
                     Yugamiru.Properties.Resources.k_13r.Dispose();
                
                     Yugamiru.Properties.Resources.k_14l.Dispose();
                
                     Yugamiru.Properties.Resources.k_14r.Dispose();
                
                     Yugamiru.Properties.Resources.k_15.Dispose();

             
                     Yugamiru.Properties.Resources.r_01.Dispose();
                
                     Yugamiru.Properties.Resources.r_02l.Dispose();
                
                     Yugamiru.Properties.Resources.r_02r.Dispose();
                
                     Yugamiru.Properties.Resources.r_03l.Dispose();
                
                     Yugamiru.Properties.Resources.r_03r.Dispose();
                
                     Yugamiru.Properties.Resources.r_04l.Dispose();
                
                     Yugamiru.Properties.Resources.r_04r.Dispose();
                
                     Yugamiru.Properties.Resources.r_05l.Dispose();
                
                     Yugamiru.Properties.Resources.r_05r.Dispose();
                
                     Yugamiru.Properties.Resources.r_06l.Dispose();
                
                     Yugamiru.Properties.Resources.r_06r.Dispose();
                
                     Yugamiru.Properties.Resources.r_07l.Dispose();
               
                     Yugamiru.Properties.Resources.r_07r.Dispose();
               
                     Yugamiru.Properties.Resources.r_08.Dispose();
                
                     Yugamiru.Properties.Resources.r_09l.Dispose();
                
                     Yugamiru.Properties.Resources.r_09r.Dispose();
                
                     Yugamiru.Properties.Resources.r_10l.Dispose();
                
                     Yugamiru.Properties.Resources.r_10r.Dispose();
                
                     Yugamiru.Properties.Resources.r_11l.Dispose();
                
                     Yugamiru.Properties.Resources.r_11r.Dispose();
                
                     Yugamiru.Properties.Resources.r_12l.Dispose();
                
                     Yugamiru.Properties.Resources.r_12r.Dispose();
                
                     Yugamiru.Properties.Resources.r_13l.Dispose();
                
                     Yugamiru.Properties.Resources.r_13r.Dispose();
                
                     Yugamiru.Properties.Resources.r_14l.Dispose();
                
                     Yugamiru.Properties.Resources.r_14r.Dispose();
                
                     Yugamiru.Properties.Resources.r_15.Dispose();

            Yugamiru.Properties.Resources.chart.Dispose();

            Yugamiru.Properties.Resources.rankA_memo.Dispose();
            Yugamiru.Properties.Resources.rankB_memo.Dispose();
             Yugamiru.Properties.Resources.rankC_memo.Dispose();
            Yugamiru.Properties.Resources.rankD_memo.Dispose();
            
              Properties.Resources.rankA.Dispose();
            
                    Properties.Resources.rankB.Dispose();
            Properties.Resources.rankC.Dispose();
             Properties.Resources.rankD.Dispose();

            Yugamiru.Properties.Resources.num2.Dispose();
            Yugamiru.Properties.Resources.num3.Dispose();
            
              Yugamiru.Properties.Resources.num4.Dispose();
            Yugamiru.Properties.Resources.num5.Dispose();
            Yugamiru.Properties.Resources.num6.Dispose();
             Yugamiru.Properties.Resources.num7.Dispose();
             Yugamiru.Properties.Resources.num8.Dispose();
             Yugamiru.Properties.Resources.num9.Dispose();

             


            this.Dispose();
            this.Close();

        }

        private void ResultView_FormClosed(object sender, FormClosedEventArgs e)
        {
           /* if (Application.OpenForms.Count == 0) Application.Exit();
            else
            {
                List<Form> openForms = new List<Form>();

                foreach (Form f in Application.OpenForms)
                    openForms.Add(f);

                foreach (Form f in openForms)
                {
                    if (f.Name != "IDD_BALANCELABO_DIALOG")
                    {
                        f.Controls.Clear();
                        f.Close();
                    }
                  
                }
               
            }*/
               
           
        }
    }
}
