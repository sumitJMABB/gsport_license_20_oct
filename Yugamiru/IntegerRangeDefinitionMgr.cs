﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Yugamiru
{
    public class IntegerRangeDefinitionMgr
    {
        int m_iCount;
        IntegerRangeDefinitionElement[] m_pElement;
        SymbolFunc m_SymbolFunc = new SymbolFunc();

        // ®”’lˆæƒVƒ“ƒ{ƒ‹’è‹`ƒ}ƒXƒ^[ƒf[ƒ^.
        public IntegerRangeDefinitionMgr()
        {
            m_iCount = 0;

            m_pElement = new IntegerRangeDefinitionElement[m_iCount];
    
    }

   
public bool ReadFromFile( string lpszFolderName, string lpszFileName, string pchErrorFilePath /* = NULL */ )
{
    char[] buf = new char[1024];
    List<IntegerRangeDefinitionElement> listElement = new List<IntegerRangeDefinitionElement>();

    if (m_pElement != null)
    {
        
        m_pElement = null;
    }
    m_iCount = 0;

            DualSourceTextReader DualSourceTextReader = new DualSourceTextReader();
            int i = DualSourceTextReader.Open(lpszFolderName, lpszFileName);
            if (i == 0)
            {
                return false;
            }

            string strError = string.Empty;
            int iLineNo = 1;
            while (!(DualSourceTextReader.IsEOF()))
            {
                buf[0] = '\0';
                DualSourceTextReader.Read(ref buf, 1024);
                //var t = File.ReadAllLines(@"Resources\ResultAction.inf");
                IntegerRangeDefinitionElement Element = new IntegerRangeDefinitionElement();

                int iErrorCode = Element.ReadFromString(ref buf);
                if (iErrorCode == Constants.READFROMSTRING_SYNTAX_OK)
                {
                    //listElement.push_back(Element);
                    listElement.Add(Element);
                }
                else
                {
                    m_SymbolFunc.OutputReadErrorString(strError, iLineNo, iErrorCode);
                }
                iLineNo++;
            }
            // d•¡’è‹`ƒ`ƒFƒbƒN.
            //listElement.Sort();
    //list<CIntegerRangeDefinitionElement>::iterator index;
    string strOldSymbol = string.Empty;
    bool bFoundMultipleDefinition = false;
    foreach ( var index in listElement)
    {
        if (index.m_strRangeSymbol == strOldSymbol)
        {
            string strTmp = "MultipleDefinition "+ strOldSymbol+"\n";
            
            strError += strTmp;
        }
        strOldSymbol = index.m_strRangeSymbol;
    }

    if (pchErrorFilePath != null)
    {
                StreamWriter fp = new StreamWriter(pchErrorFilePath);
                fp.Dispose();
            }

    // €–Ú”•ª‚¾‚¯ƒƒ‚ƒŠŠm•Û.
    int iSize = listElement.Count;
    if (iSize <= 0)
    {
        return true;
    }
    m_pElement = new IntegerRangeDefinitionElement[iSize];
    if (m_pElement == null)
    {
        return false;
    }
    m_iCount = iSize;

            for (int j = 0; j < listElement.Count; j++)
            {
                m_pElement[j] = listElement[j];
            }

            return true;
}

public int GetElementCount() 
{
	return m_iCount;
}

public bool IsSatisfied( string pchRangeSymbol, int iValue) 
{
	int i = 0;
	for( i = 0; i<m_iCount; i++ ){
		if ( m_pElement[i].IsEqualRangeSymbol(pchRangeSymbol ) ){
			return ( m_pElement[i].IsSatisfied(iValue ) );
		}
	}
	return false;
}

public bool GetRange(int iMinValueOperatorID, int iMaxValueOperatorID, int iMinValue, int iMaxValue, string pchRangeSymbol) 
{
	int i = 0;
	for( i = 0; i<m_iCount; i++ ){
		if ( m_pElement[i].IsEqualRangeSymbol(pchRangeSymbol ) ){
			m_pElement[i].GetRange(iMinValueOperatorID, iMaxValueOperatorID, iMinValue, iMaxValue );
			return true;
		}
	}
	return false;
}

    }
}
