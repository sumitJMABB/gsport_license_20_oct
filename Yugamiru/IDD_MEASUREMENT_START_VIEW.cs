﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.IO;
using static Yugamiru.stretchDIBbits;

namespace Yugamiru
{
    public partial class IDD_MEASUREMENT_START_VIEW : Form
    {
        bool m_ShootBtnFlag = false;
        bool m_RotateFlag = false;
        enum MY_STATE
        {
            MY_STATE_STANDING = 0,
            MY_STATE_KNEE_DOWN,
            MY_STATE_SIDE,
            MY_STATE_END
        };


        ImageClipWnd m_ImageClipWnd = new ImageClipWnd();
        ImagePreviewWnd m_ImagePreviewWnd = new ImagePreviewWnd();
        
        PictureBox picturebox1 = new PictureBox();
       
        Bitmap bmBack1;
        JointEditDoc m_JointEditDoc;
        Bitmap m_currentImage;
        
        public IDD_MEASUREMENT_START_VIEW(JointEditDoc GetDocument)
        {
            InitializeComponent();
            m_JointEditDoc = GetDocument;
            
            GetDocument.CreateBmpInfo(1280 * 1024 * 3, 1024, 1280);

            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_on;
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_on;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_on;
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_on;
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_on;

         
            IDC_ID.Text = GetDocument.GetDataID().ToString();
            IDC_Name.Text = GetDocument.GetDataName();
            
            string str = "-";
            if (GetDocument.GetDataGender() == 1) str = "M";
            if (GetDocument.GetDataGender() == 2) str = "F";
            
            IDC_Gender.Text = str;
            string year = string.Empty, month = string.Empty, day = string.Empty;
            GetDocument.GetDataDoB(ref year, ref month, ref day);
            str = month + "." + day + " " + year;
            
            IDC_DoB.Text = str;
            if (GetDocument.GetDataHeight() == 0)
                str = "-";  // g’·
            else
                str = GetDocument.GetDataHeight().ToString(); // g’·
            
            IDC_Height.Text = str;
            string strInstitutionName = string.Empty;
            GetDocument.GetInstitutionName(strInstitutionName);
            


            bmBack1 = Yugamiru.Properties.Resources.Mainpic3;
            picturebox1.Size = new Size(bmBack1.Size.Width, bmBack1.Size.Height);
            picturebox1.BackColor = Color.Transparent;
            this.Controls.Add(picturebox1);
            picturebox1.Image = bmBack1;
            IDC_NextBtn.Visible = false;

           
            switch (GetDocument.GetMeasurementStartViewMode())
            {
                case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                    SetSideStandingMode();
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                    SetFrontStandingMode();
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                    SetFrontKneedownMode();
                    break;
                default:
                    break;
            }
           
        }
        public void DisposeControls()
        {
            this.picturebox1.Image.Dispose();

            this.IDC_BackBtn.Image.Dispose();
            this.IDC_NextBtn.Image.Dispose();
            this.IDC_RotateBtn.Image.Dispose();
            this.IDC_SearchBtn.Image.Dispose();
            this.IDC_ShootBtn.Image.Dispose();

            Yugamiru.Properties.Resources.Mainpic3.Dispose();

            Yugamiru.Properties.Resources.imagecopy_on.Dispose();
            Yugamiru.Properties.Resources.imageload_on.Dispose();
            Yugamiru.Properties.Resources.imagerotation_on.Dispose();
            Yugamiru.Properties.Resources.gobackgreen_on.Dispose();
            Yugamiru.Properties.Resources.gonextgreen_on.Dispose();

            Yugamiru.Properties.Resources.SpeechFrontStanding.Dispose();

            this.Dispose();
            this.Close();

        }

        public void IDD_MEASUREMENT_START_VIEW_SizeChanged(object sender, EventArgs e)
        {
            
            picturebox1.Left = (this.ClientSize.Width - picturebox1.Width) / 2;
            picturebox1.Top = 0;

            IDC_ID.Location = new Point(230 + 10, 50 -10);
            IDC_ID.Size = new Size(90, 24);
            IDC_ID.Font = new Font("HP Simplified Light", 10);

            IDC_Name.Location = new Point(150 + 230 + 10, 50 - 10);
            IDC_Name.Size = new Size(167, 24);
            IDC_Name.Font = new Font("HP Simplified Light", 10);

            IDC_Gender.Location = new Point(300 + 230 + 20, 50 - 10);

            IDC_Gender.Size = new Size(34, 24);
            IDC_Gender.Font = new Font("HP Simplified Light", 10);

            IDC_DoB.Location = new Point(450 + 230 - 50, 50 - 10);
            IDC_DoB.Size = new Size(126, 24);
            IDC_DoB.Font = new Font("HP Simplified Light", 10);

            IDC_Height.Location = new Point(600 + 230 - 30, 50 - 10);
            IDC_Height.Size = new Size(50, 24);
            IDC_Height.Font = new Font("HP Simplified Light", 10);

            IDC_BackBtn.Location = new Point(220, 550 + 80);
            IDC_BackBtn.Size = new Size(112, 42);
            IDC_SearchBtn.Location = new Point(IDC_BackBtn.Left + IDC_BackBtn.Width + 20, 550 + 80);
            IDC_SearchBtn.Size = new Size(112, 42);
            IDC_RotateBtn.Location = new Point(IDC_SearchBtn.Left + IDC_SearchBtn.Width + 20, 550 + 80);
            IDC_RotateBtn.Size = new Size(112, 42);
            IDC_NextBtn.Location = new Point(900, 550 + 80);
            IDC_NextBtn.Size = new Size(112, 42);
            IDC_ShootBtn.Location = new Point(600 + 26, 250);
            IDC_ShootBtn.Size = new Size(112, 42);

            
            ImagePrevWnd.Top = 130;
            ImagePrevWnd.Size = new Size(384, 480);
            
            ImageClipWnd.Top = 130;
            ImageClipWnd.Size = new Size(384, 480);

            IDC_ID.Left = (this.Width  - IDC_ID.Width)/2 - 400;
            IDC_Name.Left =  IDC_ID.Left + 100;
            IDC_Gender.Left = IDC_Name.Left + 170 + 4 ;
            IDC_DoB.Left = IDC_Gender.Left + 56 + 4;
            IDC_Height.Left = IDC_DoB.Left + 180;

            IDC_ShootBtn.Left = (this.Width - IDC_ShootBtn.Width) / 2;
            IDC_ShootBtn.Top = (this.Height - IDC_ShootBtn.Height) / 2;
            ImageClipWnd.Left = (this.Width - ImageClipWnd.Width) / 2- 264;
            ImagePrevWnd.Left =  ImageClipWnd.Left + ImageClipWnd.Width + IDC_ShootBtn.Width + 30;
           
            IDC_BackBtn.Left = ImageClipWnd.Left - 10;
            IDC_SearchBtn.Left = IDC_BackBtn.Left + IDC_BackBtn.Width + 20;
            IDC_RotateBtn.Left = IDC_SearchBtn.Left + IDC_SearchBtn.Width + 20;
            IDC_NextBtn.Left = ImagePrevWnd.Left + ImagePrevWnd.Width - IDC_NextBtn.Width;

            pictureBox2.Left = IDC_Height.Left + 120;
            pictureBox2.Top = 30 + 5;
            
            m_ImagePreviewWnd.m_iOffscreenWidth = ImagePrevWnd.Width;
            m_ImagePreviewWnd.m_iOffscreenHeight = ImagePrevWnd.Height;

            m_ImageClipWnd.m_iOffscreenWidth = ImagePrevWnd.Width;
            m_ImageClipWnd.m_iOffscreenHeight = ImagePrevWnd.Height;
            
        }

        
        public void SetSideStandingMode()
        {
            //int iCmdShowNextButtonOnNewInput = Constants.SW_HIDE;
            pictureBox2.Visible = false;

            if (m_JointEditDoc.m_SideImageBytes == null )
            {
                //string strScoresheetFolderPath;
                //m_JointEditDoc.GetScoresheetFolderPath(strScoresheetFolderPath);
                Image<Bgr, Byte> EmguCVImage = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.sokui);
                EmguCVImage = EmguCVImage.Resize(384, 480, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                m_JointEditDoc.SetFlipImage(EmguCVImage.ToBitmap());
                m_ImageClipWnd.SetBackgroundBitmap(EmguCVImage);
                m_ImagePreviewWnd.SetBackgroundBitmap(new Image<Bgr, Byte>(Yugamiru.Properties.Resources.sokui));
            }
            else
            {
                m_ImageClipWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_SideImageBytes);
                m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_SideImageBytes);
                //iCmdShowNextButtonOnNewInput = Constants.SW_SHOW;
            }
            //m_JointEditDoc.AllocSideImage(m_ImageClipWnd.m_pbyteBits);//new line
            m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING);
            
            ImageClipWnd.Invalidate();           
            ImagePrevWnd.Invalidate();
            
            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        
                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        IDC_BackBtn.Visible = false;
                        IDC_NextBtn.Visible = true;
                    }
                    break;
                default:
                    break;
            }
        }

        public void SetFrontStandingMode()
        {

            //int iCmdShowNextButtonOnNewInput = Constants.SW_HIDE;
            pictureBox2.Visible = true;
            pictureBox2.Image = Yugamiru.Properties.Resources.SpeechFrontStanding;
            pictureBox2.Width = pictureBox2.Image.Width;
            pictureBox2.Height = pictureBox2.Image.Height;
            

            if (m_JointEditDoc.m_FrontStandingImageBytes == null)
            {
                Image<Bgr, Byte> EmguCVImage = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.ritui);
                EmguCVImage = EmguCVImage.Resize(384, 480, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                m_JointEditDoc.SetFlipImage(EmguCVImage.ToBitmap());
                m_ImageClipWnd.SetBackgroundBitmap(EmguCVImage);
                    m_ImagePreviewWnd.SetBackgroundBitmap(new Image<Bgr, Byte>(Yugamiru.Properties.Resources.ritui));
            }
            else
            {
                m_ImageClipWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontStandingImageBytes);
                byte[] showbytes = m_JointEditDoc.m_FrontStandingImageBytes;
                m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontStandingImageBytes);
                //iCmdShowNextButtonOnNewInput = Constants.SW_SHOW;
            }
            //m_JointEditDoc.AllocStandingImage(m_ImageClipWnd.m_pbyteBits);//new line
            m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING);
            ImageClipWnd.Invalidate();
            ImagePrevWnd.Invalidate();
            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        
                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        IDC_BackBtn.Visible = true;
                        IDC_BackBtn.Image = Properties.Resources.gobackgreen_up;
                        IDC_NextBtn.Visible = true;
                       
                    }
                    break;
                default:
                    break;
            }
        }

        
            public void SetFrontKneedownMode()
        {
            //int iCmdShowNextButtonOnNewInput = Constants.SW_HIDE;
            pictureBox2.Visible = true;
            pictureBox2.Image = Yugamiru.Properties.Resources.SpeechFrontKneedown;
            pictureBox2.Width = pictureBox2.Image.Width;
            pictureBox2.Height = pictureBox2.Image.Height;
            

            if (m_JointEditDoc.m_FrontKneedownImageBytes == null)
            {
               
                Image<Bgr, Byte> EmguCVImage = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.kutui);
                EmguCVImage = EmguCVImage.Resize(384, 480, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                m_JointEditDoc.SetFlipImage(EmguCVImage.ToBitmap());
                m_ImageClipWnd.SetBackgroundBitmap(EmguCVImage);
                m_ImagePreviewWnd.SetBackgroundBitmap(new Image<Bgr, Byte>(Yugamiru.Properties.Resources.kutui));

            }
            else
            {
                m_ImageClipWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontKneedownImageBytes);
                m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontKneedownImageBytes);
                //iCmdShowNextButtonOnNewInput = Constants.SW_SHOW;
            }
            //m_JointEditDoc.AllocKneedownImage(m_ImageClipWnd.m_pbyteBits);//new line
            m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN);
            ImageClipWnd.Invalidate();
            ImagePrevWnd.Invalidate();

            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                       
                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                       IDC_BackBtn.Image = Properties.Resources.gobackgreen_up;
                        IDC_NextBtn.Visible = true;
                
                    
                    }
                    break;
                default:
                    break;
            }
        }


        private void ImageClipWnd_SizeChanged(object sender, EventArgs e)
        {
        }

        private void ImageClipWnd_Paint(object sender, PaintEventArgs e)
        {
            
                 if(m_RotateFlag)
                  m_ImageClipWnd.RotateClockwiseBackgroundBitmap(e.Graphics);
               
            m_ImageClipWnd.UpdateOffscreen(e.Graphics);
        }

        private void ImageClipWnd_MouseMove(object sender, MouseEventArgs e)
        {
         if (m_ImageClipWnd.m_iMouseCaptureMode > 0)
            {
                int iSelectionFrameCenterX = (e.X - m_ImageClipWnd.m_iDestRectUpperLeftCornerX) *
                    m_ImageClipWnd.m_iBackgroundWidth / m_ImageClipWnd.m_iDestRectWidth;
                if (iSelectionFrameCenterX < 0)
                {
                    iSelectionFrameCenterX = 0;
                }
                if (iSelectionFrameCenterX >= m_ImageClipWnd.m_iBackgroundWidth)
                {
                    iSelectionFrameCenterX = m_ImageClipWnd.m_iBackgroundWidth;
                }
                m_ImageClipWnd.m_iSelectionFrameUpperLeftCornerX = iSelectionFrameCenterX -
                    m_ImageClipWnd.m_iSelectionFrameWidth / 2;
                ImageClipWnd.Invalidate();





            }

        }

        private void ImageClipWnd_MouseUp(object sender, MouseEventArgs e)
        {
            m_ImageClipWnd.m_iMouseCaptureMode = 0;
        }

        private void ImageClipWnd_MouseDown(object sender, MouseEventArgs e)
        {
           m_ImageClipWnd.m_iMouseCaptureMode = 1;
            int iSelectionFrameCenterX = (e.X - m_ImageClipWnd.m_iDestRectUpperLeftCornerX) *
                m_ImageClipWnd.m_iBackgroundWidth / m_ImageClipWnd.m_iDestRectWidth;
            if (iSelectionFrameCenterX < 0)
            {
                iSelectionFrameCenterX = 0;
            }
            if (iSelectionFrameCenterX >= m_ImageClipWnd.m_iBackgroundWidth)
            {
                iSelectionFrameCenterX = m_ImageClipWnd.m_iBackgroundWidth;
            }
            m_ImageClipWnd.m_iSelectionFrameUpperLeftCornerX = iSelectionFrameCenterX -
                m_ImageClipWnd.m_iSelectionFrameWidth / 2;
        
        }

        private void IDC_ShootBtn_Click(object sender, EventArgs e)
        {
            IDC_NextBtn.Visible = true;
            m_ShootBtnFlag = true;                               
                     

            ImagePrevWnd.Invalidate();
            
           

        }

        private void IDC_SearchBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            

            openFileDialog.InitialDirectory = "Desktop";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;
            openFileDialog.Title = "Image Select";

            // image filters
            openFileDialog.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                
                m_currentImage = new Bitmap(openFileDialog.FileName);
                m_JointEditDoc.SetFlipImage(m_currentImage);
                m_ImageClipWnd.SetBackgroundBitmap(new Image<Bgr, Byte>(openFileDialog.FileName));
                
                ImageClipWnd.Invalidate();

                if (GlobalVar.positionNameOne.Length == 0)
                {
                    GlobalVar.positionNameOne = "FirstImage";
                    GlobalVar.OriginalImages.Add(GlobalVar.positionNameOne, openFileDialog);
                }
                else if (GlobalVar.positionNameTwo.Length == 0)
                {
                    GlobalVar.positionNameTwo = "SecondImage";
                    GlobalVar.OriginalImages.Add(GlobalVar.positionNameTwo, openFileDialog);
                }
                else if (GlobalVar.positionNameThree.Length == 0)
                {
                    GlobalVar.positionNameThree = "ThirdImage";
                    GlobalVar.OriginalImages.Add(GlobalVar.positionNameThree, openFileDialog);
                }
                //imageGlobal.images.Add(openFileDialog);

            }
        }

                

        private void IDC_BackBtn_Click(object sender, EventArgs e)
        {

            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        switch (m_JointEditDoc.GetMeasurementStartViewMode())
                        {
                            case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                                // ŒÂlî•ñ‰æ–Ê‚É‘JˆÚ‚·‚é.
                                m_JointEditDoc.SetMeasurementStartViewMode
                                    (Constants.MEASUREMENTSTARTVIEWMODE_NONE);
                                //m_JointEditDoc.ChangeToMeasurementView();
                                CloseForm(EventArgs.Empty);
                                this.Close();
                                DisposeControls();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                                SetSideStandingMode();                                
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                                SetFrontStandingMode();
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        switch (m_JointEditDoc.GetMeasurementStartViewMode())
                        {
                            case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                                // ‚±‚±‚É‚­‚é‚±‚Æ‚Í‚È‚¢.
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                                SetSideStandingMode();                               
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                                SetFrontStandingMode();
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                default:
                    break;
            }

            
        }


        public event EventHandler closeForm; // creating event handler - step1
        public void CloseForm(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = closeForm;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        public void IDC_NextBtn_Click(object sender, EventArgs e)
        {
            byte[] pbyteBits = new byte[1024 * 1280 * 3];
            Bitmap bmp = new Bitmap(ImagePrevWnd.ClientSize.Width, ImagePrevWnd.ClientSize.Height);
            ImagePrevWnd.DrawToBitmap(bmp, ImagePrevWnd.ClientRectangle);
            Image<Bgr, byte> EmguCVPreviewImage = new Image<Bgr, byte>(bmp);
            EmguCVPreviewImage = EmguCVPreviewImage.Resize(1024, 1280, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            pbyteBits = EmguCVPreviewImage.Bytes;

            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        switch (m_JointEditDoc.GetMeasurementStartViewMode())
                        {
                            case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                                IDC_NextBtn.Visible = false;
                                m_JointEditDoc.AllocSideImage(pbyteBits);
                                SetFrontStandingMode();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                                IDC_NextBtn.Visible = false;
                                m_JointEditDoc.AllocStandingImage(pbyteBits);
                                 SetFrontKneedownMode();
                               // SetFrontStandingMode();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                                m_JointEditDoc.AllocKneedownImage(pbyteBits);
                                m_JointEditDoc.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP);
                                m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_NONE);
                                CloseFormToStartNextScreen(EventArgs.Empty);


                                this.Close();
                                DisposeControls();
                                //m_JointEditDoc.ChangeToJointEditView();
                                break;
                            default:
                                break;
                        }
                    }
                            break;
                        case Constants.INPUTMODE_MODIFY:
                    {
                        switch (m_JointEditDoc.GetMeasurementStartViewMode())
                        {
                            case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                                m_JointEditDoc.AllocSideImage(pbyteBits);
                                SetFrontStandingMode();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                                m_JointEditDoc.AllocStandingImage(pbyteBits);
                                SetFrontKneedownMode();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                                // Œ‹‰Ê‰æ–Ê‚É‘JˆÚ‚·‚é.
                                //GetDocument()->ChangeToResultView();
                                m_JointEditDoc.AllocKneedownImage(pbyteBits);
                                m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_NONE);
                           
                                FunctionToDisplayResultViewFromCrouchedView(EventArgs.Empty);
                               
                             
                              
                                this.Close();
                                DisposeControls();

                                break;
                            default:
                                break;

                        }
                    }
                    break;
                        default :
                        break;

                    }
         
        }
        
      
        public event EventHandler EventToStartNextScreen; // creating event handler - step1
        public void CloseFormToStartNextScreen(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToStartNextScreen;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
        public event EventHandler EventfromCrouchedViewtoResultView;
        public void FunctionToDisplayResultViewFromCrouchedView(EventArgs e)
        {
            EventHandler eventHandler = EventfromCrouchedViewtoResultView;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }

        }

        private void ImagePrevWnd_Paint(object sender, PaintEventArgs e)
        {
           
                int iSelectedBitmapWidth = m_ImageClipWnd.GetSelectedBitmapWidth();
                int iSelectedBitmapHeight = m_ImageClipWnd.GetSelectedBitmapHeight();
                int iSelectedBitmapSize = m_ImageClipWnd.CalcSelectedBitmapSize();
                byte[] pbyteBits = new byte[1024 * 1280 * 3];
                m_ImageClipWnd.GetSelectedBitmap(e.Graphics, ref pbyteBits);


        }

        private void IDC_RotateBtn_Click(object sender, EventArgs e)
        {
            
            Bitmap bmp = m_JointEditDoc.GetFlipImage();
            bmp.RotateFlip(RotateFlipType.Rotate270FlipXY);
           
            Image<Bgr, Byte> EmguCVImage = new Image<Bgr, Byte>(bmp);
            
            int iDestImageWidth =m_ImageClipWnd.m_iBackgroundHeight;
            int iDestImageHeight = m_ImageClipWnd. m_iBackgroundWidth;
            m_ImageClipWnd.SetBackgroundBitmap(iDestImageWidth, iDestImageHeight,EmguCVImage.Bytes);
            ImageClipWnd.Invalidate();

        }

        private void IDD_MEASUREMENT_START_VIEW_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.DrawImage(bmBack1, (this.Width - bmBack1.Width) / 2, 0, bmBack1.Width, bmBack1.Height);
        }

        private void IDD_MEASUREMENT_START_VIEW_Resize(object sender, EventArgs e)
        {
            //MessageBox.Show("hi");
        }
    }

}
   