﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class MultiChar
    {
        public char[] m_achChar = new char[2];
        public int m_iValidCount;

       public MultiChar()
        {
            m_iValidCount = 0;
            m_achChar[0] = '\0';
            m_achChar[1] = '\0';
        }

       MultiChar( MultiChar rSrc ) 
        {
            m_iValidCount = rSrc.m_iValidCount;
            m_achChar[0] = rSrc.m_achChar[0];
            m_achChar[1] = rSrc.m_achChar[1];
        }

 ~MultiChar()
{
}
        /*
        CMultiChar &CMultiChar::operator=( const CMultiChar &rSrc )
        {
            m_achChar[0]	= rSrc.m_achChar[0];
            m_achChar[1]	= rSrc.m_achChar[1];
            m_iValidCount	= rSrc.m_iValidCount;
            return( *this );
        }*/

        public bool IsEqual(UInt32 chCheck) {
	if ( m_achChar[0] != chCheck ){
		return false ;
	}
	if ( m_iValidCount != 1 ){
		return false ;
	}
	return true ;
}

        public bool IsEqual( MultiChar rCheck )
{
    if (m_iValidCount != rCheck.m_iValidCount)
    {
        return (false);
    }
    if (m_iValidCount >= 1)
    {
        if (m_achChar[0] != rCheck.m_achChar[0])
        {
            return (false);
        }
    }
    if (m_iValidCount >= 2)
    {
        if (m_achChar[1] != rCheck.m_achChar[1])
        {
            return (false);
        }
    }
    return (true);
}

        public bool IsHankakuDigit() 
{
            	if ( m_iValidCount != 1 ){
                    return( false );
                }
                if ( m_achChar[0] < '0' ){
                    return( false );
                }
                if ( m_achChar[0] > '9' ){
                    return( false );
                }
                return( true );
            
}

        public bool IsHankakuAlphabet() {
	if ( m_iValidCount != 1 ){
		return( false );
	}
	if (( 'A' <= m_achChar[0] ) && ( m_achChar[0] <= 'Z' )){
		return( true );
	}
	if (( 'a' <= m_achChar[0] ) && ( m_achChar[0] <= 'z' )){
		return( true );
	}
	return( false );
}

        public int GetHankakuDigitValue() 
{
	if ( !IsHankakuDigit() ){
		return 0;
	}
	return( m_achChar[0] - '0' );
}

    }
}
