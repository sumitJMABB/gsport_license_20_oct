﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class SideBodyResultData
    {
        int m_iSideBodyBalance;             //@‘ÌŠ²‰ñù
        int m_iNeckForwardLeaning;          //  Žñ‘OŒX.
        int m_iPosturePatternID;
        public SideBodyResultData( )
        { 

    m_iSideBodyBalance = 0 ;

    m_iNeckForwardLeaning = 0 ;

            m_iPosturePatternID = Constants.POSTUREPATTERNID_NONE;
        
        }

        public SideBodyResultData( SideBodyResultData rSrc )
        {

            m_iSideBodyBalance = rSrc.m_iSideBodyBalance;

            m_iNeckForwardLeaning = rSrc.m_iNeckForwardLeaning;

            m_iPosturePatternID = rSrc.m_iPosturePatternID;
        
        }

     

    public void Clear( )
    {
        m_iSideBodyBalance = -999;
        m_iNeckForwardLeaning = -999;
        m_iPosturePatternID = Constants.POSTUREPATTERNID_NONE;
    }

   public void Reset()
    {
        m_iSideBodyBalance = 0;
        m_iNeckForwardLeaning = 0;
        m_iPosturePatternID = Constants.POSTUREPATTERNID_NONE;
    }

    public void Calc(int sex, int gene,
    		 SideBodyAngle SideBodyAngle,
    		ResultActionScriptMgr ResultActionScriptMgr )
    {
           // SideBodyResultData temp_SideBodyResultData = new SideBodyResultData();
        ResultActionScriptMgr.Execute(this, SideBodyAngle);
    }

      public int GetSideBodyBalance() 
{
	return m_iSideBodyBalance;
}

    public int GetNeckForwardLeaning() 
{
	return m_iNeckForwardLeaning;
}

public int GetPosturePatternID() 
{
	return m_iPosturePatternID;
}

public void SetDataByResultID(int iResultID, int iValue)
{
    switch (iResultID)
    {
        case Constants.RESULTID_RIGHTKNEE:
        case Constants.RESULTID_LEFTKNEE:
        case Constants.RESULTID_HIP:
        case Constants.RESULTID_CENTERBALANCE:
        case Constants.RESULTID_SHOULDERBALANCE:
        case Constants.RESULTID_EARBALANCE:
        case Constants.RESULTID_BODYCENTER:
        case Constants.RESULTID_HEADCENTER:
            break;
        case Constants.RESULTID_SIDEBODYBALANCE:
            m_iSideBodyBalance = iValue;
            break;
        case Constants.RESULTID_NECKFORWARDLEANING:
            m_iNeckForwardLeaning = iValue;
            break;
        case Constants.RESULTID_POSTUREPATTERN_NORMAL:
            if (iValue != 0)
            {
                m_iPosturePatternID = Constants.POSTUREPATTERNID_NORMAL;
            }
            break;
        case Constants.RESULTID_POSTUREPATTERN_STOOP_AND_BEND_BACKWARD:
            if (iValue != 0)
            {
                m_iPosturePatternID = Constants.POSTUREPATTERNID_STOOP_AND_BEND_BACKWARD;
            }
            break;
        case Constants.RESULTID_POSTUREPATTERN_STOOP:
            if (iValue != 0)
            {
                m_iPosturePatternID = Constants.POSTUREPATTERNID_STOOP;
            }
            break;
        case Constants.RESULTID_POSTUREPATTERN_BEND_BACKWARD:
            if (iValue != 0)
            {
                m_iPosturePatternID = Constants.POSTUREPATTERNID_BEND_BACKWARD;
            }
            break;
        case Constants.RESULTID_POSTUREPATTERN_FLATBACK:
            if (iValue != 0)
            {
                m_iPosturePatternID = Constants.POSTUREPATTERNID_FLATBACK;
            }
            break;
        case Constants.RESULTID_POSTUREPATTERN_FRONTSIDE_UNBALANCED:
            if (iValue != 0)
            {
                m_iPosturePatternID = Constants.POSTUREPATTERNID_FRONTSIDE_UNBALANCED;
            }
            break;
        default:
            break;
    }
}


    }
}
