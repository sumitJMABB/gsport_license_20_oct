﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Yugamiru.stretchDIBbits;
using System.Drawing;


namespace Yugamiru
{
    public class MuscleBitmapFileImage
    {
        public int m_iSize;
        public byte[] m_pbyteFileImage;
        public int m_height;
        public int m_width;

        public MuscleBitmapFileImage()
        {
            m_iSize = 0;
            m_pbyteFileImage = new byte[m_iSize];

        }



        public bool Create(int iWidth, int iHeight)
        {
            if (m_pbyteFileImage != null)
            {

                m_pbyteFileImage = null;
            }
            m_iSize = 0;
            int iSize = 1078 + (iWidth + 3) / 4 * 4 * iHeight;

            m_pbyteFileImage = new byte[iSize];
            if (m_pbyteFileImage == null)
            {
                return false;
            }
            m_iSize = iSize;
            m_width = iWidth;
            m_height = iHeight;
            /*
                BITMAPFILEHEADER* pbfh = (BITMAPFILEHEADER*)(m_pbyteFileImage);
                BITMAPINFOHEADER* pbih = (BITMAPINFOHEADER*)(m_pbyteFileImage + sizeof(BITMAPFILEHEADER));

                pbfh->bfType = 'M' * 256 + 'B';
                pbfh->bfSize = iSize;
                pbfh->bfReserved1 = 0;
                pbfh->bfReserved2 = 0;
                pbfh->bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + sizeof(stretchDIBbits.RGBQUAD) * 256; ;
                pbih->biSize = sizeof(BITMAPINFOHEADER);
                pbih->biWidth = iWidth;
                pbih->biHeight = iHeight;
                pbih->biPlanes = 1;
                pbih->biBitCount = 8;
                pbih->biCompression = 0;
                pbih->biSizeImage = 0;
                pbih->biXPelsPerMeter = 0;
                pbih->biYPelsPerMeter = 0;
                pbih->biClrUsed = 0;
                pbih->biClrImportant = 0;

                RGBQUAD* pRGBQuad = (RGBQUAD*)(m_pbyteFileImage + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER));
                int i = 0;
                for (i = 0; i < 256; i++)
                {
                    pRGBQuad->rgbBlue = (BYTE)i;
                    pRGBQuad->rgbGreen = (BYTE)i;
                    pRGBQuad->rgbRed = (BYTE)i;
                    pRGBQuad->rgbReserved = 0;
                    pRGBQuad++;
                }
                */
            int iHeaderSize = 1078;/*sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * 256;*/

                for (int i = 0; i < m_iSize - iHeaderSize; i++)
                {
                    m_pbyteFileImage[iHeaderSize + i] = 0;
                }

            return true;
        }

        public bool Load(Image pchFileName)
        {
            if (m_pbyteFileImage != null)
            {

                m_pbyteFileImage = null;
            }
            m_width = pchFileName.Width;
            m_height = pchFileName.Height;
            m_iSize = 0;
            ImageConverter _imageConverter = new ImageConverter();
            m_pbyteFileImage = (byte[])_imageConverter.ConvertTo(pchFileName, typeof(byte[]));
            //m_pbyteFileImage = new byte[iSize];
            m_iSize = m_pbyteFileImage.Length;
           
            
            return true;

        }
        /*
        BOOL CMuscleBitmapFileImage::Save( const char* pchFileName) const
        {
            FILE* fp = fopen(pchFileName, "wb");
            if ( fp == NULL ){
                return FALSE;
            }
            if ( fwrite( &(m_pbyteFileImage[0]), 1, m_iSize, fp ) < (size_t)m_iSize ){

                fclose(fp );
        fp = NULL;
                return FALSE;
            }

            fclose(fp );
        fp = NULL;
            return TRUE;
        }
        */
        public int GetWidth() 
        {
            /* if ( m_pbyteFileImage == null ){
                 return 0;
             }
             if ( m_iSize< sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) ){
                 return 0;
             }
             BITMAPINFOHEADER* pbih = (BITMAPINFOHEADER*)(m_pbyteFileImage + sizeof(BITMAPFILEHEADER));
             return ( pbih->biWidth );*/
            return m_width;
        }

        public int GetHeight() 
        {
            /*   if ( m_pbyteFileImage == NULL ){
                   return 0;
               }
               if ( m_iSize< sizeof( BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) ){
                   return 0;
               }
               BITMAPINFOHEADER* pbih = (BITMAPINFOHEADER*)(m_pbyteFileImage + sizeof(BITMAPFILEHEADER));
               return ( pbih->biHeight );*/
            return m_height;
        }

        public int GetSize() 
        {
            return m_iSize;
        }

       public byte GetImageData(int iIndex) 
        {
            if ( m_pbyteFileImage == null ){
                return 0;
            }
            if ( iIndex< 0 ){
                return 0;
            }
            if ( iIndex + 1078/*sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD)*256*/ >= m_iSize ){
                return 0;
            }
            return m_pbyteFileImage[iIndex + 1078/*sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * 256*/];
        }
        
        public bool Merge( MuscleBitmapFileImage rSrc )
        {
            if (GetWidth() != rSrc.GetWidth())
            {
                return false;
            }
            if (GetHeight() != rSrc.GetHeight())
            {
                return false;
            }
            int iHeaderSize = 1078;//(stretchDIBbits.BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * 256;
            int iWidth = GetWidth();
            int iHeight = GetHeight();
            int iLineStep = (iWidth + 3) / 4 * 4;
            int i = 0;
            int j = 0;
            for (i = 0; i < iHeight; i++)
            {
                for (j = 0; j < iWidth; j++)
                {
                    int iSum = m_pbyteFileImage[iHeaderSize + iLineStep * i + j] + rSrc.m_pbyteFileImage[iHeaderSize + iLineStep * i + j];
                    if (iSum > 255)
                    {
                        iSum = 255;
                    }
                    m_pbyteFileImage[iHeaderSize + iLineStep * i + j] = (byte)iSum;
                }
            }
            return true;
        }

    }
}
