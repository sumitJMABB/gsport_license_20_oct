﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Yugamiru
{
    public class ResultActionScriptMgr
    {
        int m_iCount = 0;
        ResultActionScriptElement[] m_pElement;
        SymbolFunc m_SymbolFunc;





        // Šp“xƒAƒNƒVƒ‡ƒ“ƒXƒNƒŠƒvƒgƒ}ƒXƒ^[ƒf[ƒ^.
       public ResultActionScriptMgr()
        {
            m_SymbolFunc = new SymbolFunc();
            m_pElement = new ResultActionScriptElement[m_iCount];
        }



        public ResultActionScriptMgr(ResultActionScriptMgr rSrc)
        {
            m_iCount = rSrc.m_iCount;

            m_pElement = null;
            if (rSrc.m_pElement != null)
            {
                
                int i = 0;
                for (i = 0; i < m_iCount; i++)
                {
                    m_pElement[i] = rSrc.m_pElement[i];
                }
            }
        }
        
         ~ResultActionScriptMgr( )
        {
            if ( m_pElement != null ){
               // delete[] m_pElement;
            m_pElement = null;
            }
        }

   /*     ResultActionScriptMgr ( ResultActionScriptMgr rSrc )
        {
            if (m_pElement != NULL)
            {
                delete[] m_pElement;
                m_pElement = NULL;
            }
            m_iCount = rSrc.m_iCount;
            if (rSrc.m_pElement != NULL)
            {
                m_pElement = new CResultActionScriptElement[m_iCount];
                int i = 0;
                for (i = 0; i < m_iCount; i++)
                {
                    m_pElement[i] = rSrc.m_pElement[i];
                }
            }
            return *this;
        }
        */
        
        public bool ReadFromFile(string lpszFolderName, string lpszFileName,  string pchErrorFilePath /* = NULL */ )
        {
            char[] buf = new char[1024];
            
            List<ResultActionScriptElement> listElement = new List<ResultActionScriptElement>();

            if (m_pElement != null)
            {
                //delete[] m_pElement;
                m_pElement = null;
            }
            m_iCount = 0;

            DualSourceTextReader DualSourceTextReader = new DualSourceTextReader();
            int i = DualSourceTextReader.Open(lpszFolderName, lpszFileName);
            if (i == 0)
            {
                return false;
            }

            string strError = string.Empty;
            int iLineNo = 1;
            while (!(DualSourceTextReader.IsEOF()))
            {
                buf[0] = '\0';
                DualSourceTextReader.Read( ref buf,1024);
                //var t = File.ReadAllLines(@"Resources\ResultAction.inf");
                ResultActionScriptElement Element = new ResultActionScriptElement();
                
                    int iErrorCode = Element.ReadFromString( ref buf);
                    if (iErrorCode == Constants.READFROMSTRING_SYNTAX_OK)
                    {
                        //listElement.push_back(Element);
                        listElement.Add(Element);
                    }
                    else
                    {
                        m_SymbolFunc.OutputReadErrorString(strError, iLineNo, iErrorCode);
                    }
                    iLineNo++;               
            }

             if (pchErrorFilePath != null)
               {
                /*  FileStream fp = new FileStream(pchErrorFilePath, FileMode.Open, FileAccess.Write, FileShare.Write);
                  //FILE* fp = ::fopen(pchErrorFilePath, "wt");

                  ::fwrite(strError, 1, strError.Length, fp );

                  ::fclose(fp);
                  fp = null;*/
                StreamWriter fp = new StreamWriter(pchErrorFilePath);
                fp.Dispose();
                   
               }

            // €–Ú”•ª‚¾‚¯ƒƒ‚ƒŠŠm•Û.
            int iSize = listElement.Count;
            if (iSize <= 0)
            {
                return true;
            }
            m_pElement = new ResultActionScriptElement[iSize];
            if (m_pElement == null)
            {
                return false;
            }
            m_iCount = iSize;

            //int i = 0;
            //List<ResultActionScriptElement> iterator index;
            for (int j = 0; j < listElement.Count; j++)
            {
                m_pElement[j] = listElement[j];
            }

            return true;
        }

        public bool ResolveRange(RealValueRangeDefinitionMgr RangeDefinitionMgr, 
        string pchErrorFilePath /* = NULL */ )
        {
            bool bRet = true;
            string strError = string.Empty;
            int i = 0;
            for (i = 0; i < m_iCount; i++)
            {
                if (m_pElement[i].m_iScriptOpecodeID == m_SymbolFunc.ANGLESCRIPTOPECODEID_CHECK)
                {
                    string strRangeSymbol = m_pElement[i].m_strRangeSymbol;
                    int iMinValueOperatorID = 0;
                    int iMaxValueOperatorID = 0;
                    double dMinValue = 0.0;
                    double dMaxValue = 0.0;
                    if (RangeDefinitionMgr.GetRange(ref iMinValueOperatorID, ref iMaxValueOperatorID, ref dMinValue, ref dMaxValue, strRangeSymbol))
                    {
                        m_pElement[i].ResolveRange(iMinValueOperatorID, iMaxValueOperatorID, dMinValue, dMaxValue);
                    }
                    else
                    {
                        string strTmp;
                        strTmp = "Symbol %s is undefined\n" +strRangeSymbol;
                        strError += strTmp;
                        bRet = false;
                    }
                }
            }
            if (pchErrorFilePath != null)
            {
                /* FILE* fp = ::fopen(pchErrorFilePath, "wt");

                 ::fwrite((const char*)strError, 1, strError.GetLength(), fp );

                 ::fclose(fp);
                 fp = NULL;*/

                StreamWriter sw = new StreamWriter(pchErrorFilePath);
                sw.Write(strError);

                sw.Dispose();
                
            }
            return bRet;
        }
        

        public bool Execute(FrontBodyResultData FrontBodyResultData, FrontBodyAngle Angle)
        {
            SideBodyAngle AngleSide = new SideBodyAngle();
            bool bContextFlag = true;
            //m_SymbolFunc = new SymbolFunc();
            int i = 0;
            for (i = 0; i < m_iCount; i++)
            {

                if (m_pElement[i].m_iScriptOpecodeID == m_SymbolFunc.ANGLESCRIPTOPECODEID_START)
                {
                    switch (m_pElement[i].m_iBodyPositionTypeID)
                    {

                        case Constants.BODYPOSITIONTYPEID_STANDING:
                            bContextFlag = (Angle.IsStanding());
                            break;
                        case Constants.BODYPOSITIONTYPEID_KNEEDOWN:
                            bContextFlag = !(Angle.IsStanding());
                            break;
                        case Constants.BODYPOSITIONTYPEID_COMMON:
                            bContextFlag = true;
                            break;
                        default:
                            bContextFlag = false;
                            break;
                    }
                }
                else if (m_pElement[i].m_iScriptOpecodeID == m_SymbolFunc.ANGLESCRIPTOPECODEID_CHECK)
                {
                    int iBodyAngleID = m_pElement[i].m_iBodyAngleID;
                    // strAngleSymbol‚©‚ç•Ï”‚Ì’l‚ðdValue‚É‹‚ß‚é.			
                    double dValue = m_SymbolFunc.GetBodyAngleValue(Angle, AngleSide, iBodyAngleID);
                    if (!(m_pElement[i].IsSatisfied(dValue)))
                    {
                        bContextFlag = false;
                    }
                }
                else if (m_pElement[i].m_iScriptOpecodeID == m_SymbolFunc.ANGLESCRIPTOPECODEID_END)
                {
                    if (bContextFlag)
                    {
                        int iResultID = m_pElement[i].m_iResultID;
                        int iResultValue = m_pElement[i].m_iResultValue;
                        FrontBodyResultData.SetDataByResultID(iResultID, iResultValue);
                    }
                }
            }
            return true;
        }
        
        public bool Execute(SideBodyResultData SideBodyResultData, SideBodyAngle AngleSide ) 
        {
            FrontBodyAngle Angle = new FrontBodyAngle();
        bool bContextFlag = true;
        int i = 0;
            for( i = 0; i<m_iCount; i++ ){
                if ( m_pElement[i].m_iScriptOpecodeID == m_SymbolFunc.ANGLESCRIPTOPECODEID_START)
                {
                    bContextFlag = true;
                }
                else if ( m_pElement[i].m_iScriptOpecodeID == m_SymbolFunc.ANGLESCRIPTOPECODEID_CHECK)
                {
                    int iBodyAngleID = m_pElement[i].m_iBodyAngleID;
        // strAngleSymbol‚©‚ç•Ï”‚Ì’l‚ðdValue‚É‹‚ß‚é.			
        double dValue = m_SymbolFunc.GetBodyAngleValue(Angle, AngleSide, iBodyAngleID);
                    if ( !(m_pElement[i].IsSatisfied(dValue )) ){
                        bContextFlag = false;
                    }
                }
                else if ( m_pElement[i].m_iScriptOpecodeID == m_SymbolFunc.ANGLESCRIPTOPECODEID_END)
                {
                    if ( bContextFlag ){
                        int iResultID = m_pElement[i].m_iResultID;
        int iResultValue = m_pElement[i].m_iResultValue;
        SideBodyResultData.SetDataByResultID( iResultID, iResultValue );
                    }
                }
            }
            return true;
        }
   


    }
}
