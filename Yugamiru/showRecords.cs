﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;


namespace Yugamiru
{
    public partial class showRecords : Form
    {
        SQLiteConnection yugamiru_dbconnection;

        public showRecords()
        {
            InitializeComponent();
            dtpDOC.Format = DateTimePickerFormat.Custom;
            dtpDOC.CustomFormat = "dd-MM-yyyy";
            dtpDOB.Format = DateTimePickerFormat.Custom;
            dtpDOB.CustomFormat = "dd-MM-yyyy";

            dgvResult.AutoGenerateColumns = true;
            dgvResult.Columns.Clear();

            dgvResult.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.Fill);


            ///Language Setting///////////////
          


   


            ////////////////////////////////////////////////////*******Database Connection*********************///////////////////////

            if (File.Exists("yugamirus.sqlite"))
                {
                    // MessageBox.Show("File Exists");
                }
                else
            { 
                    MessageBox.Show("File Not Found");
            }



            try
            {
                yugamiru_dbconnection = new SQLiteConnection("DataSource=yugamirus.sqlite;version=3");
                yugamiru_dbconnection.Open();
                string sql = @"select PatientID, Patientname, Score, Rank, ComputerID, strftime('%d-%m-%Y',PatientDOB) 
                  as PatientDOB, strftime('%d-%m-%Y',Dateofcreation) as Dateofcreation from tblmaster_yugamiru";
                SQLiteCommand command = new SQLiteCommand(sql, yugamiru_dbconnection);
                command.ExecuteNonQuery();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable("tblmaster_yugamiru");
                adapter.Fill(dt);
                BindingSource bs = new BindingSource();
                bs.DataSource = dt;
                dgvResult.DataSource = bs;
                adapter.Update(dt);
                yugamiru_dbconnection.Close();
            }

            catch(SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
                Logger.LogError(ex);
            }

        }


       




        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvResult.AutoGenerateColumns = true;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtSearchName_TextChanged(object sender, EventArgs e)
        {
            if (txtSearchName.Text.Length != 0)
            {
                if (!System.Text.RegularExpressions.Regex.IsMatch(txtSearchName.Text, "^[a-zA-Z]"))
                {
                    MessageBox.Show("This textbox accepts only alphabetical characters");
                    txtSearchName.Text.Remove(txtSearchName.Text.Length - 1);
                }

            }
        }
        public void getSearchData()
        {
            try
            {
                foreach (DataGridViewRow dr in dgvResult.Rows)
                {
                    dr.Visible = true;

                }
                if (txtSearchName.Text.Trim().Length > 0)
                {
                    for (int r = 0; r < dgvResult.Rows.Count; r++)
                    {
                        if (dgvResult.Rows[r].Visible)
                        {
                            if (dgvResult.Rows[r].Cells[0].Value == null)
                            {
                                continue;
                            }
                            if (dgvResult.Rows[r].Cells["Patientname"].Value.ToString().ToUpper().Contains(txtSearchName.Text.Trim().ToUpper()))
                            {
                                //dgvResult.Rows[r].Visible = true;
                                CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgvResult.DataSource];
                                currencyManager1.SuspendBinding();
                                dgvResult.Rows[r].Visible = true;
                                currencyManager1.ResumeBinding();
                            }
                            else
                            {
                                CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgvResult.DataSource];
                                currencyManager1.SuspendBinding();
                                dgvResult.Rows[r].Visible = false;
                                currencyManager1.ResumeBinding();
                            }
                        }
                    }
                }
                if (txtId.Text.Trim().Length > 0)
                {
                    for (int r = 0; r < dgvResult.Rows.Count; r++)
                    {
                        if (dgvResult.Rows[r].Visible)
                        {

                            if (dgvResult.Rows[r].Cells[0].Value == null)
                            {
                                continue;
                            }
                            if (dgvResult.Rows[r].Cells["PatientID"].Value.ToString().ToUpper().Contains(txtId.Text.Trim().ToUpper()))
                            {
                                // dgvResult.Rows[r].Visible = true;
                                CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgvResult.DataSource];
                                currencyManager1.SuspendBinding();
                                dgvResult.Rows[r].Visible = true;
                                currencyManager1.ResumeBinding();
                            }
                            else
                            {
                                // dgvResult.Rows[r].Visible = false;
                                CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgvResult.DataSource];
                                currencyManager1.SuspendBinding();
                                dgvResult.Rows[r].Visible = false;
                                currencyManager1.ResumeBinding();
                            }
                        }
                    }
                }
                if (txtDOB.Text.Trim().Length > 0)
                {
                    for (int r = 0; r < dgvResult.Rows.Count; r++)
                    {
                        if (dgvResult.Rows[r].Visible)
                        {
                            if (dgvResult.Rows[r].Cells[0].Value == null)
                            {
                                continue;
                            }
                            if (dgvResult.Rows[r].Cells["PatientDOB"].Value.ToString().ToUpper().Contains(txtDOB.Text.Trim().ToUpper()))
                            {
                                CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgvResult.DataSource];
                                currencyManager1.SuspendBinding();                       
                                dgvResult.Rows[r].Visible = true;
                                currencyManager1.ResumeBinding();
                            }
                            else
                            {
                                CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgvResult.DataSource];
                                currencyManager1.SuspendBinding();
                                dgvResult.Rows[r].Visible = false;
                                currencyManager1.ResumeBinding();

                            }
                        }
                    }
                }
                if (txtDOC.Text.Trim().Length > 0)
                {
                    for (int r = 0; r < dgvResult.Rows.Count; r++)
                    {
                        if (dgvResult.Rows[r].Visible)
                        {
                            if (dgvResult.Rows[r].Cells[0].Value == null)
                            {
                                continue;
                            }
                            if (dgvResult.Rows[r].Cells["Dateofcreation"].Value.ToString().ToUpper().Contains(txtDOC.Text.Trim().ToUpper()))
                            {
                                CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgvResult.DataSource];
                                currencyManager1.SuspendBinding();
                                dgvResult.Rows[r].Visible = true;
                                currencyManager1.ResumeBinding();
                            }
                            else
                            {
                                CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgvResult.DataSource];
                                currencyManager1.SuspendBinding();
                                dgvResult.Rows[r].Visible = false;
                                currencyManager1.ResumeBinding();
                                // ((DataGridViewRow)( dgvResult.Rows[r])).Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failure in search field" + ex);
            }
        }
        public void getIdData()
        {
            try
            {
                yugamiru_dbconnection = new SQLiteConnection("Data Source=yugamirus.sqlite;version=3");
                yugamiru_dbconnection.Open();
                DataTable dt = new DataTable("tblmaster_yugamiru");
                //string test = "SELECT * FROM GetPatientdetails WHERE Patientname = '" + txtSearchName.Text +"'";
                string tests = " Select PatientID, Patientname, Score, Rank, ComputerID, strftime('%d-%m-%Y',PatientDOB) as PatientDOB, strftime('%d-%m-%Y HH:MM:SS',Dateofcreation) as Dateofcreation FROM tblmaster_yugamiru where PatientID like '" + txtId.Text + "'";
                SQLiteDataAdapter da = new SQLiteDataAdapter(tests, yugamiru_dbconnection);
                //MessageBox.Show(tests);

                da.Fill(dt);
                dgvResult.DataSource = dt;
                yugamiru_dbconnection.Close();


            }

            catch (Exception ex)
            {
                MessageBox.Show("Table failure" + ex);

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            refreshdetails();
        }

        public void refreshdetails()
        {
            try
            {
                yugamiru_dbconnection = new SQLiteConnection("DataSource=yugamirus.sqlite;version=3");
                yugamiru_dbconnection.Open();
                string sql = @"select  PatientID, Patientname, Score, Rank, ComputerID, strftime('%d-%m-%Y',PatientDOB) 
                  as PatientDOB, strftime('%d-%m-%Y',Dateofcreation) as Dateofcreation from tblmaster_yugamiru";
                SQLiteCommand command = new SQLiteCommand(sql, yugamiru_dbconnection);
                command.ExecuteNonQuery();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable("tblmaster_yugamiru");
                adapter.Fill(dt);
                BindingSource bs = new BindingSource();
                bs.DataSource = dt;
                dgvResult.DataSource = bs;
                adapter.Update(dt);
                yugamiru_dbconnection.Close();
            }

            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtSearchName.Text = string.Empty;
            txtId.Text = string.Empty;
            textBox3.Text = string.Empty;
            txtScore.Text = string.Empty;
           txtDOB.Text = string.Empty;
            txtDOC.Text = string.Empty;
            refreshdetails();
        }
////////////////////////////////////////////////*******************Datatbase Ends********************************////////////////////

        private void dtpDOC_ValueChanged(object sender, EventArgs e)
        {
            txtDOC.Text = String.Format("{0: dd-MM-yyyy}", dtpDOC.Value);
        }

        private void dtpDOB_ValueChanged(object sender, EventArgs e)
        {
            txtDOB.Text = String.Format("{0: dd-MM-yyyy}", dtpDOB.Value);
        }

        private void btn_filter_Click(object sender, EventArgs e)
        {
            try
            {
                getSearchData();
            }
            catch(SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }

}
