﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebComCation
{
    public static class LicenseValidator
    {
        static Registry_Handler rh = new Registry_Handler();
        static ServerOutInMsg valueSetReg = new ServerOutInMsg();
        static ServerOutInMsg valueSetServer = new ServerOutInMsg();
        static ServerCommunicator sc = new ServerCommunicator();

        public static LicenseStatus IsResponseValid(ServerOutInMsg registryMSG, ServerOutInMsg srverMSG)
        {
            LicenseStatus lt = LicenseStatus.Unknown;
            //if(registryMSG.ActivationDate==srverMSG.ActivationDate &&
            //    registryMSG.ComputerID == srverMSG.ActivationDate &&
            //    registryMSG.ComputerName == srverMSG.ActivationDate &&
            //        registryMSG.EndDate == srverMSG.ActivationDate &&
            //    registryMSG.LicenseKey == srverMSG.ActivationDate &&
            //    registryMSG.ServerMessage == srverMSG.ActivationDate &&
            //    registryMSG.StartDate == srverMSG.ActivationDate &&
            //    registryMSG.Status  == srverMSG.ActivationDate)
            if (srverMSG.Status.ToUpper().Contains("invalid".ToUpper()))
            {
                lt = LicenseStatus.Invalid;
            }
            else
            {
                lt = LicenseStatus.Valid;
            }


            return lt;
        }
        public static LicenseStatus IsDateValid(ServerOutInMsg registryMSG)
        {
            LicenseStatus lstt = LicenseStatus.Unknown;
            if(registryMSG.EndDate==null || registryMSG.EndDate.Length ==0)
            {
                registryMSG.EndDate = Utility.DateTimeToString(DateTime.Now.AddHours(1));
            }
            DateTime dtEndCheck = Utility.StringToDateTime(registryMSG.EndDate);
            DateTime dtLastRun = (Registry_Handler.GetLastRunDateFromReg());
            DateTime dt = DateTime.Now;
            bool isDateCheckOK = false;
            if (Utility.IsFirstDateGreaterOrEqual( dtEndCheck , dt))
            {
                //lstt = LicenseStatus.Valid;
                isDateCheckOK = true;
            }
            else
            {
                //lstt = LicenseStatus.Invalid;
                isDateCheckOK = false ;
            }
            if(Utility.IsFirstDateGreaterOrEqual(dtLastRun,DateTime.Now.AddDays(0)))
            {
                isDateCheckOK = false;
                System.Windows.Forms.MessageBox.Show("System Date is Invalid","GSPORT");
                Application.Exit();
                System.Environment.Exit(1);
                //throw new Exception(" System Date is Invalid");
            }

          



            //Check system date
            if(isDateCheckOK)
            {
                return LicenseStatus.Valid;
            }
            else
            {
                return LicenseStatus.Invalid;
            }            
        }
        public static LicenseValidationResult IsPresentStoredLicenseValid()
        {
            
            LicenseValidationResult result = new LicenseValidationResult();
            result.status = LicenseStatus.Valid;
            valueSetReg = rh.GetStoredRegistryMsg();
            LicenseStatus lst = LicenseStatus.Valid;
            //Check last run basis if system date is valid
            try
            {
                if (IsDateValid(valueSetReg) != LicenseStatus.Valid && lst == LicenseStatus.Invalid)
                {
                    //throw new Exception("system date is not valid");
                    result.Message = "system date is not valid";
                    result.status = LicenseStatus.Invalid;
                    return result;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,"GSPORT");
                Application.Exit();
            }
            if(valueSetReg.LicenseKey==null || valueSetReg.LicenseKey.Length==0)
            {
                //License not found It may be due to the application on the  is running first time.
                //Launch License Wizard and ask user for License Key
                //System.Windows.Forms.MessageBox.Show("No valid License found for the application");

                result.Message = "No valid License found for the application."+Environment.NewLine+"Do you want to provide a valid license key?";
                result.status = LicenseStatus.Invalid;
                //return LicenseStatus.Invalid;
                return result;
            }
          


            valueSetServer = sc.SendAndGetResponse(valueSetReg.LicenseKey);
            
            
            if (/*result.status!=LicenseStatus.Valid && */valueSetServer.keyStatus.ToUpper().Trim().Contains("INVALID"))
            {
                //lst = LicenseStatus.Invalid;
                result.Message = "License key is Invalid";
                result.status = LicenseStatus.Invalid;

                //throw new Exception("License key is not valid");
            }
           
            if(!(valueSetServer.LicenseKey!=null || valueSetServer.LicenseKey.Trim().ToUpper()!=valueSetReg.LicenseKey.Trim().ToUpper() || lst != LicenseStatus.Valid))
            {
                //throw new Exception("Application's License is not valid");
                result.Message = "License key is not valid";
                result.status = LicenseStatus.Invalid;
            }


            // return lst;
            return result;
        }

        public static void HandleInvalidLicenseEvent(LicenseValidationResult rs)
        {
            if(rs.status!=LicenseStatus.Valid)
            {
                DialogResult dr= System.Windows.Forms.MessageBox.Show(rs.Message + Environment.NewLine + "Do you want to provide a Valid License Key?", "GSPORT",
                    MessageBoxButtons.YesNo);
                if (dr == DialogResult.No)
                {
                    System.Windows.Forms.Application.Exit();
                }
                else if(dr==DialogResult.Yes)
                {
                    KeyValidator kv = new KeyValidator();
                    kv.ShowDialog();
                }
            }
        }
    }
}
