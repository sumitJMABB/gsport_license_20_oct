﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebComCation
{
    public partial class frmViewLicense : Form
    {
        string licenseKey = null;
        string licenseDuration = null;
        string licenseType = null;
        string subscriptionDate = null;
        string expiryDate = null;
        string activationDate = null;
        ServerOutInMsg license = null;
        



        public frmViewLicense()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmViewLicense_Load(object sender, EventArgs e)
        {
            try
            {
                license = (new Registry_Handler()).GetStoredRegistryMsg();
                if (license.LicenseKey.Trim().Length == 0)
                {
                    MessageBox.Show("No License found on this machine");
                    return;
                }
                else
                {
                    txtLicense.Text = license.LicenseKey;
                    txtActivationDate.Text = license.ActivationDate;
                    txtType.Text = "Single";//license.keyStatus;
                    txtExpiryDate.Text = license.EndDate;
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Occured.");
            }
        }
    }
}
