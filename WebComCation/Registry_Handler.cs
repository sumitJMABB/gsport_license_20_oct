﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace WebComCation
{
    public class Registry_Handler
    {
        //Actual Values
        ServerOutInMsg oneMessage = new ServerOutInMsg();
         string LicenseKey { get; set; }
         string ComputerName { get; set; }
         string ComputerID { get; set; }
         string Status { get; set; }
         string Message { get; set; }
         string StartDate { get; set; }
         string EndDate { get; set; }
         string ActivationDate { get; set; }
        string LastRunDate { get; set; }
        //Actual Values end

        

        /// <summary>
        /// Call this method after setting the values of all elements
        /// True means write success and null means write failed
        /// </summary>
        /// <returns></returns>
        public static bool? SaveToRegistry( ServerOutInMsg serverMsgToStore)
        {

            bool? saveOp = null; //
            try
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\WinRegistry");
                key.SetValue("LicenseKey", serverMsgToStore.LicenseKey);
                key.SetValue("ComputerName", serverMsgToStore.ComputerName);
                key.SetValue("ComputerID", serverMsgToStore.ComputerID);               
                key.SetValue("Status", serverMsgToStore.Status);
                key.SetValue("ServerMessage", serverMsgToStore.ServerMessage);
                key.SetValue("StartDate", serverMsgToStore.StartDate);
                key.SetValue("EndDate", serverMsgToStore.EndDate);
                key.SetValue("ActivationDate", serverMsgToStore.ActivationDate);
                key.SetValue("LastRunDate", Utility.DateTimeToString(DateTime.Now));

                key.Close();
                //Keys stored successfully
                saveOp = true;
            }
            catch(Exception ex)
            {
                //Something wrong happened so operation was not sucessfull.
                saveOp = false;
            }
            return saveOp;
        }
        /// <summary>
        /// Call this method at startup and then elements' values from registry will be available.
        /// True means Read success and false means no keys are present yet and null means some exceptioned occured while reading from registry.
        /// </summary>
        /// <returns></returns>
        bool? ReadInitializeFromReg()
        {
            bool? readOp = null;
            try
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\WinRegistry");
                if (key != null)
                {
                    LicenseKey = (key.GetValue("LicenseKey","").ToString());
                    ComputerName = (key.GetValue("ComputerName","").ToString());
                    ComputerID = (key.GetValue("ComputerID","").ToString());
                    Status = (key.GetValue("Status", "").ToString());
                    Message = (key.GetValue("Message", "").ToString());
                    StartDate = (key.GetValue("StartDate", "").ToString());
                    EndDate = (key.GetValue("EndDate", "").ToString());
                    ActivationDate = (key.GetValue("ActivationDate", "").ToString());
                    LastRunDate = (key.GetValue("LastRunDate", "").ToString());

                    readOp = true;          //success
                    key.Close();
                }
                else
                {
                    readOp = false; //Read failed this will happen if the keys are not present yet in registry. Like at very first run.
                }
            }
            catch(Exception ex)
            {
                //some thing went wrong
                readOp = null;
            }
            


            return readOp;
        }
        public ServerOutInMsg GetStoredRegistryMsg()
        {
            if (ReadInitializeFromReg() == true)
            {
                //set oneMessage
                oneMessage.ActivationDate = this.ActivationDate;
                oneMessage.ComputerID = this.ComputerID;
                oneMessage.ComputerName = this.ComputerName;
                oneMessage.EndDate = this.EndDate;
                oneMessage.LicenseKey = this.LicenseKey;
                oneMessage.ServerMessage = this.Message;
                oneMessage.StartDate = this.StartDate;
                oneMessage.Status = this.Status;
                oneMessage.LastRunDate = this.LastRunDate;
            }
            else
            {
                throw new Exception("Unable to read from registry");
            }
            return oneMessage;
        }

        public string RegToJsonRawMsgToSend(ServerOutInMsg msg)
        {
            //string jsonQuery = "[{\"computer_id\": \"" + keyRequest.GetComputerID() +
            //                        "\",\"computer_name\": \"" + keyRequest.GetComputerName() +
            //                        "\",\"activation_key\": \"" + txtLicKey.Text + "\"}]";
            string jsonQuery = "[{\"computer_id\": \"" + keyRequest.GetComputerID() +
                                  "\",\"computer_name\": \"" + keyRequest.GetComputerName() +
                                  "\",\"activation_key\": \"" + oneMessage.LicenseKey + "\"}]";

            return "";
        }

        public static DateTime GetLastRunDateFromReg()
        {
            DateTime dtLastRun = new DateTime();
            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\WinRegistry");
            if (key != null)
            {
                dtLastRun = Utility.StringToDateTime(key.GetValue("LastRunDate", "20000921125936").ToString());

            }
            else
            {
                dtLastRun = DateTime.Now;
                SaveCurrentRunDateToReg();
            }
            key.Close();
            return dtLastRun;

        }
        public static void SaveCurrentRunDateToReg()
        {

            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\WinRegistry");
            key.SetValue("LastRunDate", Utility.DateTimeToString(DateTime.Now));
            key.Close();

        }

    }
}
